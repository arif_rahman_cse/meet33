package com.khalidkotlin.online.di

import android.app.Application
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.dao.SoftMaxDao
import com.khalidkotlin.online.data.database.AppDatabase
import com.khalidkotlin.online.utils.Constants.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)

class DatabaseModule {


    @Singleton
    @Provides
    fun provideRequestOptions(): RequestOptions? {
        return RequestOptions
            .placeholderOf(R.drawable.white_background)
            .error(R.drawable.white_background)
    }

    @Singleton
    @Provides
    fun provideGlideInstance(
        application: Application?,
        requestOptions: RequestOptions?
    ): RequestManager? {
        return Glide.with(application!!)
            .setDefaultRequestOptions(requestOptions!!)
    }

    @Singleton
    @Provides
    fun provideAppDb(application: Application?): AppDatabase? {
        return Room.databaseBuilder(application!!, AppDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration() // get correct db version if schema changed
            .build()
    }

    @Singleton
    @Provides
    fun providePodCastDao(db: AppDatabase): SoftMaxDao? {
        return db.softMaxDao()
    }

    @Singleton
    @Provides
    @Named("diskIO")
    fun provideDiskIOExecutor(): Executor? {
        return Executors.newSingleThreadExecutor()
    }

    @Singleton
    @Provides
    @Named("networkIO")
    fun provideNetworkIOExecutor(): Executor? {
        return Executors.newFixedThreadPool(3)
    }


}