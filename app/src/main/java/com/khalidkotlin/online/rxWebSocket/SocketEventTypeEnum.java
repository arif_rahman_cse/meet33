package com.khalidkotlin.online.rxWebSocket;


public enum SocketEventTypeEnum {
    OPEN, CLOSING, CLOSED, FAILURE, MESSAGE
}
