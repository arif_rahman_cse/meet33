package com.khalidkotlin.online.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

fun View.snackbar(msg:String) {
    Snackbar.make(this,msg,Snackbar.LENGTH_SHORT).show()
}

fun Context.toast(msg: String){
    Toast.makeText(this,msg,Toast.LENGTH_SHORT).show()
}