package com.khalidkotlin.online.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserCredentialPreference {

    private static UserCredentialPreference userCredentialPreference;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private UserCredentialPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFARANCE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();

    }

    //Creating object of private constructor for SharedPrefarance ready to data stored!
    public static UserCredentialPreference getPrefarences(Context context) {
        if (userCredentialPreference == null) {
            userCredentialPreference = new UserCredentialPreference(context);
        }
        return userCredentialPreference;
    }


    //-----------------------------Set value and get value form sharedPrefarance------------------------------//

    public void setUserName(String userName) {
        editor.putString(Constants.USERNAME, userName);
        editor.apply();
    }

    public String getUserName() {
        return sharedPreferences.getString(Constants.USERNAME, null);
    }

    public void setPassword(String password) {
        editor.putString(Constants.PASSWORD, password);
        editor.apply();
    }

    public String getPassword() {
        return sharedPreferences.getString(Constants.PASSWORD, null);
    }

    public void setIsRemember(boolean isRemember) {
        editor.putBoolean(Constants.IS_REMEMBER, isRemember);
        editor.apply();
    }

    public boolean getIsRemember() {
        return sharedPreferences.getBoolean(Constants.IS_REMEMBER, false);
    }


    public void setUserEmail(String userEmail) {
        editor.putString(Constants.USER_EMAIL, userEmail);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(Constants.USER_EMAIL, null);
    }

    public void deleteSharedPrefarance(Context mContext) {
        sharedPreferences = mContext.getSharedPreferences(Constants.SHARED_PREFARANCE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }


}
