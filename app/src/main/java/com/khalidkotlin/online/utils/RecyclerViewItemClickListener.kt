package com.khalidkotlin.online.utils

interface RecyclerViewItemClickListener {
    fun didPressed(id: Int, name: String, username: String)
}