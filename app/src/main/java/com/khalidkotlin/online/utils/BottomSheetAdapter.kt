package com.khalidkotlin.online.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.khalidkotlin.online.R

class BottomSheetAdapter(context: Context,var resource:Int,var list:ArrayList<String>) : ArrayAdapter<String>(context,resource,list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater=LayoutInflater.from(context)
        val view=layoutInflater.inflate(resource,null)

        val title:TextView=view.findViewById(R.id.tvCourseTitle)
        title.setText(""+list[position])

        return view
    }
}