package com.khalidkotlin.online.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.home.model.CommentsItem

class BottomSheetCommentAdapter(context: Context, var resource:Int, var list:List<CommentsItem>) : ArrayAdapter<CommentsItem>(context,resource,list) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater=LayoutInflater.from(context)
        val view=layoutInflater.inflate(resource,null)

        val title:TextView=view.findViewById(R.id.tvCourseTitle)
        val userName:TextView=view.findViewById(R.id.tvUserName)
        userName.text = list[position].author!!.username
        title.text = list[position].comment

        return view
    }
}