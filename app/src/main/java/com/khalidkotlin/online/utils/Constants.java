package com.khalidkotlin.online.utils;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.jetbrains.annotations.NotNull;

/**
 * Defines constants used in the app.
 */
public class Constants {

    private Constants() {
        // Restrict instantiation
    }
    /** Base URL for iTunes Search API */
    public static final String IMAGE_BASE_URL = "http://44.237.74.211/";
    public static final String IMAGE_BASE_URL2 = "http://kaykobad.pythonanywhere.com";
    public static final String LOGIN_RP = "rp_login";
    public static final String IS_PIN_CHANGED = "is_pin_changed";
    public static final String DATABASE_NAME = "meet33.db";
    public static final String THREAD_ID = "thread_id";
    public static final String PARTICIPATE_ID = "thread_perticipent";
    public static final String PARTICIPATE= "perticipent";
    public static final String NAME= "name";
    public static final String RECEIVER_ID= "receiver_id";
    public static final String ROOM_NAME= "room_name";
    public static final String GROUP_ID= "group_id";

    public static final String CHAT_TAB = "chat_tab";
    public static final String CHAT_TAB_NAME = "chat_tab_name";
    public static final String FROM_POST_CREATE = "from_post_create";

    @NotNull
    public static final String LOAD_HOME = "loadHomeFrag";
    public static final String VIEW_PROFILE = "viewProfile";
    public static final String VIEW_PROFILE_ID = "viewProfileId";

    public static final String SHARED_PREFARANCE_NAME = "EMAIL_PASSWORD_SHARED_PREFARANCE";
    public static final String USERNAME = "user_name";
    public static final String PASSWORD = "PASSWORD";
    public static final String IS_REMEMBER = "is_remember";

//    public static final String FIRST_PARTICIPATE_ID= "first_perticipent_id";
//    public static final String SECOND_PARTICIPATE_NAME= "second_perticipent_name";
//    public static final String SECOND_PARTICIPATE_ID= "second_perticipent_id";

    public static final String USER_EMAIL = "user_email";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String GENDER = "gender";
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
    public static final String COUNTRY = "country";
    public static final String ALREADY_IN_ROOM = "You are already in this room.";



    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }





}
