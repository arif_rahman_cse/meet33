package com.khalidkotlin.online.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.khalidkotlin.online.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public final class ProgressBar {
    private static final String TAG = "AppUtils";
    static SweetAlertDialog pDialog;
    static SweetAlertDialog networkDialog;
    static SweetAlertDialog ncPDialog;

    public static void showProgress(Context context, String msg) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#FF8B00"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void showNCProgress(Context context) {
        ncPDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        ncPDialog.getProgressHelper().setBarColor(Color.parseColor("#FF8B00"));
        ncPDialog.setTitleText("Loading ...");
        ncPDialog.setCancelable(false);
        ncPDialog.show();
    }

    public static void hideNCProgress() {
        if (ncPDialog != null) {
            ncPDialog.dismissWithAnimation();
        } else {
            Log.d(TAG, "hideNetworkProgress: Nothing to dismiss!");
        }
    }

    public static void hideProgress() {
        if (pDialog != null) {
            pDialog.dismissWithAnimation();
        } else {
            Log.d(TAG, "hideNetworkProgress: Nothing to dismiss!");
        }
    }

    public static void hideNetworkProgress() {
        if (networkDialog != null) {
            networkDialog.dismissWithAnimation();
        } else {
            Log.d(TAG, "hideNetworkProgress: Nothing to dismiss!");
        }

    }


    public static void showErrorProgressBar(Context context, String title, String content, Drawable icon) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText(title);
        pDialog.setContentText(content);
        pDialog.setCustomImage(icon);
        pDialog.setConfirmButtonBackgroundColor(Color.parseColor("#FF8B00"));
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public static void userAttentionPb(Context context, String content) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText("Your balance");
        pDialog.setContentText(content);
        pDialog.setCustomImage(R.drawable.ic_coins);
        pDialog.setConfirmButtonBackgroundColor(Color.parseColor("#FF8B00"));
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public static void userBalanceShow(Context context, String content) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setContentText(content);
        pDialog.setCustomImage(R.drawable.ic_coins);
        pDialog.setConfirmButtonBackgroundColor(Color.parseColor("#FF8B00"));
        pDialog.show();
        pDialog.setCancelable(true);
    }

    public static void errorMessage(Context context, String message) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        pDialog.setContentText(message);
        pDialog.setConfirmButtonBackgroundColor(Color.parseColor("#F81707"));
        pDialog.show();
        pDialog.setCancelable(true);
    }

    public static void showInfoDialog(Context context, String content) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText("Attention!");
        pDialog.setContentText(content);
        pDialog.setCustomImage(R.drawable.ic_info);
        pDialog.setConfirmButtonBackgroundColor(Color.parseColor("#FF8B00"));
        pDialog.show();
        pDialog.setCancelable(false);
    }

    public static SweetAlertDialog userActionSuccessPb(Context context, String content) {
        pDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.setTitleText("Success!");
        pDialog.setContentText(content);
        pDialog.show();
        return pDialog;
    }

    public static void networkConnectionPb(Context context) {
        networkDialog = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        networkDialog.setCancelable(false);
        networkDialog.setTitleText("Turn on Internet to continue");
        networkDialog.setContentText("Turn on your mobile Data or Wi-Fi and try again");
        networkDialog.setCustomImage(R.drawable.ic_internet);
        networkDialog.setConfirmButtonBackgroundColor(Color.parseColor("#FF8B00"));
        networkDialog.show();
    }


}
