package com.khalidkotlin.online.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

abstract class BaseFragment<T:ViewDataBinding,VM:ViewModel> :Fragment() {

    protected lateinit var binding: T
    protected lateinit var viewModel: VM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=DataBindingUtil.inflate(inflater,getFragmentView(),container,false)
        viewModel=ViewModelProvider(this).get(getViewModel())
        return binding.root
    }

    fun displayToast(msg:String){

        Toast.makeText(context,"$msg", Toast.LENGTH_SHORT).show()

    }


    abstract fun getFragmentView():Int
    abstract fun getViewModel(): Class<VM>
}



//class PersonAdapter :BaseRecyclerViewAdapter<Person,LayoutPersonBinding>() {
//    override fun getLayout() = R.layout.layout_person
//
//    override fun onBindViewHolder(
//        holder: Companion.BaseViewHolder<LayoutPersonBinding>,
//        position: Int
//    ) {
//        holder.binding.name.text=items[position].name
//        holder.binding.root.name.setOnClickListener {
//            listener?.invoke(it,items[position],position)
//        }
//    }
//}