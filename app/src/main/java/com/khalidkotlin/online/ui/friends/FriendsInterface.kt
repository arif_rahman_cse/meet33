package com.khalidkotlin.online.ui.friends

interface FriendsInterface {
    fun onAccept(id:Int,position:Int)
    fun onReject(id:Int,position: Int)
    fun onCancel(id:Int,position: Int)
    fun onAdd(id:Int,position: Int)

}