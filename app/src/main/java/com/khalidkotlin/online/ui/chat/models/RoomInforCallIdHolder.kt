package com.khalidkotlin.online.ui.chat.models

data class RoomInforCallIdHolder(val roomId : Int){
    companion object{
        val instance = ArrayList<RoomInforCallIdHolder>()
    }
}
