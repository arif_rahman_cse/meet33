package com.khalidkotlin.online.ui.friendlist.adapter

import android.content.Context
import android.util.Log
import android.widget.PopupMenu
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.FriendListItmeViewBinding
import com.khalidkotlin.online.ui.friendlist.InterfacesClick
import com.khalidkotlin.online.ui.friendlist.model.RpFriends
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener

class FriendListAdapter(
    private val context: Context,
    private val callBack: RecyclerViewItemClickListener,
    private val click: InterfacesClick
) : BaseRecyclerViewAdapter<RpFriends, FriendListItmeViewBinding>() {
    override fun getLayout() = R.layout.friend_list_itme_view

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<FriendListItmeViewBinding>,
        position: Int
    ) {

        /*
        holder.binding.friendListUserProfileName.text =
            "${items[position].firstName} ${items[position].lastName}"
        holder.binding.friendListUserName.text = "${items[position].username}"

        Glide.with(context).load(Constants.IMAGE_BASE_URL + items[position].profilePicture)
            .placeholder(R.drawable.developer).into(holder.binding.friendListUserImg)

         */

        holder.binding.friendListUserName.text = "${items[position].username}"

        holder.binding.friendListUserName.setOnClickListener {
            items[position].id?.let { it1 ->
                items[position].username?.let { it2 ->
                    callBack.didPressed(
                        it1, items[position].firstName + " " + items[position].lastName,
                        it2
                    )
                }
            }
        }

        holder.binding.imageView2.setOnClickListener {
            val menu = PopupMenu(context, it)
            menu.menu.add("Unfriend")
            //menu.menu.add("Block")
            menu.setOnMenuItemClickListener { item ->
                if (item!!.title == "Unfriend") {
                    Log.e("TAG", "onMenuItemClick: Unfriend")
                    click.unFriend(items[position].id!!, position)


                } else {
                    Log.e("TAG", "onMenuItemClick: block")
                }
                true
            }
            menu.show()


        }
    }

}