package com.khalidkotlin.online.ui.login.models

import com.google.gson.annotations.SerializedName

data class User(

	@field:SerializedName("gender")
	val gender: Any? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("profile_id")
	val profileId: Int? = null,

	@field:SerializedName("date_of_birth")
	val dateOfBirth: Any? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("bio")
	val bio: Any? = null,

	@field:SerializedName("profile_picture")
	val profilePicture: String? = null,

	@field:SerializedName("mobile_number")
	val mobileNumber: Any? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
