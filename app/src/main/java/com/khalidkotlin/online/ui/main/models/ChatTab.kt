package com.khalidkotlin.online.ui.main.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "chat_tab")
data class ChatTab(
    val title: String,
    var isOpen : Boolean,
    val isRoom : Boolean,
    @PrimaryKey(autoGenerate = false) val id: Int,
)