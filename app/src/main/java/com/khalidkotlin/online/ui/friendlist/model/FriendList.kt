package com.khalidkotlin.online.ui.friendlist.model

data class FriendList(
    val userImage: Int,
    val userProfileName: String,
    val userName: String,
)