package com.khalidkotlin.online.ui.chat.models;

class ChatRoomExpandableToggle {
    var myRoomExpand: Boolean = false
    var favouriteRoomExpand: Boolean = false
    var recentRoomExpand: Boolean = false
    var officialRoomExpand: Boolean = false
    var hotRoomExpand: Boolean = false
}