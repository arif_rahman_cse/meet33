package com.khalidkotlin.online.ui.friends.adapter

import android.content.Context
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemFriendsRequestBinding
import com.khalidkotlin.online.databinding.ItemPublicFriendsBinding
import com.khalidkotlin.online.ui.friends.FriendsInterface
import com.khalidkotlin.online.ui.friends.models.RpAllFriendList
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.utils.Constants

class AllFriendAdapter(private  val context: Context, var friendsInterface: FriendsInterface) :BaseRecyclerViewAdapter<RpAllUser,ItemPublicFriendsBinding>() {
    override fun getLayout()= R.layout.item_public_friends

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemPublicFriendsBinding>,
        position: Int
    ) {
        holder.binding.tvUsername.text=items[position].username
        holder.binding.tvFullName.text=items[position].firstName+" "+items[position].lastName

        Glide.with(context).load(Constants.IMAGE_BASE_URL + items[position].profilePicture)
            .placeholder(R.drawable.developer).into(holder.binding.userProfileImg)

        holder.binding.btnAdd.setOnClickListener {
            friendsInterface.onAdd(items[position].userId!!,position)
        }
    }


}