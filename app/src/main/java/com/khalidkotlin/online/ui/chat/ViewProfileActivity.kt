package com.khalidkotlin.online.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityViewProfileBinding
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_account.*
import javax.inject.Inject

@AndroidEntryPoint
class ViewProfileActivity : AppCompatActivity() {

    private var binding: ActivityViewProfileBinding? = null

    private val _profile = MutableLiveData<Resource<RpProfile>>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""
    private var userId = 0

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_profile)
        binding!!.toolbar.tvToolbarHeadline.text = "View Profile"
        initHawk()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"

        val isViewProfile = intent.getBooleanExtra(Constants.VIEW_PROFILE, false)
        val viewProfileId = intent.getIntExtra(Constants.VIEW_PROFILE_ID, 0)

        userId = if (isViewProfile) {
            viewProfileId
        } else {
            Hawk.get(Constants.RECEIVER_ID)

        }


        //tvName.text = "${rpLogin.firstName} ${rpLogin.lastName}"

        fetchProfile("Token ${rpLogin.authToken}", userId)
        observeProfile()

        binding!!.toolbar.ivBackButton.setOnClickListener {
            onBackPressed()

        }


    }

    private fun fetchProfile(token: String, userId: Int) {

        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getSingleProfile(token, userId).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeProfile() {
        profile.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeProfile: ${it.data.toString()}")

                    binding!!.toolbar.tvToolbarHeadline.text = it.data!!.username
                    binding!!.userProfileNameTv.text = it.data!!.username
                    binding!!.friendCount.text = it.data.friendCount.toString()
                    binding!!.postCount.text = it.data.totalPosts.toString()
                    binding!!.sentGiftCount.text = it.data.sentGiftCount.toString()
                    binding!!.receivedGiftCount.text = it.data.receivedGiftCount.toString()

                    Glide.with(this)
                        .load(Constants.IMAGE_BASE_URL + it.data.profilePicture)
                        .into(binding!!.circleImageView)
                    //binding!!.balanceCoin.text = it.data!!.points.toString()
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "ViewProfileActivity"
    }
}