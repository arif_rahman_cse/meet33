package com.khalidkotlin.online.ui.main

interface GetChatIdOnButtonClick {
    fun getId(id : Int)
}