package com.khalidkotlin.online.ui.home.model

data class User(
    val userImage: Int,
    val postImage: Int,
    val userProfileName: String,
    val userDesignation: String
)