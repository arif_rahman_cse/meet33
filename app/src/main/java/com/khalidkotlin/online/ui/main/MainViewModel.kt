package com.khalidkotlin.online.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {
//    private val _post= MutableLiveData<Resource<RpCreatePost>>()
//    private val _newsFeed= MutableLiveData<Resource<RpNewsFeed>>()
//
//    val posts: LiveData<Resource<RpCreatePost>>
//        get() = _post
//
//    val newsFeed: LiveData<Resource<RpNewsFeed>>
//        get() = _newsFeed
//
//
//
//    private fun createPost(post:String,token:String){
//        viewModelScope.launch {
//            _post.postValue(Resource.loading(null))
//
//            if (networkHelper.isNetworkConnected()){
//                repository.createPost(post, token).let {
//                    if (it.isSuccessful){
//                        _post.postValue(Resource.success(it.body()))
//                    }
//                    else
//                        _post.postValue(Resource.error(it.errorBody().toString(),null))
//                }
//            }
//            else
//                _post.postValue(Resource.error("No Internet Connection",null))
//        }
//    }
//
//
//
//
//    private fun fetchNewsFeed(post:String,token:String){
//        viewModelScope.launch {
//            _newsFeed.postValue(Resource.loading(null))
//
//            if (networkHelper.isNetworkConnected()){
//                repository.getNewsFeed(token).let {
//                    if (it.isSuccessful){
//                        _newsFeed.postValue(Resource.success(it.body()))
//                    }
//                    else
//                        _newsFeed.postValue(Resource.error(it.errorBody().toString(),null))
//                }
//            }
//            else
//                _newsFeed.postValue(Resource.error("No Internet Connection",null))
//        }
//    }





}