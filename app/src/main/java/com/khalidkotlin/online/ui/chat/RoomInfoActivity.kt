package com.khalidkotlin.online.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityRoomInfoBinding
import com.khalidkotlin.online.databinding.ActivitySignInBinding
import com.khalidkotlin.online.ui.group_chat.GroupChatActivity
import com.khalidkotlin.online.ui.group_chat.GroupChatViewModel
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RoomInfoActivity : AppCompatActivity() {

    private val viewModel by viewModels<GroupChatViewModel>()
    private var binding: ActivityRoomInfoBinding? = null
    private var token = ""
    var roomId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_info)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room_info)

        binding!!.toolBar.tvToolbarHeadline.text = "Room info"

        initHawk()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        roomId = Hawk.get(Constants.GROUP_ID)

        viewModel.roomInfo(roomId, token)
        observeRoomInfo()

        binding!!.toolBar.ivBackButton.setOnClickListener {
            Log.d(TAG, "onCreate: Back")
            onBackPressed()
        }
    }

    private fun observeRoomInfo() {
        viewModel!!.roomInfo.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRoomInfo: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRoomInfo: ${it.data!!.toString()}")
                    binding!!.roomId.text = "Room ID: ${it.data.id.toString()}"
                    binding!!.tvRoomName.text = it.data.roomName
                    binding!!.tvOwnerName.text = it.data.createdBy!!.username
                    binding!!.tvDescription.text = it.data.announcement.toString()
                    binding!!.tvCapacity.text = it.data.maxMemberCapacity.toString()
                    //binding!!.tvModerators.text = it.data.maxMemberCapacity.toString()

                    if (it.data.isLocked!!) {
                        binding!!.tvPublicKick.text = "YES"
                    } else {
                        binding!!.tvPublicKick.text = "NO"
                    }

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRoomInfo: error")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "RoomInfoActivity"
    }
}