package com.khalidkotlin.online.ui.single_chat

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityLiveChatWithAnonimousPersonBinding
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.ProgressBar
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.google.GoogleEmojiProvider
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import kotlin.concurrent.fixedRateTimer

@AndroidEntryPoint
class LiveChatWithAnonymousPerson : AppCompatActivity() {


    private val viewModel by viewModels<LiveChatViewModel>()
    //private val viewModel by viewModels<GiftStoreViewModel>()

    private var binding: ActivityLiveChatWithAnonimousPersonBinding? = null
    private var token = ""
    private var threadId = 0
    private var receiverId = 0

    private var giftArrayList = ArrayList<RpGift>()

    private val chatAdapter by lazy {
        LiveChatAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EmojiManager.install(GoogleEmojiProvider())
        setContentView(R.layout.activity_live_chat_with_anonimous_person)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_live_chat_with_anonimous_person)
        initHawk()
        prepareRecylerView()

        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        threadId = Hawk.get(Constants.THREAD_ID)
        receiverId = Hawk.get(Constants.PARTICIPATE_ID)

        Log.e(TAG, "onCreate: thread id " + threadId)

        viewModel.fetchAllGift(token)

        observeAllGift()
        observeSingleChat()
        observeSendMessage()
        observeGiftMessage()
        observeProfile()

        binding!!.backButtonIv.setOnClickListener {
            onBackPressed()
        }

        binding!!.checkWallet.setOnClickListener {
            viewModel.fetchProfile(token)

        }


        binding!!.checkmark.setOnClickListener {
            if (binding!!.inputMessage.text.isNullOrEmpty()) {
                return@setOnClickListener
            }

            for (item in giftArrayList) {
                if (item.giftCommand!! == binding!!.inputMessage.text.toString()) {
                    viewModel.sendGiftMessage(token, receiverId, item.id!!)
                    binding!!.inputMessage.setText("")
                    break
                }
            }

            if (binding!!.inputMessage.text.toString().isNotEmpty()) {
                viewModel.sendMessage(token, receiverId, binding!!.inputMessage.text.toString())
                binding!!.inputMessage.setText("")
            }

            Log.e(TAG, "onCreate: sending message")
        }

        binding!!.attachmentIv.setOnClickListener {
            Log.d(TAG, "onCreate: attachment")

            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }


        fixedRateTimer("timer", false, 0, 5000) {
            this@LiveChatWithAnonymousPerson.runOnUiThread {
                viewModel!!.getSingleChat(threadId, token)
            }
        }

        //emoji start
        val emojiPopup =
            EmojiPopup.Builder.fromRootView(binding!!.rootView).build(binding!!.inputMessage)
        /* emojiPopup.toggle() // Toggles visibility of the Popup.
         emojiPopup.dismiss() // Dismisses the Popup.
         emojiPopup.isShowing*/

        binding!!.emojiIv.setOnClickListener {
            if (emojiPopup.isShowing) {
                emojiPopup.dismiss()
                binding!!.emojiIv.setImageResource(R.drawable.ic_emoji)
            } else {
                emojiPopup.toggle()
                binding!!.emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }

    }


    private fun observeProfile() {
        viewModel.profile.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    ProgressBar.userAttentionPb(
                        this,
                        "Current Transferable balance is ${it.data!!.points}")
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun observeGiftMessage() {
        viewModel.sendGiftMessage.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSendMessage: loading........")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSendMessage: success and data is :${it!!.data}")
                    // chatAdapter.addItems(it.data!!)
                    Log.d(TAG, "observeGiftMessage: success!")
                    viewModel.getSingleChat(threadId, token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSendMessage: error......")
                }
            }
        })
    }

    private fun observeAllGift() {
        viewModel.gift.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllGift: loading......")
                }
                Status.SUCCESS -> {
                    //giftArrayList.clear()
                    Log.e(TAG, "observeAllGift: ${it.data!!.toString()}")

                    for (item in it.data) {
                        giftArrayList.add(item)
                    }
                    //adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllGift: error")
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                val file: File = ImagePicker.getFile(data)!!

                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
                // MultipartBody.Part is used to send also the actual file name
                val body = MultipartBody.Part.createFormData("attachment", file.name, requestFile)

                viewModel!!.sendMessageWithImage(
                    binding!!.inputMessage.text.toString(),
                    receiverId,
                    token,
                    body
                )
                binding!!.inputMessage.setText("")

                /*
                if (binding!!.inputMessage.text.toString().isEmpty()) {
                    //viewModel!!.sendGroupMessage(binding!!.inputMessage.text.toString(), groupId, token, body)
                    viewModel!!.sendMessageWithImage(binding!!.inputMessage.text.toString(), receiverId, token, body)
                    binding!!.inputMessage.setText("")
                } else {
                    viewModel!!.sendMessageWithImage(binding!!.inputMessage.text.toString(), receiverId, token, body)
                    binding!!.inputMessage.setText("")
                }

                 */

                //You can also get File Path from intent
                //  val filePath:String = ImagePicker.getFilePath(data)!!
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun prepareRecylerView() {

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        //Set Post RV
        binding!!.chatMessageRecyclerView.layoutManager = layoutManager
        //binding!!.chatMessageRecyclerView.hasFixedSize()
        binding!!.chatMessageRecyclerView.adapter = chatAdapter
        binding!!.chatMessageRecyclerView.smoothScrollToPosition(chatAdapter.itemCount)


    }

    private fun observeSingleChat() {
        viewModel.singleChat.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSingleChat: loading........")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSingleChat: success and data is :${it!!.data}")
                    chatAdapter.addItems(it.data!!.reversed())
                    binding!!.chatMessageRecyclerView.smoothScrollToPosition(chatAdapter.itemCount)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSingleChat: error......")
                }
            }
        })
    }


    private fun observeSendMessage() {
        viewModel.sendMessage.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSendMessage: loading........")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSendMessage: success and data is :${it!!.data}")
                    // chatAdapter.addItems(it.data!!)
                    viewModel.getSingleChat(threadId, token)
                    if (threadId == 0) {
                        chatAdapter.addItems(it.data!!)
                    }
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSendMessage: error......")
                }
            }
        })
    }


    companion object {
        private const val TAG = "LiveChatWithAnonymousPe"
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }


}