package com.khalidkotlin.online.ui.gift_store.models

data class RpGiftX(
    val added_on: String,
    val all_send_coin: Double,
    val gift_command: String,
    val gift_icon: String,
    val id: Int,
    val is_free: Boolean,
    val message: String,
    val single_send_coin: Double
)