package com.khalidkotlin.online.ui.settting.change_pin

import com.google.gson.annotations.SerializedName

data class RpChangePin(

	@field:SerializedName("current_pin")
	val currentPin: String? = null,

	@field:SerializedName("new_pin_2")
	val newPin2: String? = null,

	@field:SerializedName("new_pin")
	val newPin: String? = null
)
