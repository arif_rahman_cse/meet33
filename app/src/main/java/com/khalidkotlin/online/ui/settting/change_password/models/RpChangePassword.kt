package com.khalidkotlin.online.ui.settting.change_password.models

import com.google.gson.annotations.SerializedName

data class RpChangePassword(

	@field:SerializedName("new_password")
	val newPassword: String? = null,

	@field:SerializedName("new_password_2")
	val newPassword2: String? = null,

	@field:SerializedName("current_password")
	val currentPassword: String? = null
)
