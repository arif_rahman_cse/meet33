package com.khalidkotlin.online.ui.chat

interface ViewProfileInterface {
    fun clickProfileId(userId: Int)
}