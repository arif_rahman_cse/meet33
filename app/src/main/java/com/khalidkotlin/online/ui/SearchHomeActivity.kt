package com.khalidkotlin.online.ui


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_search_home.*

class SearchHomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_home)

        // back button listener set
        iv_backbutton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}