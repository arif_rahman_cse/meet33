package com.khalidkotlin.online.ui.group_chat.models

import com.google.gson.annotations.SerializedName

data class RpRoomInfo(

	@field:SerializedName("is_private")
	val isPrivate: Boolean,

	@field:SerializedName("is_locked")
	val isLocked: Boolean? = null,

	@field:SerializedName("date_updated")
	val dateUpdated: String? = null,

	@field:SerializedName("level")
	val level: Int? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("announcement_sender")
	val announcementSender: Any? = null,

	@field:SerializedName("total_members")
	val totalMembers: Int? = null,

	@field:SerializedName("admin")
	val admin: Admin? = null,

	@field:SerializedName("created_by")
	val createdBy: CreatedBy? = null,

	@field:SerializedName("max_member_capacity")
	val maxMemberCapacity: Int? = null,

	@field:SerializedName("room_name")
	val roomName: String? = null,

	@field:SerializedName("has_announcement")
	val hasAnnouncement: Boolean? = null,

	@field:SerializedName("members")
	val members: List<MembersItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("announcement")
	val announcement: String? = null
)

data class CreatedBy(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)

data class Admin(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)

data class MembersItem(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
