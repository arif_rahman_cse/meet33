package com.khalidkotlin.online.ui.chat.adapter

import android.content.Context
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewKickUserBinding
import com.khalidkotlin.online.databinding.ItemViewUserBinding
import com.khalidkotlin.online.ui.chat.KickUserInterface
import com.khalidkotlin.online.ui.chat.RoomInterface
import com.khalidkotlin.online.ui.group_chat.models.MembersItem
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo

class KickUserAdapter(private val kickUserInterface: KickUserInterface) :
    BaseRecyclerViewAdapter<MembersItem, ItemViewKickUserBinding>() {
    override fun getLayout() = R.layout.item_view_kick_user
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewKickUserBinding>,
        position: Int
    ) {

        holder.binding.tvUsername.text = items[position].username

        holder.binding.btnKickUser.setOnClickListener {
            kickUserInterface.kickUser(position, items[position].id!!)
        }

    }
}
