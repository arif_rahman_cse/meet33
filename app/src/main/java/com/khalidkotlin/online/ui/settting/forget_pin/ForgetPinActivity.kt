package com.khalidkotlin.online.ui.settting.forget_pin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.settting.change_pin.ChangePinActivity
import com.khalidkotlin.online.ui.settting.change_pin.RpChangePin
import com.khalidkotlin.online.ui.settting.forget_pin.models.RpForgetPin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
@AndroidEntryPoint
class ForgetPinActivity : AppCompatActivity() {

    private var binding: com.khalidkotlin.online.databinding.ActivityForgetPinBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""

    private val _forgetPin = MutableLiveData<Resource<RpForgetPin>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_pin)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_pin)
        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        observeForgotPin()

        binding!!.btnChangePin.setOnClickListener {
            closeKeyBoard()
            changeForgetPin(
                binding!!.currentPassword.text.toString(), binding!!.newPin.text.toString(),
                binding!!.confirmNewPin.text.toString()
            )
        }
        binding!!.backIconIv.setOnClickListener {
            onBackPressed()
        }

    }

    private fun observeForgotPin() {
        _forgetPin.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    //adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun changeForgetPin(currentPassword: String, newPin: String, confirmNewPin: String) {
        lifecycleScope.launchWhenCreated {
            _forgetPin.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.forgetPin(token, currentPassword, newPin, confirmNewPin).let {
                    if (it.isSuccessful) {
                        _forgetPin.postValue(Resource.success(it.body()))

                        binding!!.currentPassword.setText("")
                        binding!!.newPin.setText("")
                        binding!!.confirmNewPin.setText("")

                        val snack = Snackbar.make(
                            findViewById(android.R.id.content),
                            "Pin change successful!",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack.setAction("Ok") {
                            // TODO when you tap on "Click Me"
                        }
                        snack.show()
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                    } else {
                        _forgetPin.postValue(Resource.error(it.errorBody().toString(), null))
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                    }
                }
            } else
                _forgetPin.postValue(Resource.error("No Internet Connection", null))

        }
    }

    companion object {
        private const val TAG = "ForgetPinActivity"
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}