package com.khalidkotlin.online.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivitySignInBinding
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.signup.CreatAccount
import com.khalidkotlin.online.ui.signup.VerificationCodeActivity
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.UserCredentialPreference
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInActivity : AppCompatActivity() {

    private var binding: ActivitySignInBinding? = null
    private val loginViewModel by viewModels<LoginViewModel>()

    private var userCredentialPreference: UserCredentialPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        //data binding with layout
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        userCredentialPreference = UserCredentialPreference.getPrefarences(this)
        initHawk()

        if (userCredentialPreference!!.isRemember) {

            binding!!.EtemailId.setText(userCredentialPreference!!.password)
            binding!!.EtUserNameId.setText(userCredentialPreference!!.userName)
        }

        binding!!.ButLogin.setOnClickListener {
            val userName = binding!!.EtUserNameId.text.toString()
            val password = binding!!.EtemailId.text.toString()

            Log.e(TAG, "onLogin clicked, username:$userName & password:$password")

            if (isValidForm(userName, password)) {
                loginViewModel.fetchLogin(userName, password)
            } else {
                toast("Invalid input")
            }

        }

        binding!!.ButCreatAnAccount.setOnClickListener {
            //startActivity(Intent(this, CreateAccount::class.java))
            val intent = Intent(this, CreatAccount::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        binding!!.tvForgotPassword.setOnClickListener {
            //startActivity(Intent(this, ForgotPasswordActivity::class.java))
            val intent = Intent(applicationContext, ForgotPasswordActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        observeUser()


    }

    private fun observeUser()  {

        loginViewModel.user.observe(this, {
            when (it.status) {
                Status.LOADING -> {
                    //start loading progressbar
                    binding!!.loadingProgressBar.visibility = View.VISIBLE
                    Log.e(TAG, "onCreate: loading...")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "onCreate: success....")
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    val user = it.data
                    if (user!!.error == null) {
                        userCredentialPreference!!.userName = binding!!.EtUserNameId.text.toString()

                        if (binding!!.rememberMe.isChecked) {
                            userCredentialPreference!!.isRemember = true
                            userCredentialPreference!!.password = binding!!.EtemailId.text.toString()
                        }else{
                            userCredentialPreference!!.isRemember = false
                            userCredentialPreference!!.password = ""
                            userCredentialPreference!!.userName = ""
                        }

                        toast("login success")
                        Hawk.put(Constants.LOGIN_RP, user)
                        Hawk.put(Constants.IS_PIN_CHANGED, false)
                        finish()
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
                        toast(user.error!!)
                    }
                }
                Status.ERROR -> {
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    toast("Network Error")
                    Log.e(TAG, "onCreate: onError called")
                }
            }
        })

    }


    private fun isValidForm(userName: String, password: String): Boolean {
        if (userName.isEmpty() || password.isEmpty()) {
            return false
        }
        return true
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "SignInActivity"
    }


}