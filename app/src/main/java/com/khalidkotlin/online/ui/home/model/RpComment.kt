package com.khalidkotlin.online.ui.home.model

import com.google.gson.annotations.SerializedName

data class RpComment(

	@field:SerializedName("author_name")
	val authorName: String? = null,

	@field:SerializedName("total_comments")
	val totalComments: Int? = null,

	@field:SerializedName("comments")
	val comments: List<CommentsItem?>? = null,

	@field:SerializedName("post")
	val post: String? = null,

	@field:SerializedName("date_edited")
	val dateEdited: String? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("is_edited")
	val isEdited: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class CommentsItem(

	@field:SerializedName("author")
	val author: Author? = null,

	@field:SerializedName("post_id")
	val postId: Int? = null,

	@field:SerializedName("date_edited")
	val dateEdited: Any? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("is_edited")
	val isEdited: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("post_text")
	val postText: String? = null
)

data class Author(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)

