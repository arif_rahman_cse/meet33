package com.khalidkotlin.online.ui.account

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.settting.transfer_coin.TransferCoinActivity
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_account.*
import javax.inject.Inject

@AndroidEntryPoint
class AccountActivity : AppCompatActivity() {

    private var binding: com.khalidkotlin.online.databinding.ActivityAccountBinding? = null

    private val _profile = MutableLiveData<Resource<RpProfile>>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_account)
        initHawk()

        val rpLogin=Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        tvName.text="${rpLogin.firstName} ${rpLogin.lastName}"

        fetchProfile("Token ${rpLogin.authToken}")
        observeProfile()



        binding!!.ivBackButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun fetchProfile(token: String) {
        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun observeProfile() {
        profile.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    binding!!.balanceCoin.text = it.data!!.points.toString()
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    companion object {
        private const val TAG = "AccountActivity"
    }

    private fun initHawk(){
        Hawk.init(this).build()
    }
}