package com.khalidkotlin.online.ui.signup

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.google.gson.GsonBuilder
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.data.model.Response
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.chat.models.RpChatError
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.settting.change_password.ChangePasswordActivity
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.ProgressBar
import com.lelin.baseproject.utils.Status
import kotlinx.coroutines.launch
import java.io.IOException

class CreateAccountViewModel @ViewModelInject constructor(
    private val repository: Repository,
    private val networkHelper: NetworkHelper
) : ViewModel() {
    private val _user = MutableLiveData<Resource<RpLogin>>()

    val user: LiveData<Resource<RpLogin>>
        get() = _user


    fun registerAccount(
        userName: String, email: String, password: String, firstName: String, lastName: String,
        vCode: String, country: String, gender: String
    ) {
        viewModelScope.launch {
            _user.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.register(
                    userName,
                    email,
                    password,
                    firstName,
                    lastName,
                    vCode,
                    country,
                    gender
                ).let {
                    if (it.isSuccessful) {
                        _user.postValue(Resource.success(it.body()))
                    } else
                        if (it.code() == 400) {
                            val gson = GsonBuilder().create()
                            val errorMsg: com.khalidkotlin.online.ui.signup.models.Response

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    com.khalidkotlin.online.ui.signup.models.Response::class.java
                                )
                                val errorMassage = StringBuilder()

                                if (!errorMsg.email.isNullOrEmpty())
                                    errorMassage.append(errorMsg.email)

                                if (!errorMsg.username.isNullOrEmpty())
                                    errorMassage.append(errorMsg.username)

                                if (!errorMsg.password.isNullOrEmpty())
                                    errorMassage.append(errorMsg.password)

                                if (!errorMsg.verificationCode.isNullOrEmpty())
                                    errorMassage.append(errorMsg.verificationCode)



                                Log.d("TAG", "registerAccount: Error Msg: $errorMassage")
                                _user.postValue(Resource.error(errorMassage.toString(), null))

                            } catch (e: IOException) {

                            }
                        }else if (it.code() == 404){
                            _user.postValue(Resource.error(it.errorBody()!!.string(), null))
                        }

                }
            } else
                _user.postValue(Resource.error("No Internet Connection", null))
        }
    }


    fun sendVerificationCode(email: String) {
        viewModelScope.launch {
            _user.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.sendVerificationCode(email).let {
                    if (it.isSuccessful) {
                        _user.postValue(Resource.success(it.body()))
                    } else
                        if (it.code() == 400) {
                            val gson = GsonBuilder().create()
                            val errorMsg: com.khalidkotlin.online.ui.signup.models.Response

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    com.khalidkotlin.online.ui.signup.models.Response::class.java
                                )
                                val errorMassage = StringBuilder()

                                if (!errorMsg.email!!.isNullOrEmpty())
                                    errorMassage.append(errorMsg.email)

                                Log.d("TAG", "registerAccount: Error Msg: $errorMassage")
                                _user.postValue(Resource.error(errorMassage.toString(), null))

                            } catch (e: IOException) {

                            }
                        }

                }
            } else
                _user.postValue(Resource.error("No Internet Connection", null))
        }
    }


}