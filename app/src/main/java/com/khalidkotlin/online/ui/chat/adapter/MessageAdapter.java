package com.khalidkotlin.online.ui.chat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.khalidkotlin.online.R;
import com.khalidkotlin.online.rxWebSocket.entities.SocketMessageEvent;
import com.khalidkotlin.online.ui.chat.models.ChatErrorResponseModel;
import com.khalidkotlin.online.ui.chat.models.ChatHistoryModel;
import com.khalidkotlin.online.ui.chat.models.ChatResponseModel;
import com.khalidkotlin.online.ui.chat.models.MessageWithTypeModel;
import com.khalidkotlin.online.ui.login.models.RpLogin;
import com.khalidkotlin.online.utils.Constants;
import com.khalidkotlin.online.utils.ProgressBar;
import com.orhanobut.hawk.Hawk;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private Context context;

    private String userName = "";
    private AdapterUIOnClick uiOnClick;
    private int senderId = 0;
    private int userId;

    public MessageAdapter(Context context, String userName, AdapterUIOnClick uiOnClick) {
        this.context = context;
        this.userName = userName;
        this.uiOnClick = uiOnClick;

        RpLogin rpLogin = Hawk.get(Constants.LOGIN_RP);
        this.userId = rpLogin.getId();
    }


    private ArrayList<MessageWithTypeModel> messages = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_chat_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MessageWithTypeModel singleMessage = messages.get(position);
        holder.userName.setText(String.format("%s : ", singleMessage.getUserName()));
        holder.userName.setOnClickListener(v -> uiOnClick.userNameOnClick(singleMessage.getUserName()));

        if (singleMessage.isOwnMessage()){
            holder.userName.setTextColor(context.getResources().getColor(R.color.green_for_layout_bg));
        }
        holder.message.setText(singleMessage.getMessage());

        switch (singleMessage.getType()){
            case "yellow" :
                holder.userName.setVisibility(View.GONE);
                holder.message.setTextColor(ContextCompat.getColor(context,R.color.yellow));
                holder.message.setTypeface(null,Typeface.BOLD);
                holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                holder.message.setGravity(Gravity.CENTER);
                break;
            case "purple_light" :
                holder.userName.setVisibility(View.GONE);
                holder.message.setTextColor(ContextCompat.getColor(context,R.color.purple_light));
                holder.message.setTypeface(null,Typeface.BOLD);
                holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                holder.message.setGravity(Gravity.CENTER);
                break;
            case "purple" :
                holder.userName.setVisibility(View.GONE);
                holder.message.setTextColor(ContextCompat.getColor(context,R.color.purple));
                holder.message.setTypeface(null,Typeface.BOLD);
                holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                holder.message.setGravity(Gravity.CENTER);
                break;
            default:
                holder.userName.setVisibility(View.VISIBLE);
                holder.message.setTextColor(ContextCompat.getColor(context,R.color.black));
                holder.message.setTypeface(null,Typeface.NORMAL);
                holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                holder.message.setGravity(Gravity.START);
                break;
        }

        if (singleMessage.getUserName() == null || singleMessage.getUserName().isEmpty()){
            holder.userName.setVisibility(View.GONE);
            holder.message.setTypeface(null,Typeface.BOLD);
        }else if (singleMessage.getMessage() == null || singleMessage.getMessage().isEmpty()){
            holder.itemView.setVisibility(View.GONE);
        }

        if (singleMessage.getHasGift()){
//            holder.message.setTextColor(ContextCompat.getColor(context,R.color.red_btn_bg_pressed_color));
            holder.giftIcon.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(Constants.IMAGE_BASE_URL + singleMessage.getGiftIcon())
                    .into(holder.giftIcon);

        }else {
            holder.giftIcon.setVisibility(View.GONE);
        }

        /*if (singleMessage.isInformativeMessage()){
            holder.userName.setVisibility(View.GONE);
            holder.message.setTextColor(ContextCompat.getColor(context,R.color.maroon));
            holder.message.setTypeface(null,Typeface.BOLD);
            holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
            holder.message.setGravity(Gravity.CENTER);
        }else {
            holder.userName.setVisibility(View.VISIBLE);
            holder.message.setTextColor(ContextCompat.getColor(context,R.color.black));
            holder.message.setTypeface(null,Typeface.NORMAL);
            holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            holder.message.setGravity(Gravity.START);
        }*/

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        holder.itemView.requestFocus();
        super.onViewAttachedToWindow(holder);
    }

    public void addMessage(String data) {
        Log.e("chatRes",data);
            if (data.contains("type")){
            //messages.add(new Pair<>(simpleDateFormat.format(new Date()), event.getText()));
        }else {
            Gson gson = new Gson(); // Or use new GsonBuilder().create();
            ChatResponseModel chatResponse = gson.fromJson(data, ChatResponseModel.class);

            if (chatResponse.getId() != 0){

                senderId = chatResponse.getSender().getId();
                boolean isSender;
                isSender = userId == senderId;

                if (chatResponse.is_info_message()){
                    messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(),
                            chatResponse.getMessage(),"purple_light",false,"",isSender,true));

                }else if (chatResponse.is_music()){
                    //messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(), chatResponse.getMessage()+" "+chatResponse.getMusic().getLyric(),"maroon",false,"",isSender,true));
                    messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(), chatResponse.getMessage(),"purple_light",false,"",isSender,true));

                }else if (chatResponse.is_broadcast_message()){
                    messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(),
                            chatResponse.getMessage(),"yellow",false,"",isSender,false));

                }else if (chatResponse.is_gift_message()){

                    if (!chatResponse.getGift().is_free()){
                        messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(),
                                chatResponse.getMessage(),"purple",true,chatResponse.getGift().getGift_icon(),isSender,true));

                        if (chatResponse.getSender().getUsername().equals(userName)){
                            String message = "Single Send Coin : "+chatResponse.getGift().getSingle_send_coin()
                                    +"\nAll Send Coin : "+chatResponse.getGift().getAll_send_coin();
                            ProgressBar.userBalanceShow(context,message);
                        }

                    }else {
                        //messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(), chatResponse.getMessage(),"red",true,chatResponse.getGift().getGift_icon(),isSender,true));
                        messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(),
                                chatResponse.getMessage(),"purple_light",false, "",isSender,true));
                    }
                }else{
                    messages.add(new MessageWithTypeModel(chatResponse.getSender().getUsername(),
                            chatResponse.getMessage(),"normal",false,"",isSender,false));
                }
            }else {
                ChatErrorResponseModel errorResponseModel = gson.fromJson(data,ChatErrorResponseModel.class);
                Toast.makeText(context, errorResponseModel.getError(),Toast.LENGTH_SHORT).show();
                //.errorMessage(context,errorResponseModel.getError());
            }

        }
        notifyItemInserted(messages.size() );
    }

    public void deleteAll() {
        messages.clear();
        Log.e("data length",String.valueOf(messages.size()));
        notifyItemInserted(messages.size());
    }

    public void loadChatHistory(@NotNull ChatHistoryModel chatHistory) {
 /*       for (int i = 0 ; i<chatHistory.getMessage().size() ; i++){
            ChatResponseModel singleMessage = chatHistory.getMessage().get(i);
            senderId = singleMessage.getSender().getId();
            boolean isSender;
            isSender = userId == senderId;

            if (singleMessage.is_info_message()){
                messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(),
                        singleMessage.getMessage(),"purple_light",false,"",isSender,true));

            }else if (singleMessage.is_music()){

                //messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(), singleMessage.getMessage()+" "+singleMessage.getMusic().getLyric(),"red",false,"",isSender,true));
                messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(), singleMessage.getMessage(),"purple_light",false,"",isSender,true));

            }else if (singleMessage.is_broadcast_message()){
                messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(),
                        singleMessage.getMessage(),"yellow",false,"",isSender,false));

            }else if (singleMessage.is_gift_message()){

                if (!singleMessage.getGift().is_free()){
                    messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(),
                            singleMessage.getMessage(),"purple",true,singleMessage.getGift().getGift_icon(),isSender,true));

                    String message = "Single Send Coin : "+singleMessage.getGift().getSingle_send_coin()
                            +"\nAll Send Coin : "+singleMessage.getGift().getAll_send_coin();
                    //ProgressBar.userBalanceShow(context,message);
                }else {
                    //messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(), singleMessage.getMessage(),"red",true,singleMessage.getGift().getGift_icon(),isSender,true));
                    messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(),
                            singleMessage.getMessage(),"purple_light",false, "",isSender,true));
                }
            }else{
                messages.add(new MessageWithTypeModel(singleMessage.getSender().getUsername(),
                        singleMessage.getMessage(),"normal",false,"",isSender,false));
            }
        }

        notifyItemInserted(messages.size());*/
    }

    public void addMessageObject(@NotNull MessageWithTypeModel messageWithTypeModel) {
        messages.add(messageWithTypeModel);
        notifyItemInserted(messages.size());
    }

    public interface AdapterUIOnClick{
        void userNameOnClick(String userName);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView message,userName;
        ImageView giftIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.chat_message_message);
            userName = itemView.findViewById(R.id.chat_message_username);
            giftIcon = itemView.findViewById(R.id.gift_img);
        }
    }
}
