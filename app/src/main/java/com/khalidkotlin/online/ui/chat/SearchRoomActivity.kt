package com.khalidkotlin.online.ui.chat

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.reflect.TypeToken
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.roomdb.AppDatabase
import com.khalidkotlin.online.data.roomdb.AppExecutors
import com.khalidkotlin.online.databinding.ActivitySearchRoomBinding
import com.khalidkotlin.online.ui.chat.adapter.RoomSearchAdapter
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.main.models.ChatTab
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.ProgressBar
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import java.lang.reflect.Type


@AndroidEntryPoint
class SearchRoomActivity : AppCompatActivity(), RoomInterface {

    private val viewModel by viewModels<SearchRoomViewModel>()
    private var binding: ActivitySearchRoomBinding? = null

    private var token = ""
    private var roomId = 0
    private var roomName = ""
    private var mDb: AppDatabase? = null
    private lateinit var tabs: List<ChatTab>

    private val adapter by lazy {
        RoomSearchAdapter(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_room)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_room)
        mDb = AppDatabase.getInstance(applicationContext)
        initHawk()
        prepareRecylerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        observeSearchRoom()
        observeJoinRoom()

        binding!!.etSearchRoom.addTextChangedListener { text ->
            viewModel!!.fetcSearchRoom(text.toString(), token)
            Log.e(TAG, "onCreate: text is " + text.toString())
        }

        binding!!.ivBackButton.setOnClickListener {
            onBackPressed()
        }


    }


    private fun observeSearchRoom() {
        viewModel!!.allRoom.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    adapter.addItems(it.data!!)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun observeJoinRoom() {
        viewModel.joinRoom.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    ProgressBar.showProgress(this, "Joining Room...")
                    Log.e(TAG, "observeJoinRoom: loading......")
                }
                Status.SUCCESS -> {
                    ProgressBar.hideProgress()
                    Log.e(TAG, "observeJoinRoom: ${it.data!!.toString()}")

                    //Insert New tab in Database
                    val tab = ChatTab(roomName, true, isRoom = true, id = roomId)
                    AppExecutors.getInstance().diskIO().execute {
                        mDb!!.chatTabDao().insertTab(tab)
                    }

                    Hawk.put(
                        Constants.CHAT_TAB,
                        ChatTab(roomName, true, isRoom = true, id = roomId)
                    )
                    Hawk.put(Constants.GROUP_ID, roomId)
                    val intent = Intent(this, MainActivity::class.java)
                    intent.putExtra(Constants.THREAD_ID, roomId)
                    intent.putExtra(Constants.CHAT_TAB_NAME, roomName)
                    startActivity(intent)
                    finish()
                }
                Status.ERROR -> {
                    ProgressBar.hideProgress()
                    ProgressBar.showInfoDialog(this, it.message.toString())
                    Log.d(TAG, "observeJoinRoom: Error Body: ${it.data.toString()}")
                    //Log.d(TAG, "observeJoinRoom: Error: ${it.data!!.error}")
                    Log.d(TAG, "observeJoinRoom: Error message: ${it.message}")
                    Log.d(TAG, "observeJoinRoom: Error Code: ${it.status}")

                }
            }
        })
    }

    private fun prepareRecylerView() {

        val layoutManager = LinearLayoutManager(this)

        //Set Post RV
        binding!!.roomNameRv.layoutManager = layoutManager
        binding!!.roomNameRv.hasFixedSize()
        binding!!.roomNameRv.adapter = adapter


    }

    private fun initHawk() {
        Hawk.init(this).build()
    }


    companion object {
        private const val TAG = "SearchRoomActivity"
    }

    override fun joinRoom(id: Int, roomName: String) {
        viewModel.joinRoom(id, token)
        roomId = id
        this.roomName = roomName
    }

    override fun updateRoom(id: Int, roomName: String) {
    }

    override fun deleteRoom(id: Int) {
    }

    override fun addAsFavorite(id: Int) {
        TODO("Not yet implemented")
    }

    override fun removeFromFavorite(id: Int) {
        TODO("Not yet implemented")
    }
}