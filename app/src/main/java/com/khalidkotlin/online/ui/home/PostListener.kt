package com.khalidkotlin.online.ui.home

import com.khalidkotlin.online.ui.main.models.RpNewsFeed

interface PostListener {
    fun editPost(rpNewsFeed: RpNewsFeed)
    fun deletePost(rpNewsFeed: RpNewsFeed)
    fun onPostClick(rpNewsFeed: RpNewsFeed)
    fun onReactions(rpNewsFeed: RpNewsFeed,id:Int)
    fun onUpdateRecations(rpNewsFeed: RpNewsFeed,id:Int)
}