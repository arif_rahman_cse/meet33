package com.khalidkotlin.online.ui.signup

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityCreatAccountBinding
import com.khalidkotlin.online.databinding.ActivityVerificationCodeBinding
import com.khalidkotlin.online.ui.login.SignInActivity
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class VerificationCodeActivity : AppCompatActivity() {

    private val viewModel by viewModels<CreateAccountViewModel>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var binding: ActivityVerificationCodeBinding? = null
    private var userCredentialPreference: UserCredentialPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_code)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code)
        userCredentialPreference = UserCredentialPreference.getPrefarences(this)


        val email = intent.getStringExtra(Constants.USER_EMAIL)
        val username = intent.getStringExtra(Constants.USERNAME)
        val firstName = intent.getStringExtra(Constants.FIRST_NAME)
        val lastName = intent.getStringExtra(Constants.LAST_NAME)
        val password = intent.getStringExtra(Constants.PASSWORD)
        val gender = intent.getStringExtra(Constants.GENDER)
        val country = intent.getStringExtra(Constants.COUNTRY)


        binding!!.backButtonIv.setOnClickListener {
           onBackPressed()
        }

        binding!!.btnVerify.setOnClickListener {

            if(binding!!.etCode.text.toString().isNotEmpty()){
                viewModel.registerAccount(username!!, email!!, password!!, firstName!!, lastName!!,
                binding!!.etCode.text.toString(), country!!, gender!!)

            }else{
                toast("Please Provide Verification Code!")
            }

        }

        observeUser()
    }

    private fun observeUser() {

        viewModel.user.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //start loading progressbar
                    //binding!!.loadingProgressBar.visibility = View.VISIBLE
                    ProgressBar.showProgress(this, "Creating Account...")
                    Log.e(TAG, "onCreate: loading...")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "onCreate: success....")
                    //binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    ProgressBar.hideProgress()

                    var user = it.data
                    if (user!!.error == null) {
                        toast("login success")
                        Hawk.put(Constants.LOGIN_RP, user)
                        userCredentialPreference!!.userName = user.username
                        finish()
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
                        toast(user.error!!)
                    }
                }
                Status.ERROR -> {

                    ProgressBar.hideProgress()
                    ProgressBar.showInfoDialog(this, it.message.toString())
                    //binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    //Log.e(TAG, "observeUser: Error: ${it.data.toString()}")
                    //Log.e(TAG, "observeUser: Error msg: ${it.message.toString()}")
                    Log.e(TAG, "onCreate: onError called")
                }
            }
        })

    }

    companion object {
        private const val TAG = "VerificationActivity"
    }
}