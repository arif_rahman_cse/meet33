package com.khalidkotlin.online.ui.chat;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.khalidkotlin.online.R;
import com.khalidkotlin.online.databinding.FragmentChatContainerPersonalBinding;
import com.khalidkotlin.online.ui.chat.adapter.PersonalChatAdapter;
import com.khalidkotlin.online.ui.chat.adapter.PersonalChatAdapterNew;
import com.khalidkotlin.online.ui.chat.models.ChatResponseModel;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.google.GoogleEmojiProvider;

import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class ChatContainerFragmentPersonal extends Fragment {
    private static final String TAG = "ChatContainerFragmentPe";

    private FragmentChatContainerPersonalBinding binding;
    private OkHttpClient client;
    private WebSocket ws;
    private PersonalChatAdapterNew personalChatAdapter;
    private ChatResponseModel chatResponse;


    public ChatContainerFragmentPersonal() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EmojiManager.install(new GoogleEmojiProvider());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_container_personal, container, false);
        initWs();
        initRv();

        //emoji start
        final EmojiPopup emojiPopup = EmojiPopup.Builder.fromRootView(binding.rootView).build(binding.inputMessage);

        binding.emojiIv.setOnClickListener(view -> {
            if (emojiPopup.isShowing()) {
                emojiPopup.dismiss();
                binding.emojiIv.setImageResource(R.drawable.ic_emoji);
            } else {
                emojiPopup.toggle();
                binding.emojiIv.setImageResource(R.drawable.ic_text_enable_24);

            }
        });

        binding.checkmark.setOnClickListener(view -> {

            String text = Objects.requireNonNull(binding.inputMessage.getText()).toString();
            if (!text.equals("")) {
                String jsonText = "{\"message\": \"" + text + "\"}";
                ws.send(jsonText);
                binding.inputMessage.setText("");
            }

        });

        return binding.getRoot();

    }

    private void initRv() {
        personalChatAdapter = new PersonalChatAdapterNew(requireContext(), chatResponse);
        binding.chatMessageRecyclerView.setAdapter(personalChatAdapter);
        binding.chatMessageRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }


    private final class EchoWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            Log.d("ws", "Receiving : " + text);
            Gson gson = new Gson();
            ChatResponseModel chatResponseModel = gson.fromJson(text, ChatResponseModel.class);
            chatResponse = chatResponseModel;
            Log.d(TAG, "onMessage: " + chatResponseModel.getMessage());
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            Log.d("ws", "Receiving bytes : " + bytes.hex());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            Log.d("ws", "Closing : " + code + " / " + reason + ". Reconnecting web socket...");
            client.dispatcher().executorService().shutdown();
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(() -> initWs(), 1500);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null);
            Log.d("ws", "Error : " + t.toString() + ". Reconnecting web socket...");
            Log.d(TAG, "onFailure:  ");
            client.dispatcher().executorService().shutdown();
            final Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(() -> initWs(), 1500);
        }
    }


    private void initWs() {
        // Start the web socket
        client = new OkHttpClient();
        Request request = new Request.Builder().url("ws://44.237.74.211/ws/chat/room/1/e0eeea8bcd0824cee6faa099bb27f48f4f5a7831/").build();
        EchoWebSocketListener listener = new EchoWebSocketListener();
        ws = client.newWebSocket(request, listener);
    }
}