package com.khalidkotlin.online.ui.chat.models

import kotlinx.serialization.SerialName

data class ChatResponseModel(

    @SerialName("id") val id : Int,
    @SerialName("gift") val gift : Gift,
    @SerialName("sender") val sender : Sender,
    @SerialName("music") val music : Music,
    @SerialName("message") val message : String,
    @SerialName("send_date") val send_date : String,
    @SerialName("is_room_message") val is_room_message : Boolean,
    @SerialName("is_gift_message") val is_gift_message : Boolean,
    @SerialName("attachment") val attachment : String,
    @SerialName("is_info_message") val is_info_message : Boolean,
    @SerialName("is_broadcast_message") val is_broadcast_message : Boolean,
    @SerialName("is_music") val is_music : Boolean,
    @SerialName("thread") val thread : Int,
    @SerialName("chat_room") val chat_room : String

){
    data class Gift(@SerialName("id") val id : Int,
                    @SerialName("gift_command") val gift_command : String,
                    @SerialName("message") val message : String,
                    @SerialName("single_send_coin") val single_send_coin : Double,
                    @SerialName("all_send_coin") val all_send_coin : Double,
                    @SerialName("gift_icon") val gift_icon : String,
                    @SerialName("is_free") val is_free : Boolean,
                    @SerialName("added_on") val added_on : String)

    data class Sender(@SerialName("id") val id : Int=0,
                      @SerialName("username") val username : String,
                      @SerialName("first_name") val first_name : String,
                      @SerialName("last_name") val last_name : String
    )
    data class Music(@SerialName("id") val id : Int,
                      @SerialName("lyric") val lyric : String,
                      @SerialName("date_created") val date_created : String,
                      @SerialName("date_updated") val date_updated : String
    )

    companion object {
        val instance = ArrayList<ChatResponseModel>()
    } }
