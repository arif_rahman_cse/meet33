package com.khalidkotlin.online.ui.gift_store

import android.content.Context
import android.view.View
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewCommandBinding
import com.khalidkotlin.online.ui.gift_store.models.RpGift

class CommandAdapter(val context: Context) :
    BaseRecyclerViewAdapter<RpGift, ItemViewCommandBinding>() {
    override fun getLayout() = R.layout.item_view_command

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewCommandBinding>,
        position: Int
    ) {
        holder.binding.tvCommandName.text = "/${items[position].giftCommand}"
    }
}