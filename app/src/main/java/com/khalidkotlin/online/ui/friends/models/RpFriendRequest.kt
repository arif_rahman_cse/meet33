package com.khalidkotlin.online.ui.friends.models

import com.google.gson.annotations.SerializedName

data class RpFriendRequest(

	@field:SerializedName("sender_username")
	val senderUsername: String? = null,

	@field:SerializedName("receive_date")
	val receiveDate: String? = null,

	@field:SerializedName("sender_last_name")
	val senderLastName: String? = null,

	@field:SerializedName("sender_profile_picture")
	val senderProfilePicture: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sender_first_name")
	val senderFirstName: String? = null,

	@field:SerializedName("sender_id")
	val senderId: Int? = null
)
