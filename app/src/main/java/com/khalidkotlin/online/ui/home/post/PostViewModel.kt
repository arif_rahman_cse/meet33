package com.khalidkotlin.online.ui.home.post

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class PostViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {
    private val _postStatus = MutableLiveData<Resource<RpCreatePost>>()
//    private val _newsFeed= MutableLiveData<Resource<RpNewsFeed>>()
//
    val post: LiveData<Resource<RpCreatePost>>
        get() = _postStatus

//    val newsFeed: LiveData<Resource<RpNewsFeed>>
//        get() = _newsFeed
//

    fun createPost(token:String, post:String){
        viewModelScope.launch {
            _postStatus.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.createPost(token, post).let {
                    if (it.isSuccessful){
                        _postStatus.postValue(Resource.success(it.body()))
                    }
                    else
                        _postStatus.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _postStatus.postValue(Resource.error("No Internet Connection",null))
        }
    }



    fun createPostWithImage(token:String, post:String, file: MultipartBody.Part){
        viewModelScope.launch {
            _postStatus.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.createPostWithImage(token, post, file).let {
                    if (it.isSuccessful){
                        _postStatus.postValue(Resource.success(it.body()))
                    }
                    else
                        _postStatus.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _postStatus.postValue(Resource.error("No Internet Connection",null))
        }
    }



}