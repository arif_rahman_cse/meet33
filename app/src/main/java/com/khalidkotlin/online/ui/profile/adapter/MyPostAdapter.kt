package com.khalidkotlin.online.ui.profile.adapter

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewMyPostBinding
import com.khalidkotlin.online.ui.profile.PostListener
import com.khalidkotlin.online.ui.profile.models.RpMyPost
import com.khalidkotlin.online.utils.BottomSheetAdapter
import com.khalidkotlin.online.utils.BottomSheetListView
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk

class MyPostAdapter( val context: Context, private val postListener: PostListener) :
    BaseRecyclerViewAdapter<RpMyPost, ItemViewMyPostBinding>() {

    override fun getLayout(): Int {
        return R.layout.item_view_my_post
    }

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewMyPostBinding>,
        position: Int
    ) {
        holder.binding.postTv.text = "${items[position].post}"
        holder.binding.loveCount.text = "${items[position].totalReactions}"
        holder.binding.commentCount.text = "${items[position].totalComments}"
        initHakw()

        holder.binding.moreOptionsIv.setOnClickListener {
            displayBottomSheetCategories(items[position])
        }

        if (items[position].attachment != null) {
            holder.binding.materialCardView.visibility = View.VISIBLE
            Glide.with(context).load(Constants.IMAGE_BASE_URL + items[position].attachment)
                .placeholder(R.drawable.placeholder_img).into(holder.binding.postImg)
        }else{
            holder.binding.materialCardView.visibility = View.GONE
        }


    }

    fun displayBottomSheetCategories(rpMyPost: RpMyPost){
        val dialog = BottomSheetDialog(context,R.style.BottomSheetDialog)
        dialog.setContentView(R.layout.layout_bottom_sheet)
        val singleList=ArrayList<String>()

        singleList.add("Edit")
        singleList.add("Delete")

        val listView = dialog.findViewById<View>(R.id.listViewBtmSheet) as BottomSheetListView?
        listView!!.adapter= BottomSheetAdapter(context,R.layout.item_bottom_sheet,singleList)
        listView.setOnItemClickListener { adapterView, view, i, l ->
            if (i==0){
                postListener.editPost(rpMyPost)
            }
            else{
                postListener.deletePost(rpMyPost)
                dialog.dismiss()
            }

            dialog.dismiss()
        }

        dialog.show()
    }

    private fun initHakw() {
        Hawk.init(context).build()
    }
}