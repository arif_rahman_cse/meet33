package com.khalidkotlin.online.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityForgotPasswordBinding
import com.khalidkotlin.online.ui.signup.CreatAccount
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ForgotPasswordActivity : AppCompatActivity() {

    private var binding: ActivityForgotPasswordBinding? = null
    private val loginViewModel by viewModels<LoginViewModel>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var userCredentialPreference: UserCredentialPreference? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)
        userCredentialPreference = UserCredentialPreference.getPrefarences(this)
        initHawk()

        observeSendCode()
        observeChangePassword()

        binding!!.backButtonIv.setOnClickListener {
            onBackPressed()
        }

        binding!!.sendCodeBtn.setOnClickListener {

            if (binding!!.emailTv.text.toString().isNotEmpty()) {
                loginViewModel.sendResetCode(binding!!.emailTv.text.toString())
                userCredentialPreference!!.userEmail = binding!!.emailTv.text.toString()

            } else {
                toast("Enter a valid email address.")
            }

        }

        binding!!.changePasswordBtn.setOnClickListener {

            if (isValidData(
                    binding!!.etCode.text.toString(),
                    binding!!.newPassword1.text.toString(),
                    binding!!.newPassword2.text.toString()
                )
            ) {

                loginViewModel.changePassword(
                    binding!!.etCode.text.toString(),
                    userCredentialPreference!!.userEmail,
                    binding!!.newPassword1.text.toString(),
                    binding!!.newPassword2.text.toString()
                )
            } else {
                Log.d(TAG, "onCreate: Validation required")
            }
        }
    }

    private fun isValidData(code: String, pass1: String, pass2: String): Boolean {

        when {
            code.isEmpty() -> {
                binding!!.etCode.error = "Code required"
                binding!!.etCode.requestFocus()
                return false
            }
            pass1.isEmpty() -> {
                binding!!.newPassword1.error = "Password required"
                binding!!.newPassword1.requestFocus()
                return false
            }
            pass2.isEmpty() -> {
                binding!!.newPassword2.error = "Password required"
                binding!!.newPassword2.requestFocus()
                return false
            }

            pass1 != pass2 -> {
                toast("Password doesn't match! ")
                return false
            }
        }

        return true

    }

    private fun observeChangePassword() {
        loginViewModel.changePassword.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //start loading progressbar
                    ProgressBar.showProgress(this, "Resetting...")
                    Log.e(TAG, "onCreate: loading...")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "onCreate: success....${it.data!!.status}")
                    ProgressBar.hideProgress()

                    if (it.data.error.isNullOrEmpty()) {

                        binding!!.etCode.setText("")
                        binding!!.newPassword1.setText("")
                        binding!!.newPassword2.setText("")

                        val snack = Snackbar.make(
                            binding!!.mainContent,
                            it.data.detail!!,
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack.setAction("Login") {

                            val intent = Intent(this, SignInActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }

                        snack.show()

                    } else {
                        ProgressBar.showInfoDialog(this, it.data.error)
                    }


                }
                Status.ERROR -> {
                    ProgressBar.hideProgress()
                    toast("Error Occurred! ")
                    Log.e(TAG, "onCreate: onError called")
                }
            }
        })
    }


    private fun observeSendCode() {

        loginViewModel.sendCode.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //start loading progressbar
                    ProgressBar.showProgress(this, "Sending...")
                    Log.e(TAG, "onCreate: loading...")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "onCreate: success....${it.data!!.status}")
                    binding!!.emailTv.setText("")
                    ProgressBar.hideProgress()

                    if (it.data.error.isNullOrEmpty()) {
                        ProgressBar.showInfoDialog(this, it.data.detail)

                    } else {
                        ProgressBar.showInfoDialog(this, it.data.error)
                    }


                }
                Status.ERROR -> {
                    ProgressBar.hideProgress()
                    toast("Error Occurred! ")
                    Log.e(TAG, "onCreate: onError called")
                }
            }
        })

    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "ForgotPasswordActivity"
    }


}