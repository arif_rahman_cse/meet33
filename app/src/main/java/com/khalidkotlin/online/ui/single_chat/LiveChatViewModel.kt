package com.khalidkotlin.online.ui.single_chat

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.single_chat.models.RpChat
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.send_gift.RpSendGift
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class LiveChatViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {
    private val _singleChat= MutableLiveData<Resource<List<RpChat>>>()
    private val _sendMessage= MutableLiveData<Resource<List<RpChat>>>()
    private val _gift= MutableLiveData<Resource<List<RpGift>>>()
    private val _sendGiftMessage = MutableLiveData<Resource<List<RpSendGift>>>()
    private val _profile = MutableLiveData<Resource<RpProfile>>()

    val singleChat: LiveData<Resource<List<RpChat>>>
        get() = _singleChat

    val sendMessage: LiveData<Resource<List<RpChat>>>
        get() = _sendMessage

    val gift: LiveData<Resource<List<RpGift>>>
        get() = _gift

    val sendGiftMessage: LiveData<Resource<List<RpSendGift>>>
        get() = _sendGiftMessage

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

     fun getSingleChat(id:Int,token:String){
        viewModelScope.launch {
            _singleChat.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getSingleChat(token,id).let {
                    if (it.isSuccessful){
                        _singleChat.postValue(Resource.success(it.body()))
                    }
                    else
                        _singleChat.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _singleChat.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun sendMessage(token:String, receiver:Int, message:String){
        viewModelScope.launch {
            _sendMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.sendMessage(token,receiver,message).let {
                    if (it.isSuccessful){
                        _sendMessage.postValue(Resource.success(it.body()))
                    }
                    else
                        _sendMessage.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _sendMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }

    fun sendGiftMessage(token:String, receiverId:Int, giftId:Int){
        viewModelScope.launch {
            _sendGiftMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.sendGift(token,receiverId, giftId).let {
                    if (it.isSuccessful){
                        _sendGiftMessage.postValue(Resource.success(it.body()))
                    }
                    else
                        _sendGiftMessage.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _sendGiftMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }

    fun sendMessageWithImage(message: String, roomId:Int, token:String, file: MultipartBody.Part?){
        viewModelScope.launch {
            _sendMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                if (file != null) {
                    repository.sendMessageWithImage(token,roomId, message, file).let {
                        if (it.isSuccessful){
                            _sendMessage.postValue(Resource.success(it.body()))
                        } else
                            _sendMessage.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _sendMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun fetchAllGift(token:String){
        viewModelScope.launch {
            _gift.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllGift(token).let {
                    if (it.isSuccessful){
                        _gift.postValue(Resource.success(it.body()))
                    }
                    else
                        _gift.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _gift.postValue(Resource.error("No Internet Connection",null))
        }
    }

     fun fetchProfile(token: String) {
        viewModelScope.launch {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

}