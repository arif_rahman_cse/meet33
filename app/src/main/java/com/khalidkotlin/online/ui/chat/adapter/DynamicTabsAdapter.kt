package com.khalidkotlin.online.ui.chat.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.chat.models.DynamicTabModel


class DynamicTabsAdapter(var dynamicTabsData: ArrayList<DynamicTabModel>, var callBack: Callback,val context:Context) : RecyclerView.Adapter<DynamicTabsAdapter.DynamicTabAdapterViewHolder>() {

    class DynamicTabAdapterViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.chat_tab_name_tv)
        val icon = itemView.findViewById<ImageView>(R.id.chat_tab_iv)
        val mainLayout = itemView.findViewById<LinearLayout>(R.id.chat_tab);

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DynamicTabAdapterViewHolder {
        val inflater = LayoutInflater.from(context)
        val itemView: View = inflater.inflate(R.layout.item_view_chat_tab, null, false)

        return DynamicTabAdapterViewHolder(itemView)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: DynamicTabAdapterViewHolder, position: Int) {
        holder.title.text = dynamicTabsData[position].title
        holder.icon.setImageDrawable(context.getDrawable(dynamicTabsData[position].icon))
        if (dynamicTabsData[position].isSelecteed){
            holder.title.visibility = View.VISIBLE
        }else{holder.title.visibility = View.GONE}

        holder.mainLayout.setOnClickListener {
            callBack.onTabSelection(position)
        }
    }

    override fun getItemCount(): Int {
        return dynamicTabsData.size
    }

    interface Callback {
        fun onTabSelection(position: Int)
    }
}