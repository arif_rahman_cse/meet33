package com.khalidkotlin.online.ui.chat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.data.roomdb.AppDatabase
import com.khalidkotlin.online.databinding.FragmentChatContainerPBinding
import com.khalidkotlin.online.ui.chat.adapter.MessageAdapter
import com.khalidkotlin.online.ui.chat.models.ChatHistoryModel
import com.khalidkotlin.online.ui.chat.models.ChatResponseModel
import com.khalidkotlin.online.ui.chat.models.SendMessageFormatKotlin
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.GetChatIdOnButtonClick
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.main.models.ChatTab
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.settting.transfer_coin.TransferCoinActivity
import com.khalidkotlin.online.ui.single_chat.LiveChatAdapter
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.hideKeyboard
import com.lelin.baseproject.utils.Resource
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.google.GoogleEmojiProvider
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.*
import okio.ByteString
import javax.inject.Inject

@AndroidEntryPoint
class ChatContainerFragmentPFour(val receiverId: Int) : Fragment(), GetChatIdOnButtonClick {

    private lateinit var emojiPopup: EmojiPopup

    //new Socket variable
    private  lateinit var client : OkHttpClient
    private lateinit var listener : EchoWebSocketListener
    private lateinit var ws: WebSocket


    var chatHistory = ChatHistoryModel.instance

    private var binding: FragmentChatContainerPBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper


    var onChatTabDeleteListener: ChatContainerFragmentP.OnChatTabDeleteListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onChatTabDeleteListener = try {
            activity as ChatContainerFragmentP.OnChatTabDeleteListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement onSomeEventListener")
        }
    }


    private var token = ""

    private val _profile = MutableLiveData<Resource<RpProfile>>()

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    private lateinit var tabs: List<ChatTab>

    private val chatAdapter by lazy {
        LiveChatAdapter(requireContext())
    }

    //Web Socket Variables
    private lateinit var messageAdapter: MessageAdapter

    private var mDb: AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDb = AppDatabase.getInstance(requireContext())
        EmojiManager.install(GoogleEmojiProvider())
        initHawk()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = rpLogin.authToken.toString()

        hideKeyboard()

    }

    private fun addMessagetoHistory(chatId: Int, data: String) {
        if (data.contains("type")) {
            return
        } else {
            val gson = Gson() // Or use new GsonBuilder().create();
            val chatResponse = gson.fromJson(data, ChatResponseModel::class.java)

            if (chatResponse.id != 0){
                if (chatHistory.size>0 && isAlreadyAdded(chatId, chatHistory)){
                    for (i in 0 until chatHistory.size){
                        val id = chatHistory[i].chatId
                        if (id == chatId){
                            chatHistory[i].message.add(chatResponse)
                            return
                        }
                    }

                }else{
                    val list : ArrayList<ChatResponseModel> = ArrayList()
                    list.add(chatResponse)
                    chatHistory.add(ChatHistoryModel(chatId, list))
                }
            }
        }


    }

    private fun isAlreadyAdded(chatId: Int, chatHistory: ArrayList<ChatHistoryModel>): Boolean {
        for (i in 0 until chatHistory.size) {
            if (chatHistory[i].chatId == chatId) {
                return true
            } else
                continue
        }
        return false
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_container_p, container, false)

        emojiPopup = EmojiPopup.Builder.fromRootView(binding!!.rootView).build(binding!!.inputMessage)
        emojiPopup.dismiss()

        messageAdapter = MessageAdapter(requireContext(),Hawk.get<RpLogin>(Constants.LOGIN_RP).username, object : MessageAdapter.AdapterUIOnClick{
            override fun userNameOnClick(userName: String?) {
                binding!!.inputMessage.setText("$userName  ")
            }
        })
        (activity as MainActivity?)?.initInterface(object : GetChatIdOnButtonClick {
            override fun getId(id: Int) {
                messageAdapter.deleteAll()

                client.dispatcher.executorService.shutdown()
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({ initWs() }, 1000)
            }

        })

        //load previouse data
        if (chatHistory.size > 0) {
            for (i in 0 until chatHistory.size) {
                if (chatHistory[i].chatId == receiverId) {
                    val specificChat = chatHistory[i]
                    messageAdapter.loadChatHistory(specificChat)
                    break
                }
            }

        }

        //socket call
        initWs()

        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding!!.inputMessage.requestFocus()

        prepareRecylerView()

        binding!!.chatMoreOptionsIv.setOnClickListener {
            Log.d(TAG, "onActivityCreated: Chat more options")
            showMoreOptions(binding!!.chatMoreOptionsIv)
        }

        binding!!.checkmark.setOnClickListener {
            if (binding!!.inputMessage.text.isNullOrEmpty()) {
                return@setOnClickListener
            }

            if (binding!!.inputMessage.text.toString().isNotEmpty()) {

                val messageFormat = SendMessageFormatKotlin(binding!!.inputMessage.text.toString())

                val mapper = ObjectMapper()
                var json = ""
                try {
                    json = mapper.writeValueAsString(messageFormat)
                    println("ResultingJSONstring = $json")
                } catch (e: JsonProcessingException) {
                    e.printStackTrace()
                }
                ws.send(json)
                binding!!.inputMessage.setText("")
            }

            Log.e(TAG, "onCreate: sending message")
        }


        binding!!.emojiIv.setOnClickListener {
            if (emojiPopup.isShowing) {
                emojiPopup.dismiss()
                binding!!.emojiIv.setImageResource(R.drawable.ic_emoji)
            } else {
                emojiPopup.toggle()
                binding!!.emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }


    }

    private fun prepareRecylerView() {
        //val layoutManager = CustomLayoutManager(requireContext())
        //layoutManager.stackFromEnd = true
        val layoutManager = LinearLayoutManager(requireContext())
        binding!!.chatMessageRecyclerView.hasFixedSize()
        binding!!.chatMessageRecyclerView.layoutManager = layoutManager
        binding!!.chatMessageRecyclerView.adapter = messageAdapter


    }

    companion object {
        private const val TAG = "ChatContainerFragmentP"
    }

    private fun initHawk() {
        Hawk.init(context).build()
    }


    private fun showMoreOptions(chatMoreOptionsIv: ImageView) {

        val popupMenu = androidx.appcompat.widget.PopupMenu(requireContext(), chatMoreOptionsIv)

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.view_profile -> {
                    startActivity(Intent(requireContext(), ViewProfileActivity::class.java))
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.transfer_coin -> {
                    //fetchProfile()
                    startActivity(Intent(requireContext(), TransferCoinActivity::class.java))
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.close_chat -> {
                    onChatTabDeleteListener!!.chatTabDeleteEvent(receiverId)
                    true
                }
                else -> false
            }
        }

        popupMenu.inflate(R.menu.menu_personal_chat_more_options)

        try {
            val fieldMPopup =
                androidx.appcompat.widget.PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(popupMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
        } catch (e: Exception) {
            Log.e("Main", "Error showing menu icons.", e)
        } finally {
            popupMenu.show()
        }

    }


    override fun getId(id: Int) {
        messageAdapter.deleteAll()
        //load previouse data
        if (chatHistory.size > 0) {
            for (i in 0 until chatHistory.size) {
                if (chatHistory[i].chatId == receiverId) {
                    val specificChat = chatHistory[i]
                    messageAdapter.loadChatHistory(specificChat)
                    break
                }
            }

        }
        client.dispatcher.executorService.shutdown()
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({ initWs() }, 1000)
    }


    inner class EchoWebSocketListener : WebSocketListener() {
        private val NORMAL_CLOSURE_STATUS = 1000

        override fun onOpen(webSocket: WebSocket, response: Response) {}

        override fun onMessage(webSocket: WebSocket, text: String) {
            Log.d("ws", "Receiving : $text")

            activity!!.runOnUiThread(Runnable {
                addMessagetoHistory(receiverId, text)
                messageAdapter.addMessage(text)
                binding!!.chatMessageRecyclerView.smoothScrollToPosition(messageAdapter.itemCount)
            })

        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            Log.d("ws", "Receiving bytes : " + bytes.hex())
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null)
            Log.d("ws", "Closing : $code / $reason. Reconnecting web socket...")
            client.dispatcher.executorService.shutdown()
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({ initWs() }, 1000)
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null)
            Log.d("ws", "Error : $t. Reconnecting web socket...")
            client.dispatcher.executorService.shutdown()
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({ initWs() }, 1000)
        }

    }


    private fun initWs() {
        // Start the web socket
        val request: Request =
            Request.Builder().url("ws://meet33api.xyz/ws/chat/one-to-one/$receiverId/$token/")
                .build()

        client = OkHttpClient()
        listener = EchoWebSocketListener()
        ws = client.newWebSocket(request, listener)
    }


}