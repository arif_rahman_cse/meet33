package com.khalidkotlin.online.ui.home.model

import com.google.gson.annotations.SerializedName

data class RpCreateComment(

	@field:SerializedName("author_name")
	val authorName: String? = null,

	@field:SerializedName("post_id")
	val postId: Int? = null,

	@field:SerializedName("date_edited")
	val dateEdited: Any? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("is_edited")
	val isEdited: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("post_text")
	val postText: String? = null
)


