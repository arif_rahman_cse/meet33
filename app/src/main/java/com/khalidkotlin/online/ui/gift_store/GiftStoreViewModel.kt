package com.khalidkotlin.online.ui.gift_store

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch

class GiftStoreViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {
    private val _gift= MutableLiveData<Resource<List<RpGift>>>()
//    private val _newsFeed= MutableLiveData<Resource<RpNewsFeed>>()
//
    val gift: LiveData<Resource<List<RpGift>>>
        get() = _gift
//
//    val newsFeed: LiveData<Resource<RpNewsFeed>>
//        get() = _newsFeed
//
//
//





     fun fetchAllGift(token:String){
        viewModelScope.launch {
            _gift.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllGift(token).let {
                    if (it.isSuccessful){
                        _gift.postValue(Resource.success(it.body()))
                    }
                    else
                        _gift.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _gift.postValue(Resource.error("No Internet Connection",null))
        }
    }





}