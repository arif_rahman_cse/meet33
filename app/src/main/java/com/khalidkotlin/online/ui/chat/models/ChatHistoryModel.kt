package com.khalidkotlin.online.ui.chat.models


class ChatHistoryModel(
    var chatId : Int,
    val message : ArrayList<ChatResponseModel>
) {
    companion object {
        val instance = ArrayList<ChatHistoryModel>()
    }
}