package com.khalidkotlin.online.ui.single_chat.models

import com.google.gson.annotations.SerializedName

data class RpChat(

	@field:SerializedName("gift")
	val gift: Gift? = null,

	@field:SerializedName("is_info_message")
	val isInfoMessage: Boolean? = null,

	@field:SerializedName("is_room_message")
	val isRoomMessage: Boolean? = null,

	@field:SerializedName("attachment")
	val attachment: Any? = null,

	@field:SerializedName("sender")
	val sender: Sender? = null,

	@field:SerializedName("send_date")
	val sendDate: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("is_gift_message")
	val isGiftMessage: Boolean? = null,

	@field:SerializedName("thread")
	val thread: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("chat_room")
	val chatRoom: Any? = null
)

data class Sender(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)

data class Gift(

	@field:SerializedName("gift_description")
	val giftDescription: String? = null,

	@field:SerializedName("gift_icon")
	val giftIcon: String? = null,

	@field:SerializedName("gift_name")
	val giftName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("required_coin")
	val requiredCoin: Int? = null,

	@field:SerializedName("gift_command")
	val giftCommand: String? = null,

	@field:SerializedName("added_on")
	val addedOn: String? = null
)