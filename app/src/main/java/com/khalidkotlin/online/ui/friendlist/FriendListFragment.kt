package com.khalidkotlin.online.ui.friendlist

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentFriendListBinding
import com.khalidkotlin.online.ui.chat.ChatContainerFragment
import com.khalidkotlin.online.ui.friendlist.adapter.FriendListAdapter
import com.khalidkotlin.online.ui.friendlist.model.RpFriends
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener
import com.khalidkotlin.online.utils.snackbar
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match


/**
 * A simple [Fragment] subclass.
 * Use the [FriendListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class FriendListFragment : Fragment(), RecyclerViewItemClickListener, InterfacesClick {

    private var chatFragment: ChatContainerFragment? = null
    private var binding: FragmentFriendListBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""
    private var position = 0

    private val _profileUpdate = MutableLiveData<Resource<RpProfile>>()
    private val _profile = MutableLiveData<Resource<RpProfile>>()
    private val _friends = MutableLiveData<Resource<List<RpFriends>>>()
    private val _unfriend = MutableLiveData<Resource<RpSuccess>>()


    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    val friends: LiveData<Resource<List<RpFriends>>>
        get() = _friends

    private val adapter by lazy {
        FriendListAdapter(requireContext(), this, this)
    }

    interface OnSomeEventListener {
        fun someEvent(profileId: Int, username: String, isRoom: Boolean)
    }

    var someEventListener: OnSomeEventListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        someEventListener = try {
            activity as OnSomeEventListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement onSomeEventListener")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initHawk()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_friend_list, container, false)
        // Inflate the layout for this fragment
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        prepareRecylerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        fetchFriends("Token ${rpLogin.authToken}")
        fetchProfile("Token ${rpLogin.authToken}")
        observeProfile()
        observeFriends()
        observeUnfriends()
        observeUpdateProfileBio()

        binding!!.tvUsername.setOnClickListener {
            showStatusPopup(binding!!.tvUsername)
        }

        binding!!.bioEditIv.setOnClickListener {

            binding!!.bioEt.isFocusableInTouchMode = true
            binding!!.bioEt.isFocusable = true
            binding!!.bioEditIv.visibility = View.GONE
            binding!!.bioEditOkIv.visibility = View.VISIBLE
            //editTextMobile.setFocusable(true)
            //editTextMobile.setSelection(editTextMobile.getText().length)

        }

        binding!!.bioEditOkIv.setOnClickListener {

            binding!!.bioEt.isFocusableInTouchMode = false
            binding!!.bioEt.isFocusable = false
            binding!!.bioEditIv.visibility = View.VISIBLE
            binding!!.bioEditOkIv.visibility = View.GONE
            //editTextMobile.setFocusable(true)
            //editTextMobile.setSelection(editTextMobile.getText().length)

            if (binding!!.bioEt.text.toString().isNotEmpty()) {
                closeKeyBoard()
                updateProfileBio(binding!!.bioEt.text.toString())
            } else {
                Log.d(TAG, "onViewCreated: DO Nothing...")
            }

        }

        binding!!.ivRefresh.setOnClickListener {
            fetchFriends(token)
        }
    }

    private fun fetchProfile(token: String) {
        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeProfile() {
        profile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //ProgressBar.showProgress(requireContext())
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeProfile: " + it.message)
                    Log.e(TAG, "observeProfile: success........")
                    Log.e(TAG, "observeProfile: ${it.data!!.firstName}")

                    binding!!.tvUsername.text = it.data.username.toString()
                }
                Status.ERROR -> {
                    //ProgressBar.hideProgress()
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun updateProfileBio(bio: String) {

        lifecycleScope.launchWhenCreated {
            _profileUpdate.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updateBio(token, bio).let {
                    if (it.isSuccessful) {
                        _profileUpdate.postValue(Resource.success(it.body()))
                        requireView().snackbar("Bio update successful! ")
                        //Toast.makeText(requireContext(), "Bio update successful! ", Toast.LENGTH_SHORT).show()
                    } else
                        _profileUpdate.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profileUpdate.postValue(Resource.error("No Internet Connection", null))
        }

    }

    private fun observeUpdateProfileBio() {
        profile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeProfile: success........")
                    Log.e(TAG, "observeProfile: ${it.data!!.firstName}")

                    if (it.data.bio == null) {
                        Log.d(TAG, "observeProfileBio: Nothing to set ")
                    } else {
                        Log.d(TAG, "observeProfileBio: BIO ET")
                        binding!!.bioEt.setText(it.data.bio.toString())
                    }

                    Log.d(TAG, "observeProfileBio: ${it.data.bio}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun closeKeyBoard() {
        val imm =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    private fun showStatusPopup(tvUsername: AppCompatTextView) {

        val popupMenu = PopupMenu(requireContext(), tvUsername)

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.offline -> {
                    Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    tvUsername.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_offline,
                        0,
                        0,
                        0
                    )
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.online -> {
                    Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    tvUsername.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_online,
                        0,
                        0,
                        0
                    )
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.away -> {
                    Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    tvUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_away, 0, 0, 0)
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.busy -> {
                    Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    tvUsername.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_busy, 0, 0, 0)
                    //changeStatus(tvUsername, item.title)
                    true
                }
                else -> false
            }
        }

        popupMenu.inflate(R.menu.popup_menu_user_status)

        try {
            val fieldMPopup = PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(popupMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
        } catch (e: Exception) {
            Log.e("Main", "Error showing menu icons.", e)
        } finally {
            popupMenu.show()
        }


    }


    private fun prepareRecylerView() {

        val layoutManager = LinearLayoutManager(context)

        //Set Post RV
        binding!!.friendListRv.layoutManager = layoutManager
        binding!!.friendListRv.hasFixedSize()
        binding!!.friendListRv.adapter = adapter


    }

    private fun observeFriends() {
        friends.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeNewsfeed: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeNewsfeed: ${it.data!!.size}")
                    adapter.addItems(it.data)


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeNewsfeed: error")
                }
            }
        })
    }

    private fun fetchFriends(token: String) {
        lifecycleScope.launchWhenCreated {
            _friends.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getFriends(token).let {
                    if (it.isSuccessful) {
                        _friends.postValue(Resource.success(it.body()))
                    } else
                        _friends.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _friends.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun unfriendss(id: Int) {
        lifecycleScope.launchWhenCreated {
            _unfriend.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.unfriend(id, token).let {
                    if (it.isSuccessful) {
                        _unfriend.postValue(Resource.success(it.body()))
                    } else
                        _unfriend.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _unfriend.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeUnfriends() {
        _unfriend.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeUnfriends: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeUnfriends: ${it.data!!}")
                    adapter.removeItems(position)


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeUnfriends: error")
                }
            }
        })
    }


    private fun initHawk() {
        Hawk.init(context).build()
    }

    companion object {
        fun newInstance(param1: String?, param2: String?): FriendListFragment {
            return FriendListFragment()
        }

        private const val TAG = "FriendListFragment"
    }


    override fun didPressed(id: Int, name: String, username: String) {
        Log.d(TAG, "didPressed: Online Friend ID: $id  Name: $username")

        Hawk.put(Constants.GROUP_ID, 0)
        Hawk.put(Constants.RECEIVER_ID, id)

        someEventListener!!.someEvent(id, username, false)

    }

    override fun unFriend(id: Int, position: Int) {
        unfriendss(id)
        this.position = position
    }


}