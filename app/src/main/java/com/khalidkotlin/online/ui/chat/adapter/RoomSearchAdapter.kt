package com.khalidkotlin.online.ui.chat.adapter

import android.content.Context
import android.content.Intent
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewSearchRoomBinding
import com.khalidkotlin.online.ui.chat.RoomInterface
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.group_chat.GroupChatActivity
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk

class RoomSearchAdapter(private val roomInterface: RoomInterface) :BaseRecyclerViewAdapter<RpCreateRoom,ItemViewSearchRoomBinding>() {
    override fun getLayout()= R.layout.item_view_search_room

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewSearchRoomBinding>,
        position: Int
    ) {

        holder.binding.roomName.text=items[position].roomName
        holder.binding.count.text="(${items[position].totalMembers}/${items[position].maxMemberCapacity}"

        holder.binding.joinRoom.setOnClickListener {
           roomInterface.joinRoom(items[position].id!!, items[position].roomName!!)
        }
    }


}