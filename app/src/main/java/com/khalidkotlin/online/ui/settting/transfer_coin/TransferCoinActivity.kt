package com.khalidkotlin.online.ui.settting.transfer_coin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.friendlist.model.RpFriends
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.settting.change_pin.ChangePinActivity
import com.khalidkotlin.online.ui.settting.transfer_coin.models.RpTransferCoin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.ProgressBar
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TransferCoinActivity : AppCompatActivity() {

    private var binding: com.khalidkotlin.online.databinding.ActivityTransferCoinBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""
    private var receiverID = ""

    private val _friends = MutableLiveData<Resource<List<RpFriends>>>()
    private val _transferCoin = MutableLiveData<Resource<RpTransferCoin>>()
    private val _profile = MutableLiveData<Resource<RpProfile>>()

    val friends: LiveData<Resource<List<RpFriends>>>
        get() = _friends

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    var data = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_coin)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_transfer_coin)


        if (intent.getStringExtra(Constants.NAME).isNullOrBlank()) {
            binding!!.tvUsername.text = "Username"
        } else {
            binding!!.tvUsername.text = intent.getStringExtra(Constants.NAME)

        }


        ProgressBar.showProgress(this, "Checking...")

        initHawk()
        //prepareRecyclerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        //fetchFriends("Token ${rpLogin.authToken}")
        fetchProfile("Token ${rpLogin.authToken}")
        //observeFriends()
        observeTransferCoin()
        observeProfile()

        binding!!.userNameLayout.setOnClickListener {
            startActivity(Intent(this, SearchUserForTransferCoin::class.java))
            finish()
        }


        binding!!.backIconIv.setOnClickListener {
            onBackPressed()
        }

        binding!!.transferCoin.setOnClickListener {

            if (binding!!.transferCoinEt.text.toString().isNotEmpty()
                && !intent.getStringExtra(Constants.NAME)
                    .isNullOrBlank() && binding!!.transferCoinPinEt.text.toString()
                    .isNotEmpty()
            ) {
                closeKeyBoard()
                transferCoin(
                    intent.getIntExtra(Constants.RECEIVER_ID, 0),
                    Integer.valueOf(binding!!.transferCoinEt.text.toString()),
                    binding!!.transferCoinPinEt.text.toString()
                )
            } else {
                toast("Please Provide Information")
            }

        }
    }

    private fun fetchProfile(token: String) {
        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeProfile() {
        profile.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    ProgressBar.hideProgress()
                    if (!it.data!!.isPinChanged!!) {
                        startActivity(
                            Intent(
                                this,
                                ChangePinActivity::class.java
                            ).putExtra("from_menu", true)
                        )
                    } else {
                        binding!!.mainLo.visibility = View.VISIBLE
                        binding!!.transferableCoin.text =
                            "Your transferable account balance is ${it.data!!.points} coins"
                    }

                }
                Status.ERROR -> {
                    ProgressBar.hideProgress()
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun observeTransferCoin() {
        _transferCoin.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    //adapter.addItems(it.data)
                    fetchProfile(token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun transferCoin(receiverId: Int, amountCoin: Int, pin: String) {
        Log.d(TAG, "transferCoin: Transfer Coin: $amountCoin Receiver ID: $receiverID")
        lifecycleScope.launchWhenCreated {
            _transferCoin.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.transferCoin(token, receiverId, amountCoin, pin).let {
                    if (it.isSuccessful) {
                        _transferCoin.postValue(Resource.success(it.body()))
                        binding!!.transferCoinEt.setText("")
                        binding!!.transferCoinPinEt.setText("")
                        binding!!.tvUsername.text = "Username"
                        val snack = Snackbar.make(
                            findViewById(android.R.id.content),
                            "Coin transfer successful!",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack.setAction("Ok") {
                            // TODO when you tap on "Click Me"
                        }
                        snack.show()
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                    } else {
                        _transferCoin.postValue(Resource.error(it.errorBody().toString(), null))
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                        toast("Transfer Coin Failed!")
                    }
                }
            } else
                _transferCoin.postValue(Resource.error("No Internet Connection", null))

        }
    }

    /*

    private fun fetchFriends(token: String) {
        lifecycleScope.launchWhenCreated {
            _friends.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getFriends(token).let {
                    if (it.isSuccessful) {
                        _friends.postValue(Resource.success(it.body()))
                    } else
                        _friends.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _friends.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun observeFriends() {

        friends.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeNewsfeed: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeNewsfeed: ${it.data!!.size}")

                    data.add("-- Select Username --")
                    for (i in it.data) {
                        data.add(i.firstName + " " + i.lastName + " - " + i.username + "(" + i.id + ")")
                    }
                    val adapter = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item, data
                    )
                    binding!!.spSearchUser.adapter = adapter

                    binding!!.spSearchUser.onItemSelectedListener = object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>,
                            view: View, position: Int, id: Long
                        ) {
                            if (position > 0) {
                                receiverID = data[position].substring(
                                    data[position].indexOf("(") + 1, data[position].lastIndexOf(")")
                                )
                                Log.d(TAG, "onItemSelected: Receiver ID: $receiverID")

                                //transferCoin();
                            }

                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {
                            // write code to perform some action
                        }
                    }

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeNewsfeed: error")
                }
            }
        })
    }


     */

    private fun initHawk() {
        Hawk.init(this).build()
    }


    companion object {
        private const val TAG = "TransferCoinActivity"
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}