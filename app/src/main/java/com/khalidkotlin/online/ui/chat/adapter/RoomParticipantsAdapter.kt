package com.khalidkotlin.online.ui.chat.adapter

import android.content.Context
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewUserBinding
import com.khalidkotlin.online.ui.chat.RoomInterface
import com.khalidkotlin.online.ui.chat.ViewProfileInterface
import com.khalidkotlin.online.ui.group_chat.models.MembersItem
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo

class RoomParticipantsAdapter(
    private val context: Context,
    private val viewProfileInterface: ViewProfileInterface,
) : BaseRecyclerViewAdapter<MembersItem, ItemViewUserBinding>() {
    override fun getLayout() = R.layout.item_view_user
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewUserBinding>,
        position: Int
    ) {

        holder.binding.tvUsername.text = items[position].username

        holder.binding.usernameLo.setOnClickListener {
            viewProfileInterface.clickProfileId(items[position].id!!)

        }

    }
}
