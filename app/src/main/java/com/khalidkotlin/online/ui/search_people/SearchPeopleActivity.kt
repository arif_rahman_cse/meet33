package com.khalidkotlin.online.ui.search_people

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.search_people.adapter.SearchPublicFriendAdapter
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchPeopleActivity : AppCompatActivity(), RecyclerViewItemClickListener {


    private var binding: com.khalidkotlin.online.databinding.ActivitySearchPeopleBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""
    private var position=0

    private val _searchFriend = MutableLiveData<Resource<List<RpSearchUser>>>()
    private val _request = MutableLiveData<Resource<RpSuccess>>()


    val friends: LiveData<Resource<List<RpSearchUser>>>
        get() = _searchFriend

    private val adapter by lazy {
        SearchPublicFriendAdapter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_people)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_people)
        initHawk()
        prepareRecyclerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token="Token ${rpLogin.authToken}"
        observeSearchFriends()
        observeSendFriendRequest()
        binding!!.backIconIv.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

//        binding!!.searchIconIv.setOnClickListener {
//            Log.d(TAG, "onCreate: Search People!!")
//            var userName = binding!!.searchPeopleTv.text.toString()
//            fetchSearchFriends("Token ${rpLogin.authToken}", userName)
//            Log.d(TAG, "onCreate: User Name: $userName")
//        }

        binding!!.searchPeopleTv.addTextChangedListener {
            text -> fetchSearchFriends(token,text.toString())
        }
    }

    private fun fetchSearchFriends(token: String, userName: String) {

        lifecycleScope.launchWhenCreated {
            _searchFriend.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getSearchUser(token, userName).let {
                    if (it.isSuccessful) {
                        _searchFriend.postValue(Resource.success(it.body()))
                        Log.d(TAG, "fetchSearchFriends: Response: ${it.body().toString()}")
                    } else
                        _searchFriend.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _searchFriend.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeSearchFriends() {
        _searchFriend.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun sendFriendRequest(token:String,id: Int){

        lifecycleScope.launchWhenCreated {
            _request.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.sendFriendRequest(id, token).let {
                    if (it.isSuccessful){
                        _request.postValue(Resource.success(it.body()))
                    }
                    else{
                        _request.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _request.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun observeSendFriendRequest(){
        _request.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSendFriendRequest: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSendFriendRequest: ${it.data!!.toString()}")
                    adapter.removeItems(position)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSendFriendRequest: error")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun prepareRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding!!.friendListRv.layoutManager = layoutManager
        binding!!.friendListRv.hasFixedSize()
        binding!!.friendListRv.adapter=adapter
    }


    companion object {
        private const val TAG = "SearchPeopleActivity"
    }

    override fun didPressed(id: Int, name: String, username: String) {
        Log.e(TAG, "didPressed: id is "+id )
        sendFriendRequest(token, id)
        position=name.toInt()
    }
}