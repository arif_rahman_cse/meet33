package com.khalidkotlin.online.ui.login.models

import com.google.gson.annotations.SerializedName

data class RpSuccess(

	@field:SerializedName("success")
	val success: String? = null,
	@field:SerializedName("error")
	val error: String? = null
)
