package com.khalidkotlin.online.ui.settting.forget_pin.models

data class RpForgetPin(
	val newPin2: String? = null,
	val newPin: String? = null,
	val currentPassword: String? = null
)

