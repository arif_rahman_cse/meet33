package com.khalidkotlin.online.ui.chat

interface KickUserInterface {
    fun kickUser(position: Int, userId: Int)
}