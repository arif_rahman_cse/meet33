package com.khalidkotlin.online.ui.signup.models

import com.google.gson.annotations.SerializedName

data class RpCountries(

	@field:SerializedName("country_name")
	val countryName: String,

	@field:SerializedName("id")
	val id: Int,
)
