package com.khalidkotlin.online.ui.chat.adapter;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.khalidkotlin.online.R;
import com.khalidkotlin.online.ui.chat.models.MessageWithTypeModel;
import java.util.ArrayList;

public class TopInfoAdapter extends RecyclerView.Adapter<TopInfoAdapter.ViewHolder> {
    private Context context;
    private ArrayList<MessageWithTypeModel> data;

    public TopInfoAdapter(Context context, ArrayList<MessageWithTypeModel> data) {
        this.context = context;
        this.data = data;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_chat_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MessageWithTypeModel singleData = data.get(position);
        holder.userName.setText(String.format("%s : ", singleData.getUserName()));
        holder.message.setText(singleData.getMessage());
        if (singleData.getType().equals("red")){
            holder.userName.setVisibility(View.GONE);
            holder.message.setTextColor(ContextCompat.getColor(context, R.color.red_btn_bg_color));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        holder.itemView.requestFocus();
        super.onViewAttachedToWindow(holder);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView message,userName;

        public ViewHolder(View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.chat_message_message);
            userName = itemView.findViewById(R.id.chat_message_username);
        }
    }
}
