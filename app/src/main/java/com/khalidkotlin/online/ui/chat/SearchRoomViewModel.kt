package com.khalidkotlin.online.ui.chat

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.GsonBuilder
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.chat.models.RpChatError
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import kotlinx.coroutines.launch
import java.io.IOException


class SearchRoomViewModel @ViewModelInject constructor(
    private val repository: Repository,
    private val networkHelper: NetworkHelper
) : ViewModel() {
    private val _allRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private val _joinRoom = MutableLiveData<Resource<RpSuccess>>()

    val allRoom: LiveData<Resource<List<RpCreateRoom>>>
        get() = _allRoom

    val joinRoom: LiveData<Resource<RpSuccess>>
        get() = _joinRoom


    fun fetcSearchRoom(roomName: String, token: String) {
        viewModelScope.launch {
            _allRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.searchRoom(token, roomName).let {
                    if (it.isSuccessful) {
                        _allRoom.postValue(Resource.success(it.body()))
                    } else
                        _allRoom.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _allRoom.postValue(Resource.error("No Internet Connection", null))
        }
    }

    fun joinRoom(id: Int, token: String) {
        viewModelScope.launch {
            _joinRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.joinRoom(id, token).let {
                    if (it.isSuccessful) {
                        _joinRoom.postValue(Resource.success(it.body()))
                        Log.d("TAG", "joinRoom: ${it.body()}")
                    } else {

                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            val errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java
                                )

                                _joinRoom.postValue(Resource.error(errorMsg.error.toString(), null))

                            } catch (e: IOException) {

                            }
                        } else {
                            _joinRoom.postValue(Resource.error("Something Went Wrong", null))
                        }

                    }
                }
            } else
                _joinRoom.postValue(Resource.error("No Internet Connection", null))
        }
    }


}