package com.khalidkotlin.online.ui.friends.adapter

import android.content.Context
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemFriendsRequestBinding
import com.khalidkotlin.online.ui.friends.FriendsInterface
import com.khalidkotlin.online.ui.friends.models.RpFriendRequest
import com.khalidkotlin.online.utils.Constants

class AllFriendsRequestAdapter(
    private val context: Context,
    val friendsInterface: FriendsInterface
) : BaseRecyclerViewAdapter<RpFriendRequest, ItemFriendsRequestBinding>() {
    override fun getLayout() = R.layout.item_friends_request

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemFriendsRequestBinding>,
        position: Int
    ) {
        holder.binding.tvUsername.text = items[position].senderUsername
        holder.binding.tvFullName.text =
            items[position].senderFirstName + " " + items[position].senderLastName

        Glide.with(context).load(Constants.IMAGE_BASE_URL + items[position].senderProfilePicture)
            .placeholder(R.drawable.developer).into(holder.binding.userProfileImg)

        holder.binding.btnAccept.setOnClickListener {
            friendsInterface.onAccept(items[position].id!!, position)
        }

        holder.binding.btnReject.setOnClickListener {
            friendsInterface.onReject(items[position].id!!, position)
        }
    }
}