package com.khalidkotlin.online.ui.friends.friend

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.google.firebase.messaging.FirebaseMessaging
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentPublicFriendListBinding
import com.khalidkotlin.online.ui.friends.FriendsInterface
import com.khalidkotlin.online.ui.friends.adapter.AllFriendAdapter
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.MySingleton
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import javax.inject.Inject


@AndroidEntryPoint
class PublicFriendListFragment : Fragment(),FriendsInterface {

    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey = "key=" + "AAAA_XcGutg:APA91bEXqZ3t_a2P0n133SdReJbCvIdu163JlVzKQ660kfMddqVfWvOhcGYo4f0ggarBgREj_f40Njx4EQ1BHb2Fn8f6Xdhl9oW8vfoGy7IyfNhehyPozkwmKuOY5lbNlKN-TcbmI6dc"
    private val contentType = "application/json"

    private var NOTIFICATION_TITLE = ""
    private var NOTIFICATION_MESSAGE = ""
    private var TOPIC = ""

    private var token=""
    private var username=""
    private var userId=0
    private var position=0
    private val _friends = MutableLiveData<Resource<List<RpAllUser>>>()
    private val _request = MutableLiveData<Resource<RpSuccess>>()


    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var binding:FragmentPublicFriendListBinding?=null

    private val adapter by lazy {
        AllFriendAdapter(requireContext(), this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initHawk()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=DataBindingUtil.inflate(inflater,R.layout.fragment_public_friend_list,container,false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var rpLogin=Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token="Token "+rpLogin.authToken!!
        username = rpLogin.firstName + " "+ rpLogin.lastName
        userId = rpLogin.id!!

        FirebaseMessaging.getInstance().subscribeToTopic("/topics/$userId") // Receiver Subscribe topic

        prepareRecylerView()
        allFriend(token)
        observeAllFriendList()
        observeSendFriendRequest()

    }

    private fun allFriend(token:String){

        lifecycleScope.launchWhenCreated {
            _friends.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllUser(token).let {
                    if (it.isSuccessful){
                        _friends.postValue(Resource.success(it.body()))
                    }
                    else{
                        _friends.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _friends.postValue(Resource.error("No Internet Connection", null))

        }

    }


    private fun sendFriendRequest(token:String,id: Int,position: Int){

        lifecycleScope.launchWhenCreated {
            _request.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.sendFriendRequest(id, token).let {
                    if (it.isSuccessful){
                        _request.postValue(Resource.success(it.body()))
                    }
                    else{
                        _request.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _request.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun observeAllFriendList(){
        _friends.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(Companion.TAG, "observeAllFriendList: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(Companion.TAG, "observeAllFriendList: ${it.data!!.toString()}")
                    adapter.addItems(it.data!!)

                }
                Status.ERROR -> {
                    Log.e(Companion.TAG, "observeAllFriendList: error")
                }
            }
        })
    }


    private fun observeSendFriendRequest(){
        _request.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(Companion.TAG, "observeSendFriendRequest: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSendFriendRequest: ${it.data!!.toString()}")
                    //requireContext().toast(it.message!!)
                    adapter.removeItems(position)
                }
                Status.ERROR -> {
                    Log.e(Companion.TAG, "observeSendFriendRequest: error")
                }
            }
        })
    }



    private fun prepareRecylerView() {
        //Set Post RV
        binding!!.rvRequest.layoutManager = LinearLayoutManager(context)
        binding!!.rvRequest.hasFixedSize()
        binding!!.rvRequest.adapter = adapter
    }



    fun initHawk(){
        Hawk.init(context).build()
    }


    override fun onAccept(id: Int, position: Int) {
    }

    override fun onReject(id: Int, position: Int) {
    }

    override fun onCancel(id: Int, position: Int) {
    }

    override fun onAdd(id: Int, position: Int) {
        sendFriendRequest(token, id,position)
        this.position=position
        notifyUser(id)
    }

    private fun notifyUser(id: Int) {
        Log.d(Companion.TAG, "notifyUser: Clicked")
        //AppUtils.showProgress(context)
        //Push Notification title message
        TOPIC = "/topics/"+id //topic has to match what the receiver subscribed to
        NOTIFICATION_TITLE = "You got friend request"
        NOTIFICATION_MESSAGE = "$username sent you a friend request"
        //lat = mySharedPrefarance.getLastLat()
        //lng = mySharedPrefarance.getLastLong()
        //Log.d(TAG, "notifyNearByUser: Lat: $lat")
        //Log.d(TAG, "notifyNearByUser: Lat: $lng")
        val notification = JSONObject()
        val notificationBody = JSONObject()
        try {
            notificationBody.put("title", NOTIFICATION_TITLE)
            notificationBody.put("message", NOTIFICATION_MESSAGE)
            //notificationBody.put("key", "from_bar")
            //notificationBody.put("lng", lng)
            notification.put("to", TOPIC)
            notification.put("data", notificationBody)
        } catch (e: JSONException) {
            //AppUtils.hideProgress()
            Log.e(Companion.TAG, "onCreate: " + e.message)
        }

        val jsonObjectRequest: JsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener { response ->
                Toast.makeText(context, "Request Set Success!", Toast.LENGTH_SHORT).show()
                //Toast.makeText(getContext(), "Nearby User Notified", Toast.LENGTH_SHORT).show();
                Log.i(Companion.TAG, "onResponse: $response")
            },
            Response.ErrorListener {
                Toast.makeText(context, "Request error", Toast.LENGTH_LONG).show()
                Log.i(Companion.TAG, "onErrorResponse: Didn't work")
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }

        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest)
    }

    companion object {
        private const val TAG = "PublicFriendListFragmen"
    }


}