package com.khalidkotlin.online.ui.main.models

import com.google.gson.annotations.SerializedName

data class RpNewsFeed(

	@field:SerializedName("total_comments")
	val totalComments: Int? = null,

	@field:SerializedName("post")
	val post: String? = null,

	@field:SerializedName("date_edited")
	val dateEdited: Any? = null,

	@field:SerializedName("attachment")
	val attachment: Any? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("author")
	val author: Author? = null,

	@field:SerializedName("is_edited")
	val isEdited: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("post_privacy")
	val postPrivacy: String? = null,

	@field:SerializedName("total_reactions")
	val totalReactions: Int? = null
)

data class Author(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
