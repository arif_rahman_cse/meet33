package com.khalidkotlin.online.ui.chat.models

data class MessageWithTypeModel(val userName: String? = "", val message: String?, val type: String = "normal" , val hasGift : Boolean = false, val giftIcon: String = "",val isOwnMessage: Boolean = false,
        val isInformativeMessage: Boolean = false)
