package com.khalidkotlin.online.ui.signup

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityCreatAccountBinding
import com.khalidkotlin.online.ui.chat.ParticipantsActivity
import com.khalidkotlin.online.ui.chat.ViewProfileActivity
import com.khalidkotlin.online.ui.login.SignInActivity
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.signup.models.RpCountries
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.ProgressBar
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_creat_account.*
import javax.inject.Inject

@AndroidEntryPoint
class CreatAccount : AppCompatActivity() {

    private val viewModel by viewModels<CreateAccountViewModel>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var binding: ActivityCreatAccountBinding? = null
    private var country = ""

    private val _allCountries = MutableLiveData<Resource<List<RpCountries>>>()

    val allCountries: LiveData<Resource<List<RpCountries>>>
        get() = _allCountries

    private var countries = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_creat_account)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_creat_account)
        initHawk()

        //var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        //token="Token ${rpLogin.authToken}"

        getCountryList()


        radio_but_refercode_no.setOnClickListener {
            ll_refercode_id.isVisible = false
        }

        radio_but_refercode_yes.setOnClickListener {
            ll_refercode_id.isVisible = true
        }


        val backButton = findViewById<ImageView>(R.id.back_button_iv)
        backButton.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        val signInAccount = findViewById<TextView>(R.id.sign_btn)
        signInAccount.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        binding!!.btnRegister.setOnClickListener {
            val firstName = binding!!.firstName.text.toString()
            val lastName = binding!!.lastName.text.toString()
            val email = binding!!.emailIdEt.text.toString()
            val userName = binding!!.username.text.toString()
            val password = binding!!.passwordEt.text.toString()
            val reTypePassword = binding!!.conPasswordEt.text.toString()

            Log.e(TAG, "create account : first name :$firstName")
            Log.e(TAG, "create account : last name :$lastName")
            Log.e(TAG, "create account : email :$email")
            Log.e(TAG, "create account : username :$userName")
            Log.e(TAG, "create account : password :$password")
            Log.e(TAG, "create account : retype password :$reTypePassword")

            if (isValidForm(firstName, lastName, email, userName, password, reTypePassword)) {
                if (matchPassword(password, reTypePassword)) {
                    //viewModel.registerAccount(userName, email, password, firstName, lastName)
                    viewModel.sendVerificationCode(binding!!.emailIdEt.text.toString())
                } else {
                    toast("Password not match")
                }
            }


        }


        observeSendVerificationCode()
        observeCountry()


    }

    private fun observeSendVerificationCode() {
        viewModel.user.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //start loading progressbar
                    binding!!.loadingProgressBar.visibility = View.VISIBLE
                    Log.e(TAG, "onCreate: loading...")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "onCreate: success....")
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE


                    val intent = Intent(this, VerificationCodeActivity::class.java)
                    intent.putExtra(Constants.USER_EMAIL, binding!!.emailIdEt.text.toString())
                    intent.putExtra(Constants.USERNAME, binding!!.username.text.toString())
                    intent.putExtra(Constants.FIRST_NAME, binding!!.firstName.text.toString())
                    intent.putExtra(Constants.LAST_NAME, binding!!.lastName.text.toString())
                    intent.putExtra(Constants.PASSWORD, binding!!.passwordEt.text.toString())
                    intent.putExtra(Constants.COUNTRY, country)

                    if (binding!!.rdMale.isChecked) {
                        intent.putExtra(Constants.GENDER, Constants.MALE)
                    } else {
                        intent.putExtra(Constants.GENDER, Constants.FEMALE)
                    }
                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)

                }
                Status.ERROR -> {

                    ProgressBar.showInfoDialog(this, it.message.toString())
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    //Log.e(TAG, "observeUser: Error: ${it.data.toString()}")
                    //Log.e(TAG, "observeUser: Error msg: ${it.message.toString()}")
                    Log.e(TAG, "onCreate: onError called")
                }
            }
        })
    }


    private fun observeCountry() {
        _allCountries.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeCountry: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeCountry: ${it.data!!.toString()}")
                    countries.add("-- Select Country --")
                    for (item in it.data) {
                        countries.add(item.countryName)
                    }

                    val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_spinner_item,
                        countries
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    binding!!.countrySp.adapter = adapter

                    binding!!.countrySp.onItemSelectedListener = object :
                        AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>,
                            view: View, position: Int, id: Long
                        ) {
                            if (position > 0) {
                                country = countries[position]
                                Log.d(TAG, "onItemSelected: Selected Country: $country")

                                //transferCoin();
                            }

                        }

                        override fun onNothingSelected(parent: AdapterView<*>) {
                            // write code to perform some action
                        }
                    }

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeCountry: error")
                }
            }
        })
    }

    private fun getCountryList() {

        lifecycleScope.launchWhenCreated {
            _allCountries.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getAllCountries().let {
                    if (it.isSuccessful) {
                        _allCountries.postValue(Resource.success(it.body()))
                    } else
                        _allCountries.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _allCountries.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun isValidForm(
        firstName: String,
        lastName: String,
        email: String,
        userName: String,
        password: String,
        reTypePassword: String
    ): Boolean {
        when {
            firstName.isEmpty() -> {
                binding!!.firstName.error = "First name required"
                binding!!.firstName.requestFocus()
                return false

            }
            lastName.isEmpty() -> {
                binding!!.lastName.error = "Last name required"
                binding!!.lastName.requestFocus()
                return false

            }
            userName.isEmpty() -> {
                binding!!.username.error = "Username required"
                binding!!.username.requestFocus()
                return false
            }
            userName.length < 6 -> {
                binding!!.username.error = "Username must be at least 6 characters"
                binding!!.username.requestFocus()
                return false
            }
            email.isEmpty() -> {
                binding!!.emailIdEt.error = "Email address required"
                binding!!.emailIdEt.requestFocus()
                return false
            }
            password.isEmpty() -> {
                binding!!.passwordEt.error = "Password required"
                binding!!.passwordEt.requestFocus()
                return false
            }
            reTypePassword.isEmpty() -> {
                binding!!.conPasswordEt.error = "Password required"
                binding!!.conPasswordEt.requestFocus()
                return false
            }
            binding!!.countrySp.selectedItemId < 1 -> {
                toast("Please Select Your Country")
                return false
            }
        }
        return true
    }

    private fun matchPassword(password: String, reTypePassword: String): Boolean {
        if (password == reTypePassword) {
            return true
        }
        return false
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }


    companion object {
        private const val TAG = "CreateAccount"
    }

}