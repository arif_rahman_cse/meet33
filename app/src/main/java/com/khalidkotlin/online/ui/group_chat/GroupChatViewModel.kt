package com.khalidkotlin.online.ui.group_chat

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.group_chat.models.RpAddMember
import com.khalidkotlin.online.ui.group_chat.models.RpGroupMessage
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.send_gift.RpSendGift
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

class GroupChatViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {

    private val _groupMessage= MutableLiveData<Resource<List<RpGroupMessage>>>()
    private val _sendGroupMessage= MutableLiveData<Resource<List<RpGroupMessage>>>()
    private val _leaveRoom= MutableLiveData<Resource<RpSuccess>>()
    private val _addMembers= MutableLiveData<Resource<RpAddMember>>()
    private val _kickMember= MutableLiveData<Resource<RpSuccess>>()
    private val _roomInfo= MutableLiveData<Resource<RpRoomInfo>>()
    private val _friends = MutableLiveData<Resource<List<RpAllUser>>>()
    private val _profile = MutableLiveData<Resource<RpProfile>>()
    private val _gift= MutableLiveData<Resource<List<RpGift>>>()
    private val _sendRoomGiftMessage = MutableLiveData<Resource<List<RpSendGift>>>()

    val groupMessage: LiveData<Resource<List<RpGroupMessage>>>
        get() = _groupMessage

    val sendGroupMessage: LiveData<Resource<List<RpGroupMessage>>>
        get() = _sendGroupMessage

    val leaveRoom: LiveData<Resource<RpSuccess>>
        get() = _leaveRoom

    val roomInfo: LiveData<Resource<RpRoomInfo>>
        get() = _roomInfo

    val kickMember: LiveData<Resource<RpSuccess>>
        get() = _kickMember

    val friends: LiveData<Resource<List<RpAllUser>>>
        get() = _friends

    val addMembers: LiveData<Resource<RpAddMember>>
        get() = _addMembers

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    val gift: LiveData<Resource<List<RpGift>>>
        get() = _gift

    val sendRoomGiftMessage: LiveData<Resource<List<RpSendGift>>>
        get() = _sendRoomGiftMessage


    fun sendRoomGiftMessage(token:String, roomId: Int, giftId:Int){
        viewModelScope.launch {
            _sendRoomGiftMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.sendRoomGift(token,roomId, giftId).let {
                    if (it.isSuccessful){
                        _sendRoomGiftMessage.postValue(Resource.success(it.body()))
                    }
                    else
                        _sendRoomGiftMessage.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _sendRoomGiftMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun fetchAllGift(token:String){
        viewModelScope.launch {
            _gift.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllGift(token).let {
                    if (it.isSuccessful){
                        _gift.postValue(Resource.success(it.body()))
                    }
                    else
                        _gift.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _gift.postValue(Resource.error("No Internet Connection",null))
        }
    }

    fun fetchProfile(token: String) {
        viewModelScope.launch {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }


     fun fetchAllGroupMessage(id:Int, token:String){
        viewModelScope.launch {
            _groupMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllRoomMessage(token,id).let {
                    if (it.isSuccessful){
                        _groupMessage.postValue(Resource.success(it.body()))
                    }
                    else
                        _groupMessage.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _groupMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun sendGroupMessage(message: String, roomId:Int, token:String, file: MultipartBody.Part?){
        viewModelScope.launch {
            _sendGroupMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                if (file != null) {
                    repository.sendGroupMessage(token,roomId, message, file).let {
                        if (it.isSuccessful){
                            _sendGroupMessage.postValue(Resource.success(it.body()))
                        } else
                            _sendGroupMessage.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _sendGroupMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }

    fun sendGroupOnlyMessage(message: String, roomId:Int, token:String){
        viewModelScope.launch {
            _sendGroupMessage.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                    repository.sendGroupOnlyMessage(token,roomId, message).let {
                        if (it.isSuccessful){
                            _sendGroupMessage.postValue(Resource.success(it.body()))
                        } else
                            _sendGroupMessage.postValue(Resource.error(it.errorBody().toString(),null))
                    }

            }
            else
                _sendGroupMessage.postValue(Resource.error("No Internet Connection",null))
        }
    }



    fun leaveRoom(id: Int,token:String){
        viewModelScope.launch {
            _leaveRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.leaveRoom(token,id).let {
                    if (it.isSuccessful){
                        _leaveRoom.postValue(Resource.success(it.body()))
                    }
                    else
                        _leaveRoom.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _leaveRoom.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun kick(roomId: Int,memberId: Int,token:String){
        viewModelScope.launch {
            _kickMember.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.kickMember(token,roomId, memberId).let {
                    if (it.isSuccessful){
                        _kickMember.postValue(Resource.success(it.body()))
                    }
                    else
                        _kickMember.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _kickMember.postValue(Resource.error("No Internet Connection",null))
        }
    }


    fun addMembers(roomId: Int,members: List<Int>,token:String){
        viewModelScope.launch {
            _addMembers.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.addMember(token,roomId, members).let {
                    if (it.isSuccessful){
                        _addMembers.postValue(Resource.success(it.body()))
                    }
                    else
                        _addMembers.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _addMembers.postValue(Resource.error("No Internet Connection",null))
        }
    }

    fun roomInfo(id: Int,token:String){
        viewModelScope.launch {
            _roomInfo.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.roomInfo(token,id).let {
                    if (it.isSuccessful){
                        _roomInfo.postValue(Resource.success(it.body()))
                    }
                    else
                        _roomInfo.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _roomInfo.postValue(Resource.error("No Internet Connection",null))
        }
    }



     fun allFriend(token:String){

         viewModelScope.launch {
            _friends.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getAllUser(token).let {
                    if (it.isSuccessful){
                        _friends.postValue(Resource.success(it.body()))
                    }
                    else{
                        _friends.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else
                _friends.postValue(Resource.error("No Internet Connection", null))

        }

    }



}