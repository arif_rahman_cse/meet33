package com.khalidkotlin.online.ui.group_chat.models

import com.google.gson.annotations.SerializedName

data class RpAddMember(

	@field:SerializedName("success")
	val success: List<String?>? = null
)
