package com.khalidkotlin.online.ui.settting.change_password

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.settting.change_password.models.RpChangePassword
import com.khalidkotlin.online.ui.settting.forget_pin.ForgetPinActivity
import com.khalidkotlin.online.ui.settting.forget_pin.models.RpForgetPin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChangePasswordActivity : AppCompatActivity() {
    private var binding: com.khalidkotlin.online.databinding.ActivityChangePasswordBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""

    private val _changePassword = MutableLiveData<Resource<RpChangePassword>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        observeChangePass()

        binding!!.btnChangePass.setOnClickListener {
            closeKeyBoard()
            if (binding!!.currentPass.text.toString().isNotEmpty()
                && binding!!.newPass.text.toString().isNotEmpty()
                && binding!!.confirmNewPass.text.toString().isNotEmpty()
            ) {

                changePass(
                    binding!!.currentPass.text.toString(),
                    binding!!.newPass.text.toString(),
                    binding!!.confirmNewPass.text.toString()
                )
            }else{
                toast("Please fill all fields")
            }

        }
        binding!!.backIconIv.setOnClickListener {
            onBackPressed()
        }
    }

    private fun observeChangePass() {
        _changePassword.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    //adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun changePass(currentPass: String, newPass: String, confirmNewPass: String) {
        lifecycleScope.launchWhenCreated {
            _changePassword.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.changePassword(token, currentPass, newPass, confirmNewPass).let {
                    if (it.isSuccessful) {
                        _changePassword.postValue(Resource.success(it.body()))

                        binding!!.currentPass.setText("")
                        binding!!.newPass.setText("")
                        binding!!.confirmNewPass.setText("")

                        val snack = Snackbar.make(
                            findViewById(android.R.id.content),
                            "Password change successful!",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack.setAction("Ok") {
                            // TODO when you tap on "Click Me"
                        }
                        snack.show()
                        Log.d(TAG, "ChangePassword: ${it.body().toString()}")
                    } else {
                        toast("Something went wrong")
                        _changePassword.postValue(Resource.error(it.errorBody().toString(), null))
                        Log.d(TAG, "ChangePassword: ${it.body().toString()}")
                    }
                }
            } else
                _changePassword.postValue(Resource.error("No Internet Connection", null))

        }
    }

    companion object {
        private const val TAG = "ChangePasswordActivity"
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
}