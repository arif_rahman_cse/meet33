package com.khalidkotlin.online.ui.single_chat

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.LayoutChatMessageListItem2Binding
import com.khalidkotlin.online.rxWebSocket.entities.SocketMessageEvent
import com.khalidkotlin.online.ui.single_chat.models.RpChat
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk

class LiveChatAdapter(private val context: Context) :
    BaseRecyclerViewAdapter<RpChat, LayoutChatMessageListItem2Binding>() {
    var allChat = Hawk.get<RpLogin>(Constants.LOGIN_RP)
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<LayoutChatMessageListItem2Binding>,
        position: Int
    ) {

        //holder.binding.chatMessageMessage.text="${items[position].message}"
        //holder.binding.chatMessageUsername.text="${items[position].sender!!.username}"

        holder.binding.chatMessageUsername.text = "${items[position].sender!!.username}"

        if (items[position].message == null) {
            holder.binding.chatMessageMessage.visibility = View.GONE
        } else {
            holder.binding.chatMessageMessage.visibility = View.VISIBLE
            holder.binding.chatMessageMessage.text = "${items[position].message}"

            if (items[position].isGiftMessage!!) {
                holder.binding.chatMessageMessage.setTextColor(Color.parseColor("#FF8B00"))
            }else{
                holder.binding.chatMessageMessage.setTextColor(Color.parseColor("#7A8FA6"))
            }

        }

        if (items[position].attachment == null) {
            holder.binding.messageImg.visibility = View.GONE
        } else {
            holder.binding.messageImg.visibility = View.VISIBLE
            Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + items[position].attachment)
                .placeholder(R.drawable.placeholder_img)
                .into(holder.binding.messageImg)
        }

        /*
        if (allChat.id==items[position].sender){
            holder.binding.chatMessageUsername.text= allChat.firstName+" "+allChat.lastName
            holder.binding.chatMessageUsername.setTextColor(Color.GREEN)

        }
        else{
            holder.binding.chatMessageUsername.text= Hawk.get<String>(Constants.NAME)
            holder.binding.chatMessageUsername.setTextColor(context.resources.getColor(R.color.yellow))

        }

         */

    }

    private fun hawk() {
        Hawk.init(context).build()
    }

    override fun getLayout(): Int {
        hawk()
        return R.layout.layout_chat_message_list_item2
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    fun addMessage(socketMessageEvent: SocketMessageEvent?) {
        Log.e("socketResponse",socketMessageEvent!!.text.toString())

    }
}