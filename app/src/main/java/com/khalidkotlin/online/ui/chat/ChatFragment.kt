package com.khalidkotlin.online.ui.chat

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.khalidkotlin.online.R
import com.khalidkotlin.online.animations.Animations
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentChatBinding
import com.khalidkotlin.online.ui.chat.adapter.RoomListAdapter
import com.khalidkotlin.online.ui.chat.models.ChatRoomExpandableToggle
import com.khalidkotlin.online.ui.chat.models.RpChatError
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.friendlist.FriendListFragment
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.ChatTab
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import javax.inject.Inject


@AndroidEntryPoint
class  ChatFragment : Fragment(), RoomInterface, RecyclerViewItemClickListener {
    private lateinit var expandableToggle: ChatRoomExpandableToggle
    private var binding: FragmentChatBinding? = null
    private val _createRoom = MutableLiveData<Resource<RpCreateRoom>>()
    private val _updateRoom = MutableLiveData<Resource<RpCreateRoom>>()
    private val _deleteRoom = MutableLiveData<Resource<RpSuccess>>()
    private val _allRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private val _hotRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private val _recentRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private val _myRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private val _favouriteRoom = MutableLiveData<Resource<List<RpCreateRoom>>>()
    private var token = ""
    private var roomName = ""
    private var roomId = 0

    private val _joinRoomChat = MutableLiveData<Resource<RpSuccess>>()

    private var officialRoom = ArrayList<RpCreateRoom>()
    private var unOfficialRoom = ArrayList<RpCreateRoom>()

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val adapterFavourite by lazy {
        RoomListAdapter(
            requireContext(),
            this,
            this,
            UserCredentialPreference.getPrefarences(requireContext()).userName,
            true
        )
    }
    private val adapterMyRoom by lazy {
        RoomListAdapter(
            requireContext(),
            this,
            this,
            UserCredentialPreference.getPrefarences(requireContext()).userName,
            false
        )
    }

    private val adapterOffice by lazy {
        RoomListAdapter(
            requireContext(),
            this,
            this,
            UserCredentialPreference.getPrefarences(requireContext()).userName,
            false
        )
    }


    private val adapterRecentRoom by lazy {
        RoomListAdapter(
            requireContext(),
            this,
            this,
            UserCredentialPreference.getPrefarences(requireContext()).userName,
            false
        )
    }

    private val adapterHotRoom by lazy {
        RoomListAdapter(
            requireContext(),
            this,
            this,
            UserCredentialPreference.getPrefarences(requireContext()).userName,
            false
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initHawk()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        return binding!!.root
    }

    companion object {
        fun newInstance(param1: String?, param2: String?): ChatFragment {
            return ChatFragment()
        }
    }


    var someEventListener: FriendListFragment.OnSomeEventListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        someEventListener = try {
            activity as FriendListFragment.OnSomeEventListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement onSomeEventListener")
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        //RoomListAdapter(requireContext(), this, this, rpLogin.username)
        prepareRecylerView()
        //getHotRoom(token)
        //getRecentRoom(token)


        observeAllRoom()
        observeFavouriteRoom()
        observeHotRoom()
        observeRecentRoom()
        observeMyRoom()

        observeUpdateRoom()
        observeDeleteRoom()
        observeMakeAsFavoriteRoom()
        //observeJoinChatRoom()

        expandableToggle = ChatRoomExpandableToggle()


        binding!!.searchRoomIv.setOnClickListener {

            val intent = Intent(requireContext(), SearchRoomActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        binding!!.addChatRoomIv.setOnClickListener {
            Log.d(TAG, "onActivityCreated: Called ")
            showDialog("")
        }


        binding!!.myChatRoomExpandableIv.setOnClickListener {

            val show = toggleLayout(
                !expandableToggle.myRoomExpand, it, binding!!.myChatRoomRv
            )
            expandableToggle.myRoomExpand = show
            getMyRoom(token)

        }

        binding!!.favouriteChatRoomExpandableIv.setOnClickListener {

            val show = toggleLayout(
                !expandableToggle.favouriteRoomExpand, it, binding!!.favouriteChatRoomRv
            )
            expandableToggle.favouriteRoomExpand = show
            getFavouriteRoom(token)

        }

        binding!!.recentRoomExpandableIv.setOnClickListener {
            val show = toggleLayout(
                !expandableToggle.recentRoomExpand, it, binding!!.recentChatRoomRv
            )
            expandableToggle.recentRoomExpand = show

            getRecentRoom(token)
            //setDataOnRecyclerView()
        }

        binding!!.officialRoomExpandableIv.setOnClickListener {
            val show = toggleLayout(
                !expandableToggle.officialRoomExpand, it, binding!!.officialChatRoomRv
            )
            expandableToggle.officialRoomExpand = show
            getAllRoom(token)
        }

        binding!!.hotRoomExpandableIv.setOnClickListener {

            val show = toggleLayout(
                !expandableToggle.hotRoomExpand, it, binding!!.hotChatRoomRv
            )
            expandableToggle.hotRoomExpand = show

            //setDataOnRecyclerView()
            getHotRoom(token)
        }


        binding!!.favouriteChatRoomRefreshIv.setOnClickListener {
            getFavouriteRoom(token)
        }

        binding!!.myChatRoomRefreshIv.setOnClickListener {
            getMyRoom(token)
        }


        binding!!.officialRoomRefreshIv.setOnClickListener {
            //Only Official Room
            getAllRoom(token)
        }

        binding!!.hotRoomRefreshIv.setOnClickListener {
            getHotRoom(token)
        }

        binding!!.recentRoomRefreshIv.setOnClickListener {
            getRecentRoom(token)
        }
    }


    private fun getFavouriteRoom(token: String) {
        lifecycleScope.launchWhenCreated {
            _favouriteRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getFavouriteRoom(token).let {
                    if (it.isSuccessful) {
                        _favouriteRoom.postValue(Resource.success(it.body()))
                    } else {
                        _favouriteRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _favouriteRoom.postValue(Resource.error("No Internet Connection", null))

        }
    }

    private fun observeFavouriteRoom() {
        _favouriteRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeFavouriteRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeFavouriteRoom: ${it.data!!.toString()}")
                    removeAllViews()
                    adapterFavourite.addItems(it.data)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeFavouriteRoom: error")
                }
            }
        })
    }

    private fun getMyRoom(token: String) {

        lifecycleScope.launchWhenCreated {
            _myRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getMyRoom(token).let {
                    if (it.isSuccessful) {
                        _myRoom.postValue(Resource.success(it.body()))
                    } else {
                        _myRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _myRoom.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun observeRecentRoom() {

        _recentRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRecentRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRecentRoom: ${it.data!!.toString()}")
                    removeAllViews()
                    adapterRecentRoom.addItems(it.data)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRecentRoom: error")
                }
            }
        })
    }

    private fun getRecentRoom(token: String) {

        lifecycleScope.launchWhenCreated {
            _recentRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getRecentRoom(token).let {
                    if (it.isSuccessful) {
                        _recentRoom.postValue(Resource.success(it.body()))
                    } else {
                        _recentRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _recentRoom.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun observeHotRoom() {

        _hotRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeHotRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeHotRoom: ${it.data!!.toString()}")
                    removeAllViews()
                    //hotRoom.clear()
                    adapterHotRoom.addItems(it.data)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeHotRoom: error")
                }
            }
        })
    }

    private fun getHotRoom(token: String) {

        lifecycleScope.launchWhenCreated {
            _hotRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getHotRoom(token).let {
                    if (it.isSuccessful) {
                        _hotRoom.postValue(Resource.success(it.body()))
                    } else {
                        _hotRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _hotRoom.postValue(Resource.error("No Internet Connection", null))

        }


    }


    private fun showDialog(title: String) {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.create_chat_room_pop_up, null)
        dialogBuilder.setView(dialogView)
        val alertDialog = dialogBuilder.create()
        val createButton = dialogView.findViewById(R.id.btn_submit) as AppCompatButton
        val cancelButton = dialogView.findViewById(R.id.cancel_button) as AppCompatButton
        val etName = dialogView.findViewById<EditText>(R.id.etRoomName)

        if (title.isNotEmpty()) {
            createButton.text = "Update"
            etName.setText(title)
        }

        cancelButton.setOnClickListener {
            alertDialog.dismiss()
        }

        createButton.setOnClickListener {
            if (etName.text.isNullOrEmpty()) {
                return@setOnClickListener
            }

            if (title.isNotEmpty()) {
                updateRoomName(roomId, etName.text.toString(), false)
            } else {
                createNewChatRoom(etName.text.toString(), false)
            }

            alertDialog.dismiss()
        }

        alertDialog.show()

    }

    private fun createNewChatRoom(roomName: String, isPrivate: Boolean) {

        lifecycleScope.launchWhenCreated {
            _createRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.createRoom(token, roomName, isPrivate).let {
                    if (it.isSuccessful) {
                        _createRoom.postValue(Resource.success(it.body()))
                        Toast.makeText(requireContext(), "Room Created", Toast.LENGTH_SHORT).show()
                    } else {
                        _createRoom.postValue(Resource.error(it.errorBody().toString(), null))
                        Toast.makeText(
                            requireContext(),
                            "Room limit exit.You can join maximum 5 rooms. Leave a room to create this room",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            } else
                _createRoom.postValue(Resource.error("No Internet Connection", null))

        }


    }

    private fun deleteRooms(id: Int) {

        lifecycleScope.launchWhenCreated {
            _deleteRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.deleteRoom(token, id).let {
                    if (it.isSuccessful) {
                        _deleteRoom.postValue(Resource.success(it.body()))
                    } else {
                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            var errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java
                                )
                                ProgressBar.showInfoDialog(requireContext(), errorMsg.error)

                            } catch (e: IOException) {

                            }

                        }

                        _deleteRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _deleteRoom.postValue(Resource.error("No Internet Connection", null))

        }


    }


    private fun getAllRoom(token: String) {
        officialRoom.clear()
        //unOfficialRoom.clear()

        lifecycleScope.launchWhenCreated {
            _allRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getAllRoom(token).let {
                    if (it.isSuccessful) {
                        _allRoom.postValue(Resource.success(it.body()))
                    } else {
                        _allRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _allRoom.postValue(Resource.error("No Internet Connection", null))

        }


    }

    private fun updateRoomName(id: Int, roomName: String, isPrivate: Boolean) {

        lifecycleScope.launchWhenCreated {
            _updateRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updateRoom(token, id, roomName, isPrivate).let {
                    if (it.isSuccessful) {
                        _updateRoom.postValue(Resource.success(it.body()))
                        Toast.makeText(requireContext(), "Room Updated", Toast.LENGTH_SHORT).show()
                    } else {
                        _updateRoom.postValue(Resource.error(it.errorBody().toString(), null))
                        Toast.makeText(requireContext(), "Update Failed", Toast.LENGTH_SHORT).show()
                    }
                }
            } else
                _updateRoom.postValue(Resource.error("No Internet Connection", null))

        }


    }


    private fun observeMyRoom() {

        _myRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRecentRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRecentRoom: ${it.data!!.toString()}")
                    removeAllViews()
                    adapterMyRoom.addItems(it.data)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRecentRoom: error")
                }
            }
        })
    }


    private fun observeDeleteRoom() {
        _deleteRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeMyRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeMyRoom: ${it.data!!.toString()}")
                    getFavouriteRoom(token)
                    getMyRoom(token)
                    getRecentRoom(token)
                    getAllRoom(token) //Official Room
                    getHotRoom(token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeMyRoom: error")
                }
            }
        })
    }

    private fun observeAllRoom() {
        _allRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAllRoom: ${it.data!!.toString()}")
                    removeAllViews()
                    officialRoom.clear()

                    for (item in it.data) {
                        if (item.isPrivate) {
                            officialRoom.add(item)
                        }
                    }
                    //adapter.addItems(unOfficialRoom)
                    adapterOffice.addItems(officialRoom)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllRoom: error")
                }
            }
        })
    }

    private fun observeUpdateRoom() {
        _updateRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeUpdateRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeUpdateRoom: ${it.data!!.toString()}")
                    getFavouriteRoom(token)
                    getMyRoom(token)
                    getRecentRoom(token)
                    getAllRoom(token) //Official Room
                    getHotRoom(token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeUpdateRoom: error")
                }
            }
        })
    }

    private fun prepareRecylerView() {

        binding!!.myChatRoomRv.layoutManager = LinearLayoutManager(context)
        binding!!.myChatRoomRv.hasFixedSize()
        binding!!.myChatRoomRv.adapter = adapterMyRoom

        binding!!.favouriteChatRoomRv.layoutManager = LinearLayoutManager(context)
        binding!!.favouriteChatRoomRv.hasFixedSize()
        binding!!.favouriteChatRoomRv.adapter = adapterFavourite

        binding!!.officialChatRoomRv.layoutManager = LinearLayoutManager(context)
        binding!!.officialChatRoomRv.hasFixedSize()
        binding!!.officialChatRoomRv.adapter = adapterOffice

        binding!!.hotChatRoomRv.layoutManager = LinearLayoutManager(context)
        binding!!.hotChatRoomRv.hasFixedSize()
        binding!!.hotChatRoomRv.adapter = adapterHotRoom

        binding!!.recentChatRoomRv.layoutManager = LinearLayoutManager(context)
        binding!!.recentChatRoomRv.hasFixedSize()
        binding!!.recentChatRoomRv.adapter = adapterRecentRoom
    }

    private fun removeAllViews() {
        binding!!.favouriteChatRoomRv.removeAllViews()
        binding!!.officialChatRoomRv.removeAllViews()
        binding!!.myChatRoomRv.removeAllViews()
        binding!!.recentChatRoomRv.removeAllViews()
        binding!!.hotChatRoomRv.removeAllViews()

    }

    private fun toggleLayout(isExpanded: Boolean, v: View, layoutExpand: RecyclerView): Boolean {
        Animations().toggleArrow(v, isExpanded)
        if (isExpanded) {
            Animations().expand(layoutExpand)
        } else {
            Animations().collapse(layoutExpand)
        }
        return isExpanded
    }

    fun initHawk() {
        Hawk.init(context).build()
    }

    override fun joinRoom(id: Int, roomName: String) {
    }

    override fun updateRoom(id: Int, roomName: String) {
        showDialog(roomName)
        roomId = id
    }

    override fun deleteRoom(id: Int) {
        deleteRooms(id)
    }

    override fun addAsFavorite(id: Int) {
        addAsFavoriteRoom(id)
    }

    override fun removeFromFavorite(id: Int) {
       removeFromFavoriteRoom(id)
    }

    private fun removeFromFavoriteRoom(id: Int) {


        lifecycleScope.launchWhenCreated {
            _deleteRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.removeFromFavoriteRoom(id, token).let {
                    if (it.isSuccessful) {
                        _deleteRoom.postValue(Resource.success(it.body()))
                    } else {
                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            var errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java
                                )
                                ProgressBar.showInfoDialog(requireContext(), errorMsg.error)

                            } catch (e: IOException) {

                            }

                        }

                        _deleteRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _deleteRoom.postValue(Resource.error("No Internet Connection", null))

        }

    }

    override fun didPressed(id: Int, name: String, username: String) {
        Log.d(TAG, "0: is Room True")
        Log.d(TAG, "didPressed: Room ID: $id Room Name: $name")
        roomName = name
        joinRoomChat(id, name)

    }

    private fun joinRoomChat(id: Int, name: String) {
        lifecycleScope.launchWhenCreated {
            _joinRoomChat.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.joinRoom(id, token).let {
                    if (it.isSuccessful) {
                        _joinRoomChat.postValue(Resource.success(it.body()))
                        Hawk.put(Constants.GROUP_ID, id)
                        Hawk.put(
                            Constants.CHAT_TAB,
                            ChatTab(name, true, isRoom = true, id = id)
                        )
                        someEventListener!!.someEvent(id, name, true)

                    } else {
                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            var errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java
                                )

                                if (Constants.ALREADY_IN_ROOM == errorMsg.error) {
                                    Hawk.put(Constants.GROUP_ID, id)
                                    Hawk.put(
                                        Constants.CHAT_TAB,
                                        ChatTab(name, true, isRoom = true, id = id)
                                    )
                                    someEventListener!!.someEvent(id, name, true)
                                } else {
                                    ProgressBar.showInfoDialog(requireContext(), errorMsg.error)
                                }

                            } catch (e: IOException) {

                            }

                        }

                        _joinRoomChat.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _joinRoomChat.postValue(Resource.error("No Internet Connection", null))

        }
    }


    private fun addAsFavoriteRoom(id: Int) {

        lifecycleScope.launchWhenCreated {
            _deleteRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.makeFavoriteRoom(id, token).let {
                    if (it.isSuccessful) {
                        _deleteRoom.postValue(Resource.success(it.body()))
                    } else {
                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            var errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java
                                )
                                ProgressBar.showInfoDialog(requireContext(), errorMsg.error)

                            } catch (e: IOException) {

                            }

                        }else{
                            Log.e(TAG, "addAsFavoriteRoom: Code: "+it.code()+ " ID: "+id )
                        }

                        _deleteRoom.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _deleteRoom.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun observeMakeAsFavoriteRoom() {
        _deleteRoom.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeMyRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeMyRoom: ${it.data!!.toString()}")

                    if (it.data.error.isNullOrEmpty()) { Snackbar.make(binding!!.mainContent, it.data.success!!,
                        Snackbar.LENGTH_SHORT).show()

                    } else {
                        Snackbar.make(binding!!.mainContent, it.data.error, Snackbar.LENGTH_SHORT).show()
                    }
                    getFavouriteRoom(token)
                }

                Status.ERROR -> {
                    Log.e(TAG, "observeMyRoom: error"+it.message)
                }
            }
        })
    }

}