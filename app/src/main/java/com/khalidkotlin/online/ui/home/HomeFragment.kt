package com.khalidkotlin.online.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.home.post.PostActivity
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentHomeBinding
import com.khalidkotlin.online.ui.home.adapter.UserPostAdapter
import com.khalidkotlin.online.ui.home.model.*
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.ui.profile.ProfileFragment
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiEditText
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class HomeFragment : Fragment(), PostListener {
    // TODO: Rename and change types of
    private var binding: FragmentHomeBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper


    private var username = ""
    //private var username = ""
    private var token = ""

    private val _post = MutableLiveData<Resource<RpCreatePost>>()
    private val _postUpdate = MutableLiveData<Resource<RpNewsFeed>>()
    private val _newsFeed = MutableLiveData<Resource<List<RpNewsFeed>>>()
    private val _allReactions = MutableLiveData<Resource<List<RpReactions>>>()
    private val _deletePost = MutableLiveData<Resource<RpSuccess>>()
    private val _reaction = MutableLiveData<Resource<RpSuccess>>()
    private val _comment = MutableLiveData<Resource<RpComment>>()
    private val _createComment = MutableLiveData<Resource<RpCreateComment>>()
    private var reactionsList=ArrayList<RpReactions>()


    val posts: LiveData<Resource<RpCreatePost>>
        get() = _post

    val postsUpdate: LiveData<Resource<RpNewsFeed>>
        get() = _postUpdate

    val newsFeed: LiveData<Resource<List<RpNewsFeed>>>
        get() = _newsFeed

    val deletePost: LiveData<Resource<RpSuccess>>
        get() = _deletePost

    val comment: LiveData<Resource<RpComment>>
        get() = _comment

    val createComment: LiveData<Resource<RpCreateComment>>
        get() = _createComment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initHawk()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        shimmerLayout.startShimmer()
        prepareRecylerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        username = rpLogin.username!!
        token = "Token ${rpLogin.authToken}"
        getAllRecations("Token ${rpLogin.authToken}")
        fetchNewsFeed("Token ${rpLogin.authToken}")
        observeNewsfeed()
        observeAllReactions()
        observePost()
        observeUpdatePost()
        observeDeletePost()
        observeComment()
        observeAddReactions()

        binding!!.postLayout.setOnClickListener {
            startActivity(Intent(requireContext(),PostActivity::class.java))
        }
    }

    private fun observePost() {
        posts.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observePost: ${it.data!!.post}")
                    var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
                    fetchNewsFeed("Token ${rpLogin.authToken}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observePost: error")
                }
            }
        })
    }


    private fun observeUpdatePost() {
        postsUpdate.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeUpdatePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeUpdatePost: ${it.data!!.post}")
                    var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
                    fetchNewsFeed("Token ${rpLogin.authToken}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeUpdatePost: error")
                }
            }
        })
    }

    private fun observeDeletePost() {
        deletePost.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeDeletePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeDeletePost: ${it.data!!.success}")
                    var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
                    fetchNewsFeed("Token ${rpLogin.authToken}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeDeletePost: error")
                }
            }
        })
    }


    private fun observeNewsfeed() {
        newsFeed.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeNewsfeed: loading......")

                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeNewsfeed: ${it.data!!.size}")
                    binding!!.userPostRv.adapter =
                        UserPostAdapter(it.data, username, requireContext(), this,reactionsList)
                    binding!!.userPostRv.visibility=View.VISIBLE
                    binding!!.shimmerLayout.visibility=View.INVISIBLE
                    shimmerLayout.stopShimmer()


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeNewsfeed: error")
                }
            }
        })
    }

    private fun observeAllReactions() {
        _allReactions.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllReactions: loading......")

                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAllReactions: ${it.data!!.toString()}")

                    reactionsList=it.data as ArrayList<RpReactions>
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllReactions: error")
                }
            }
        })
    }


    private fun observeAddReactions() {
        _reaction.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAddReactions: loading......")

                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAddReactions: ${it.data!!.toString()}")

                    fetchNewsFeed(token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAddReactions: error")
                }
            }
        })
    }

    private fun observeComment() {

    }

    private fun createComment(id: Int, post: String, token: String) {
        lifecycleScope.launchWhenCreated {
            _createComment.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.createComment(token, id, post).let {
                    if (it.isSuccessful) {
                        _createComment.postValue(Resource.success(it.body()))
                    } else
                        _createComment.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _createComment.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun updatePost(post: String, token: String, id: Int) {
        lifecycleScope.launchWhenCreated {
            _postUpdate.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updatePost(token, id, post).let {
                    if (it.isSuccessful) {
                        _postUpdate.postValue(Resource.success(it.body()))
                    } else
                        _postUpdate.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _postUpdate.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun deletePost(token: String, id: Int) {
        lifecycleScope.launchWhenCreated {
            _deletePost.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.deletePost(token, id).let {
                    if (it.isSuccessful) {
                        _deletePost.postValue(Resource.success(it.body()))
                    } else
                        _deletePost.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _deletePost.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun fetchNewsFeed(token: String) {
        lifecycleScope.launchWhenCreated {
            _newsFeed.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getNewsFeed(token).let {
                    if (it.isSuccessful) {
                        _newsFeed.postValue(Resource.success(it.body()))
                    } else
                        _newsFeed.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _newsFeed.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun getAllRecations(token: String) {
        lifecycleScope.launchWhenCreated {
            _allReactions.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getAllReactions(token).let {
                    if (it.isSuccessful) {
                        _allReactions.postValue(Resource.success(it.body()))
                    } else
                        _allReactions.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _allReactions.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun addReactions(postId:Int,reactionId:Int) {
        lifecycleScope.launchWhenCreated {
            _reaction.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.addReactions(token,postId, reactionId).let {
                    if (it.isSuccessful) {
                        _reaction.postValue(Resource.success(it.body()))
                    } else
                        _reaction.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _reaction.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun fetchComment(id: Int, token: String) {
        lifecycleScope.launchWhenCreated {
            _comment.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getComment(id, token).let {
                    if (it.isSuccessful) {
                        _comment.postValue(Resource.success(it.body()))
                    } else
                        _comment.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _comment.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun prepareRecylerView() {
        // val exampleList = generateDummyList(50)
        //val exampleList2 = generateDummyList2(20)

        //Set Post RV
        binding!!.userPostRv.layoutManager = LinearLayoutManager(context)
        binding!!.userPostRv.hasFixedSize()

        //Set Active RV
        /**
         * Active User Recycler View Setup
        binding!!.activeUserRv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        binding!!.activeUserRv.hasFixedSize()
        binding!!.activeUserRv.adapter = ActiveUserAdapter(exampleList2)

         */

    }

    private fun displayBottomSheetFeedback(rpNewsFeed: RpNewsFeed) {
        val dialog = BottomSheetDialog(requireContext(), R.style.DialogStyle)
        dialog.setContentView(R.layout.layout_edit_post)
        dialog.setCancelable(true)


        val et_feedback = dialog.findViewById<EditText>(R.id.etBtmSheet);
        val btnSentFeedback = dialog.findViewById<Button>(R.id.btnUpdatePost)

        et_feedback!!.setText("" + rpNewsFeed.post)

        btnSentFeedback!!.setOnClickListener {

            if (et_feedback!!.text.isNullOrEmpty()) {
                requireContext().toast("Invalid input")
            } else {
                rpNewsFeed!!.id?.let { it1 ->
                    updatePost(
                        et_feedback!!.text.toString(),
                        token,
                        it1
                    )
                }
                dialog.dismiss()
                dialog.dismiss()
            }
        }
        dialog.show()
    }


    fun displayBottomComment(rpNewsFeed: RpNewsFeed) {
        val dialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialog)
        dialog.setContentView(R.layout.layout_bottom_comment)
        val singleList = ArrayList<String>()
        rpNewsFeed.id?.let { fetchComment(it, token) }

        for (i in 1..20) {
            singleList.add("item $i")
        }
        val userImage = dialog.findViewById<TextView>(R.id.user_img)
        val user_profile_name = dialog.findViewById<TextView>(R.id.user_profile_name)
        val userDesignation = dialog.findViewById<TextView>(R.id.user_designation)
        val userPost = dialog.findViewById<TextView>(R.id.post_tv)
        val loveCount = dialog.findViewById<TextView>(R.id.loveCount)
        val commentTv = dialog.findViewById<TextView>(R.id.commentCount)
        val shareCount = dialog.findViewById<TextView>(R.id.shareCount)
        val etComment = dialog.findViewById<EditText>(R.id.comment_et)
        val btnComment = dialog.findViewById<ImageView>(R.id.submit_comment_btn)
        val emojiIv = dialog.findViewById<ImageView>(R.id.emoji_iv)
        val rootView = dialog.findViewById<View>(R.id.root_view)

        user_profile_name!!.text = "${rpNewsFeed.author!!.username}"
        userDesignation!!.text = "${rpNewsFeed.author.username}"
        userPost!!.text = "${rpNewsFeed.post}"
        commentTv!!.text = "${rpNewsFeed.totalComments}"
        shareCount!!.text = "0"
        loveCount!!.text = "0"
        userImage!!.text = "${rpNewsFeed.author.username!![0]}"
        val listView = dialog.findViewById<View>(R.id.listViewBtmSheet) as BottomSheetListView?



        btnComment!!.setOnClickListener {
            if (etComment != null) {
                rpNewsFeed.id?.let { it1 -> createComment(it1,etComment.text.toString(), token) }
                etComment.setText("")
            }
        }

        /*
        val emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(etComment!!)
        /*emojiPopup.toggle() // Toggles visibility of the Popup.
        emojiPopup.dismiss() // Dismisses the Popup.
        emojiPopup.isShowing*/

        emojiIv!!.setOnClickListener {
            if (emojiPopup.isShowing){
                emojiPopup.dismiss()
                emojiIv.setImageResource(R.drawable.ic_emoji)
            }else{
                emojiPopup.toggle()
               emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }

         */

        comment.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeComment: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeComment: ${it.data!!}")
                    listView!!.adapter = BottomSheetCommentAdapter(
                        requireContext(), R.layout.item_bottom_comment,
                        it.data.comments as List<CommentsItem>
                    )


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeComment: error")
                }
            }
        })

        listView!!.setOnItemClickListener { adapterView, view, i, l ->


            dialog.dismiss()
        }

        createComment.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeComment: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeComment: ${it.data!!}")

                    dialog.dismiss()
                    fetchNewsFeed(token)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeComment: error")
                }
            }
        })



        dialog.show()
    }


    private fun initHawk() {
        Hawk.init(context).build()
    }


    companion object {
        fun newInstance(param1: String?, param2: String?): ProfileFragment {
            return ProfileFragment()
        }

        private const val TAG = "HomeFragment"
    }

    override fun editPost(rpNewsFeed: RpNewsFeed) {
        displayBottomSheetFeedback(rpNewsFeed)
    }

    override fun deletePost(rpNewsFeed: RpNewsFeed) {
        deletePost(token, rpNewsFeed.id!!)
    }

    override fun onPostClick(rpNewsFeed: RpNewsFeed) {
        displayBottomComment(rpNewsFeed)
    }

    override fun onReactions(rpNewsFeed: RpNewsFeed, id: Int) {
        Log.d(TAG, "onReactions: post_id: ${rpNewsFeed.id}")
        Log.d(TAG, "onReactions: reaction_id: $id")
        addReactions(rpNewsFeed.id!!,id)
    }

    override fun onUpdateRecations(rpNewsFeed: RpNewsFeed, id: Int) {

    }

}