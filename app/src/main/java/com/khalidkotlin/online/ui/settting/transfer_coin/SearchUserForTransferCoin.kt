package com.khalidkotlin.online.ui.settting.transfer_coin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivitySearchUserForTransferCoinBinding
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.ui.settting.transfer_coin.adapter.SearchUserForTransferCoinAdapter
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchUserForTransferCoin : AppCompatActivity(), RecyclerViewItemClickListener {

    private var binding: ActivitySearchUserForTransferCoinBinding? = null


    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val _searchFriend = MutableLiveData<Resource<List<RpSearchUser>>>()

    val friends: LiveData<Resource<List<RpSearchUser>>>
        get() = _searchFriend

    private val adapter by lazy {
        SearchUserForTransferCoinAdapter(this, this)
    }

    private var token = ""
    private var position = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_user_for_transfer_coin)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_search_user_for_transfer_coin)

        initHawk()
        prepareRecyclerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        observeSearchFriends()

        binding!!.searchPeopleTv.addTextChangedListener { text ->
            fetchSearchFriends(token, text.toString())
        }

        binding!!.toolbar.tvToolbarHeadline.text = "Search User"
        binding!!.toolbar.ivBackButton.setOnClickListener {
            startActivity(Intent(this, TransferCoinActivity::class.java))
            finish()
        }
    }

    private fun fetchSearchFriends(token: String, userName: String) {

        lifecycleScope.launchWhenCreated {
            _searchFriend.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getSearchUser(token, userName).let {
                    if (it.isSuccessful) {
                        _searchFriend.postValue(Resource.success(it.body()))
                        Log.d(
                            TAG, "fetchSearchFriends: Response: ${it.body().toString()}"
                        )
                    } else
                        _searchFriend.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _searchFriend.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun prepareRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding!!.friendListRv.layoutManager = layoutManager
        binding!!.friendListRv.hasFixedSize()
        binding!!.friendListRv.adapter = adapter
    }

    private fun observeSearchFriends() {
        _searchFriend.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    companion object {
        private const val TAG = "SearchUserForTransferCo"
    }

    override fun didPressed(id: Int, name: String, username: String) {
        Log.d(TAG, "didPressed: $id Username: $username")
        startActivity(
            Intent(
                this,
                TransferCoinActivity::class.java
            ).putExtra(Constants.NAME, username)
                .putExtra(Constants.RECEIVER_ID, id)
        )
        finish()
    }

}