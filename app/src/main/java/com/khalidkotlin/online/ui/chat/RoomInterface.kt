package com.khalidkotlin.online.ui.chat

interface RoomInterface {
    fun joinRoom(id:Int, roomName: String)
    fun updateRoom(id: Int,roomName:String)
    fun deleteRoom(id: Int)
    fun addAsFavorite(id: Int)
    fun removeFromFavorite(id: Int)
}