package com.khalidkotlin.online.ui.chat.adapter

import android.content.Context
import android.util.Log
import android.widget.PopupMenu
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewChatRoomNameBinding
import com.khalidkotlin.online.ui.chat.RoomInterface
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener
import com.orhanobut.hawk.Hawk

private const val TAG = "RoomListAdapter"

class RoomListAdapter(
    val context: Context, private val callBack: RecyclerViewItemClickListener,
    private val roomInterface: RoomInterface,
    val username: String?,
    private val fromFavorite: Boolean
) : BaseRecyclerViewAdapter<RpCreateRoom, ItemViewChatRoomNameBinding>() {

    //initHakw()
    //val rpLogin: RpLogin = Hawk.get(Constants.LOGIN_RP)

    override fun getLayout(): Int {
        return R.layout.item_view_chat_room_name
    }

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewChatRoomNameBinding>,
        position: Int
    ) {

        Log.d(TAG, "onBindViewHolder: Username: $username")


        holder.binding.roomName.text = "${items[position].roomName}"
        holder.binding.count.text =
            "(${items[position].totalMembers}/${items[position].maxMemberCapacity})"

        holder.binding.itemView.setOnClickListener {

            callBack.didPressed(
                items[position].id!!,
                items[position].roomName!!,
                items[position].createdBy!!.username!!
            )
        }


        holder.binding.imageView2.setOnClickListener {
            val menu = PopupMenu(context, it)
            if (username == items[position].createdBy!!.username) {
                //holder.binding.imageView2.visibility = View.VISIBLE
                menu.menu.add("Delete Room")
                menu.menu.add("Update Room")
            }
            if (fromFavorite) {
                menu.menu.add("Remove from favorite")
            }

            if (!fromFavorite) {
                menu.menu.add("Add as favorite")
            }


            menu.setOnMenuItemClickListener { item ->
                if (item!!.title == "Update Room") {

                    Log.e("TAG", "onMenuItemClick: Update")
                    roomInterface.updateRoom(items[position].id!!, items[position].roomName!!)

                } else if (item.title == "Add as favorite") {
                    roomInterface.addAsFavorite(items[position].id!!)

                } else if (item.title == "Delete Room") {
                    Log.e("TAG", "onMenuItemClick: delete")
                    roomInterface.deleteRoom(items[position].id!!)

                } else {
                    Log.d(TAG, "onBindViewHolder: Remove from favorite")
                    roomInterface.removeFromFavorite(items[position].id!!)
                }
                true
            }
            menu.show()
        }

    }

    private fun initHakw() {
        Hawk.init(context).build()
    }
}