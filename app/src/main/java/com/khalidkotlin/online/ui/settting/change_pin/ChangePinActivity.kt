package com.khalidkotlin.online.ui.settting.change_pin

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.settting.SettingActivity
import com.khalidkotlin.online.ui.settting.transfer_coin.TransferCoinActivity
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChangePinActivity : AppCompatActivity() {
    private var binding: com.khalidkotlin.online.databinding.ActivityChangePinBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""
    private var key = false

    private val _changePin = MutableLiveData<Resource<RpChangePin>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pin)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_pin)
        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!

        val intent = intent
        key = intent.getBooleanExtra("from_menu", false)
        Log.d(TAG, "onCreate: From Menu: $key")

        //observeSearchRoom()
        observeChangePin()

        binding!!.btnChangePin.setOnClickListener {
            closeKeyBoard()

            var currentPin = binding!!.currentPin.text.toString()
            var newPin = binding!!.newPin.text.toString()
            var confirmNewPin = binding!!.confirmNewPin.text.toString()

            Log.d(TAG, "onCreate: Current Pin: $currentPin New Pin: $newPin Confirm New Pin: $confirmNewPin"
            )

            if (currentPin.isNotEmpty() && newPin.isNotEmpty() && confirmNewPin.isNotEmpty()) {
                changePin(currentPin, newPin, confirmNewPin)
            } else {
                toast("Please fill all fields")
            }
        }
        binding!!.backIconIv.setOnClickListener {
            startActivity(Intent(this, SettingActivity::class.java))
            finish()
        }
    }

    private fun observeChangePin() {
        _changePin.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeSearchRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSearchRoom: ${it.data!!.toString()}")
                    //Hawk.put(Constants.IS_PIN_CHANGED, true)
                    //adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSearchRoom: error")
                }
            }
        })
    }

    private fun changePin(currentPin: String, newPin: String, confirmNewPin: String) {
        lifecycleScope.launchWhenCreated {
            _changePin.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.changePin(token, currentPin, newPin, confirmNewPin).let {
                    if (it.isSuccessful) {
                        _changePin.postValue(Resource.success(it.body()))

                        binding!!.currentPin.setText("")
                        binding!!.newPin.setText("")
                        binding!!.confirmNewPin.setText("")

                        val snack = Snackbar.make(
                            findViewById(android.R.id.content),
                            "Pin change successful!",
                            Snackbar.LENGTH_INDEFINITE
                        )
                        snack.setAction("Ok") {
                            if (key) {
                                startActivity(
                                    Intent(this@ChangePinActivity, TransferCoinActivity::class.java)
                                )
                            } else {
                                Log.d(TAG, "changePin: DO Nothing!!")
                            }

                        }
                        snack.show()
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                    } else {
                        _changePin.postValue(Resource.error(it.errorBody().toString(), null))
                        Log.d(TAG, "changePin: ${it.body().toString()}")
                        toast("Pin change failed")
                    }
                }
            } else
                _changePin.postValue(Resource.error("No Internet Connection", null))

        }
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "ChangePinActivity"
    }

    private fun closeKeyBoard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


}