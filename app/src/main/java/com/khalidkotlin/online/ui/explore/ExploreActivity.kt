package com.khalidkotlin.online.ui.explore

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityExploreBinding
import com.khalidkotlin.online.ui.gift_store.CommandActivity
import com.khalidkotlin.online.ui.gift_store.GiftStoreActivity
import com.khalidkotlin.online.ui.main.MainActivity

class ExploreActivity : AppCompatActivity() {

    private var binding: ActivityExploreBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_explore)

        binding!!.toolbar.tvToolbarHeadline.text = "Explore"

        binding!!.toolbar.ivBackButton.setOnClickListener {
           onBackPressed()
        }

        binding!!.giftStore.setOnClickListener {

            val intent = Intent(this, GiftStoreActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        binding!!.command.setOnClickListener {
            val intent = Intent(this, CommandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }


    }
}