package com.khalidkotlin.online.ui.gift_store

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityGiftStoreBinding
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.explore.ExploreActivity
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GiftStoreActivity : AppCompatActivity() {

    private val viewModel by viewModels<GiftStoreViewModel>()
    private var binding: ActivityGiftStoreBinding? = null
    private var token = ""
    private var paidGift = ArrayList<RpGift>()

    private val adapter by lazy {
        GiftStoreAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gift_store)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gift_store)

        binding!!.toolbar.tvToolbarHeadline.text = "Gift Store"


        initHawk()
        prepareRecyclerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"

        viewModel.fetchAllGift(token)
        observeAllGift()

        binding!!.toolbar.ivBackButton.setOnClickListener {
            val intent = Intent(this, ExploreActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }


    }


    private fun observeAllGift() {
        viewModel.gift.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    binding!!.giftLoadPb.visibility = View.VISIBLE
                    Log.e(TAG, "observeAllGift: loading......")
                }
                Status.SUCCESS -> {
                    binding!!.giftLoadPb.visibility = View.GONE
                    Log.e(TAG, "observeAllGift: ${it.data!!.toString()}")

                    paidGift.clear()

                    for (item in it.data) {
                        if (!item.isFree) {
                            paidGift.add(item)
                        }
                    }
                    adapter.addItems(paidGift)
                }
                Status.ERROR -> {
                    binding!!.giftLoadPb.visibility = View.GONE
                    Log.e(TAG, "observeAllGift: error")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun prepareRecyclerView() {
        val layoutManager = GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false)
        binding!!.rvGift.layoutManager = layoutManager
        binding!!.rvGift.hasFixedSize()
        binding!!.rvGift.adapter = adapter
    }

    companion object {
        private const val TAG = "GiftStoreActivity"
    }
}