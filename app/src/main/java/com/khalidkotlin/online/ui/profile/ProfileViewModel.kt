package com.khalidkotlin.online.ui.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch

class ProfileViewModel @ViewModelInject constructor(private val repository: Repository, private val networkHelper: NetworkHelper): ViewModel() {
    private val _profile= MutableLiveData<Resource<RpProfile>>()

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile


    private fun fetchProfile(token:String){
        viewModelScope.launch {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getProfile(token).let {
                    if (it.isSuccessful){
                        _profile.postValue(Resource.success(it.body()))
                    }
                    else
                        _profile.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _profile.postValue(Resource.error("No Internet Connection",null))
        }
    }


}