package com.khalidkotlin.online.ui.home.adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.home.model.ActiveUser
import kotlinx.android.synthetic.main.active_friend_item_view.view.*

class ActiveUserAdapter(private val exampleList2: List<ActiveUser>) :
    RecyclerView.Adapter<ActiveUserAdapter.ActiveUserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActiveUserViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.active_friend_item_view,
            parent, false
        )
        return ActiveUserViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ActiveUserViewHolder, position: Int) {
        val currentItem = exampleList2[position]

        if (position % 3 == 0) {
            holder.userImage.setImageResource(currentItem.userImage)
        } else {
            holder.userImage.setImageResource(currentItem.userImage)
        }

    }

    override fun getItemCount() = exampleList2.size
    class ActiveUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //val materialCardView: MaterialCardView = itemView.materialCardView
        val userImage: ImageView = itemView.user_profile_img
        //val postImage: ImageView = itemView.post_img
        //val userProfileName: TextView = itemView.user_profile_name
        // val userDesignation: TextView = itemView.user_designation
        //val userPost: TextView = itemView.post_tv
    }
}