package com.khalidkotlin.online.ui.group_chat

import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemPublicFriendsBinding
import com.khalidkotlin.online.ui.group_chat.models.MembersItem

class RoomInfoAdapter(val kickInterface: KickInterface) :BaseRecyclerViewAdapter<MembersItem,ItemPublicFriendsBinding>(){
    override fun getLayout()=R.layout.item_public_friends

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemPublicFriendsBinding>,
        position: Int
    ) {
        holder.binding.tvFullName.text=items[position].firstName+" "+items[position].lastName
        holder.binding.tvUsername.text=items[position].username
        holder.binding.btnAdd.text="Kick"
        holder.binding.btnAdd.setOnClickListener {
            kickInterface.kickMembers(items[position].id!!,position)
        }
    }

}