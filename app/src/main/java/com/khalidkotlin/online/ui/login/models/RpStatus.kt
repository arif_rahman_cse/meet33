package com.khalidkotlin.online.ui.login.models

import com.google.gson.annotations.SerializedName

data class RpStatus(

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("error")
    val error: String? = null,

    @field:SerializedName("detail")
    val detail: String? = null,


    )
