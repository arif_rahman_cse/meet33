package com.khalidkotlin.online.ui.home.post

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.github.dhaval2404.imagepicker.ImagePicker
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityPostBinding
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.toast
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.google.GoogleEmojiProvider
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


@AndroidEntryPoint
class PostActivity : AppCompatActivity() {

    private val viewModel by viewModels<PostViewModel>()
    private var binding: ActivityPostBinding? = null
    private var isImageSelect = false
    private lateinit var imagesFile: File
    private var token = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EmojiManager.install(GoogleEmojiProvider())
        setContentView(R.layout.activity_post)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_post)


        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!

        observePost()

        binding!!.backBtn.setOnClickListener {
            onBackPressed()
        }

        binding!!.btnPost.setOnClickListener {

            if (isImageSelect) {

                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), imagesFile)

                val body =
                    MultipartBody.Part.createFormData("attachment", imagesFile.name, requestFile)

                //viewModel.createPostWithImage(token, binding!!.inputPost.text.toString().trim(), body)

                if (binding!!.inputPost.text.isNullOrEmpty()) {
                    viewModel.createPostWithImage(token, "-", body)
                } else {
                    viewModel.createPostWithImage(
                        token,
                        binding!!.inputPost.text.toString().trim(),
                        body
                    )
                }


            } else {
                viewModel.createPost(token, binding!!.inputPost.text.toString())

            }

        }

        binding!!.addImageIv.setOnClickListener {
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

        //emoji start
        val emojiPopup =
            EmojiPopup.Builder.fromRootView(binding!!.rootView).build(binding!!.inputPost)
        binding!!.emojiIv.setOnClickListener {
            if (emojiPopup.isShowing) {
                emojiPopup.dismiss()
                binding!!.emojiIv.setImageResource(R.drawable.ic_emoji)
            } else {
                emojiPopup.toggle()
                binding!!.emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            //binding!!.attachmentShow.setImageURI(fileUri)
            //You can get File object from intent

            val file: File = ImagePicker.getFile(data)!!
            isImageSelect = true
            imagesFile = file
            binding!!.ivPost.setImageURI(fileUri)

            //viewModel.createPost(token, binding!!.inputPost.text.toString(), "public", new)
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun observePost() {
        viewModel.post.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    binding!!.loadingProgressBar.visibility = View.VISIBLE
                    Log.e(TAG, "observePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observePost: ${it.data!!.post}")
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    startActivity(Intent(this, MainActivity::class.java).putExtra(Constants.LOAD_HOME,true))
                    finish()


                }
                Status.ERROR -> {
                    binding!!.loadingProgressBar.visibility = View.INVISIBLE
                    toast("Network Error ")
                    Log.e(TAG, "observePost: error")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }


    companion object {
        private const val TAG = "PostActivity"
    }


}