package com.khalidkotlin.online.ui.profile

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentProfileBinding
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.ui.profile.adapter.MyPostAdapter
import com.khalidkotlin.online.ui.profile.models.RpMyPost
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : Fragment(), PostListener {

    private var binding: FragmentProfileBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val adapter by lazy {
        MyPostAdapter(requireContext(), this)
    }


    private val _profileUpdate = MutableLiveData<Resource<RpProfile>>()
    private val _profile = MutableLiveData<Resource<RpProfile>>()
    private val _myPost = MutableLiveData<Resource<List<RpMyPost>>>()
    private val _deletePost = MutableLiveData<Resource<RpSuccess>>()
    private val _postUpdate = MutableLiveData<Resource<RpNewsFeed>>()

    private val _updateProfileCover = MutableLiveData<Resource<RpProfile>>()


    private var token = ""

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    private val updateProfileCover: LiveData<Resource<RpProfile>>
        get() = _updateProfileCover

    private val myPost: LiveData<Resource<List<RpMyPost>>>
        get() = _myPost

    val deletePost: LiveData<Resource<RpSuccess>>
        get() = _deletePost

    val postsUpdate: LiveData<Resource<RpNewsFeed>>
        get() = _postUpdate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private var isProfileImage = false
    private var isCoverImage = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)

        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"
        fetchProfile("Token ${rpLogin.authToken}")
        prepareRecylerView()
        //fetchProfile("Token ${rpLogin.authToken}")
        fetchMyPost("Token ${rpLogin.authToken}")
        observeMyPost()
        observeProfile()
        //observeUpdateCover()
        observeUpdateProfile()
        observeDeletePost()
        observeUpdatePost()
        observeProfileBio()


        binding!!.circleImageView.setOnClickListener {
            isProfileImage = true
            //isCoverImage = false
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()

        }

        binding!!.addCoverIv.setOnClickListener {
            //isCoverImage = true
            isProfileImage = false
            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

        binding!!.bioEditIv.setOnClickListener {

            binding!!.bioEt.isFocusableInTouchMode = true
            binding!!.bioEt.isFocusable = true
            binding!!.bioEditIv.visibility = View.GONE
            binding!!.bioEditOkIv.visibility = View.VISIBLE
            //editTextMobile.setFocusable(true)
            //editTextMobile.setSelection(editTextMobile.getText().length)

        }

        binding!!.bioEditOkIv.setOnClickListener {

            binding!!.bioEt.isFocusableInTouchMode = false
            binding!!.bioEt.isFocusable = false
            binding!!.bioEditIv.visibility = View.VISIBLE
            binding!!.bioEditOkIv.visibility = View.GONE
            //editTextMobile.setFocusable(true)
            //editTextMobile.setSelection(editTextMobile.getText().length)

            if (binding!!.bioEt.text.toString().isNotEmpty()) {
                closeKeyBoard()
                updateProfileBio(binding!!.bioEt.text.toString())
            } else {
                Log.d(TAG, "onViewCreated: DO Nothing...")
            }

        }

    }

    private fun observeUpdateCover() {
        updateProfileCover.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfileCoverUpdate: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeProfileCoverUpdate: success........")

                    Log.d(
                        TAG,
                        "observeUpdateCover: ${Constants.IMAGE_BASE_URL + it.data!!.coverPhoto}"
                    )
                    Glide.with(requireActivity())
                        .load(Constants.IMAGE_BASE_URL + it.data!!.profilePicture)
                        .placeholder(R.drawable.placeholder_img)
                        .into(binding!!.addCoverIv)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfileCoverUpdate: error.....")
                }
            }
        })
    }

    private fun updateProfileBio(bio: String) {

        lifecycleScope.launchWhenCreated {
            _profileUpdate.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updateBio(token, bio).let {
                    if (it.isSuccessful) {
                        _profileUpdate.postValue(Resource.success(it.body()))
                        requireView().snackbar("Bio update successful! ")
                        //Toast.makeText(requireContext(), "Bio update successful! ", Toast.LENGTH_SHORT).show()
                    } else
                        _profileUpdate.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profileUpdate.postValue(Resource.error("No Internet Connection", null))
        }

    }

    private fun observeProfileBio() {
        profile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeProfile: success........")
                    Log.e(TAG, "observeProfile: ${it.data!!.firstName}")

                    if (it.data.bio == null) {
                        Log.d(TAG, "observeProfileBio: Nothing to set ")
                    } else {
                        Log.d(TAG, "observeProfileBio: BIO ET")
                        binding!!.bioEt.setText(it.data.bio.toString())
                    }

                    Log.d(TAG, "observeProfileBio: ${it.data.bio}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun observeMyPost() {
        myPost.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeProfile: success........")
                    Log.e(ContentValues.TAG, "observeAllRoom: ${it.data!!}")
                    //binding!!.myPostRv.adapter = MyPostAdapter(it.data, requireContext(), this)
                    adapter.addItems(it.data)
                    //binding!!.userProfileNameTv.text = "${it.data.firstName} ${it.data.lastName}"
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun fetchMyPost(token: String) {
        lifecycleScope.launchWhenCreated {
            _myPost.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getMyPost(token).let {
                    if (it.isSuccessful) {
                        _myPost.postValue(Resource.success(it.body()))
                    } else
                        _myPost.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _myPost.postValue(Resource.error("No Internet Connection", null))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK

            if (isProfileImage) {
                val fileUri = data?.data
                binding!!.circleImageView.setImageURI(fileUri)
                //You can get File object from intent
                val file: File = ImagePicker.getFile(data)!!
                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
                // MultipartBody.Part is used to send also the actual file name
                val body = MultipartBody.Part.createFormData("profile_picture", file.name, requestFile)
                updateProfilePic(body)
            } else {
                val fileUri = data?.data
                binding!!.caverImage.setImageURI(fileUri)
                //You can get File object from intent
                val file: File = ImagePicker.getFile(data)!!
                val requestFile =
                    RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
                val body = MultipartBody.Part.createFormData("cover_photo", file.name, requestFile)
                updateProfileCover(body)
            }

            //You can also get File Path from intent
            //  val filePath:String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateProfileCover(file: MultipartBody.Part) {

        lifecycleScope.launchWhenCreated {
            _updateProfileCover.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updateCoverPic(token, file).let {
                    if (it.isSuccessful) {
                        _updateProfileCover.postValue(Resource.success(it.body()))
                        Toast.makeText(requireContext(), it.message(), Toast.LENGTH_SHORT).show()
                    } else
                        _updateProfileCover.postValue(Resource.error(it.errorBody().toString(), null))
                        //Toast.makeText(requireContext(), "Image upload Failed", Toast.LENGTH_SHORT).show()
                }
            } else
                _updateProfileCover.postValue(Resource.error("No Internet Connection", null))
        }

    }


    private fun observeProfile() {
        profile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //ProgressBar.showProgress(requireContext())
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {

                    Log.d(TAG, "observeProfile: " + it.message)
                    //ProgressBar.hideProgress()
                    Log.e(TAG, "observeProfile: success........")
                    Log.e(TAG, "observeProfile: ${it.data!!.firstName}")

                    binding!!.userProfileNameTv.text = it.data.username
                    binding!!.friendCount.text = it.data.friendCount.toString()
                    binding!!.postCount.text = it.data.totalPosts.toString()
                    binding!!.sentGiftCount.text = it.data.sentGiftCount.toString()
                    binding!!.receivedGiftCount.text = it.data.receivedGiftCount.toString()

                    Glide.with(requireActivity())
                        .load(Constants.IMAGE_BASE_URL + it.data.profilePicture)
                        .into(binding!!.circleImageView)

                    Glide.with(requireActivity())
                        .load(Constants.IMAGE_BASE_URL + it.data.coverPhoto)
                        .placeholder(R.drawable.placeholder_img)
                        .into(binding!!.caverImage)
                }
                Status.ERROR -> {
                    //ProgressBar.hideProgress()
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun observeUpdateProfile() {
        _profileUpdate.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeUpdateProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeUpdateProfile: success........")
                    Log.e(TAG, "observeUpdateProfile: ${it.data!!.firstName}")
                    // binding!!.userProfileNameTv.text="${it.data.firstName} ${it.data.lastName}"
                    //  Glide.with(context!!).load(Constants.IMAGE_BASE_URL+it.data.profilePicture).into(binding!!.circleImageView)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeUpdateProfile: error.....")
                }
            }
        })
    }

    private fun updateProfilePic(file: MultipartBody.Part) {
        lifecycleScope.launchWhenCreated {
            _profileUpdate.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updateProfilePic(token, file).let {
                    if (it.isSuccessful) {
                        Toast.makeText(requireContext(), it.message(), Toast.LENGTH_SHORT).show()
                        _profileUpdate.postValue(Resource.success(it.body()))
                    } else
                        _profileUpdate.postValue(Resource.error(it.errorBody().toString(), null))
                        //Toast.makeText(requireContext(), "Image upload Failed", Toast.LENGTH_SHORT).show()
                        //Log.d(TAG, "updateProfilePic: "+it.headers().toString())
                        //Log.d(TAG, "updateProfilePic: "+it.body().toString())
                }
            } else
                _profileUpdate.postValue(Resource.error("No Internet Connection", null))
        }
    }


    private fun fetchProfile(token: String) {
        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile(token).let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun initHawk() {
        Hawk.init(context).build()
    }

    private fun prepareRecylerView() {

        //Set Post RV
        binding!!.myPostRv.layoutManager = LinearLayoutManager(context)
        binding!!.myPostRv.hasFixedSize()
        binding!!.myPostRv.adapter = adapter


    }


    companion object {
        fun newInstance(param1: String?, param2: String?): ProfileFragment {
            return ProfileFragment()
        }

        private const val TAG = "ProfileFragment"
    }

    override fun editPost(rpMyPost: RpMyPost) {
        displayBottomSheetEditPost(rpMyPost)
    }

    override fun deletePost(rpMyPost: RpMyPost) {
        deletePost(token, rpMyPost.id!!)
    }

    override fun onPostClick(rpMyPost: RpMyPost) {
        TODO("Not yet implemented")
    }

    override fun onReactions(rpMyPost: RpMyPost, id: Int) {
        TODO("Not yet implemented")
    }

    override fun onUpdateRecations(rpMyPost: RpMyPost, id: Int) {
        TODO("Not yet implemented")
    }

    private fun deletePost(token: String, id: Int) {
        lifecycleScope.launchWhenCreated {
            _deletePost.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.deletePost(token, id).let {
                    if (it.isSuccessful) {
                        _deletePost.postValue(Resource.success(it.body()))
                    } else
                        _deletePost.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _deletePost.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeDeletePost() {
        deletePost.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeDeletePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeDeletePost: ${it.data!!.success}")
                    var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
                    fetchMyPost("Token ${rpLogin.authToken}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeDeletePost: error")
                }
            }
        })
    }

    private fun displayBottomSheetEditPost(rpMyPost: RpMyPost) {
        val dialog = BottomSheetDialog(requireContext(), R.style.DialogStyle)
        dialog.setContentView(R.layout.layout_edit_post)
        dialog.setCancelable(true)


        val et_feedback = dialog.findViewById<EditText>(R.id.etBtmSheet);
        val btnSentFeedback = dialog.findViewById<Button>(R.id.btnUpdatePost)

        et_feedback!!.setText("" + rpMyPost.post)

        btnSentFeedback!!.setOnClickListener {

            if (et_feedback!!.text.isNullOrEmpty()) {
                requireContext().toast("Invalid input")
            } else {
                rpMyPost!!.id?.let { it1 ->
                    updatePost(
                        et_feedback!!.text.toString(),
                        token,
                        it1
                    )
                }
                dialog.dismiss()
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun updatePost(post: String, token: String, id: Int) {
        lifecycleScope.launchWhenCreated {
            _postUpdate.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.updatePost(token, id, post).let {
                    if (it.isSuccessful) {
                        _postUpdate.postValue(Resource.success(it.body()))
                    } else
                        _postUpdate.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _postUpdate.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeUpdatePost() {
        postsUpdate.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeUpdatePost: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeUpdatePost: ${it.data!!.post}")
                    var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
                    fetchMyPost("Token ${rpLogin.authToken}")

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeUpdatePost: error")
                }
            }
        })
    }

    private fun closeKeyBoard() {
        val imm =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }


}