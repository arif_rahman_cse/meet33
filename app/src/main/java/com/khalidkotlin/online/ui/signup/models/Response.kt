package com.khalidkotlin.online.ui.signup.models

import com.google.gson.annotations.SerializedName

data class Response(

    @field:SerializedName("password")
    val password: List<String?>? = null,

    @field:SerializedName("email")
    val email: List<String?>? = null,

    @field:SerializedName("username")
    val username: List<String?>? = null,

    @field:SerializedName("verification_code")
    val verificationCode: List<String?>? = null
)
