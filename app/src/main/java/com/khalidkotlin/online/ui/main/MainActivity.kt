package com.khalidkotlin.online.ui.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.data.roomdb.AppDatabase
import com.khalidkotlin.online.data.roomdb.AppExecutors
import com.khalidkotlin.online.ui.account.AccountActivity
import com.khalidkotlin.online.ui.chat.*
import com.khalidkotlin.online.ui.chat.adapter.DynamicTabsAdapter
import com.khalidkotlin.online.ui.chat.models.DynamicTabModel
import com.khalidkotlin.online.ui.explore.ExploreActivity
import com.khalidkotlin.online.ui.friendlist.FriendListFragment
import com.khalidkotlin.online.ui.friends.FriendsActivity
import com.khalidkotlin.online.ui.login.SignInActivity
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.ChatTab
import com.khalidkotlin.online.ui.newsfeed.NewsFeedActivity
import com.khalidkotlin.online.ui.notification.NotificationActivity
import com.khalidkotlin.online.ui.profile.ProfileActivity
import com.khalidkotlin.online.ui.rewards.RewardsActivity
import com.khalidkotlin.online.ui.search_people.SearchPeopleActivity
import com.khalidkotlin.online.ui.settting.SettingActivity
import com.khalidkotlin.online.ui.settting.transfer_coin.TransferCoinActivity
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.ProgressBar
import com.khalidkotlin.online.utils.hideKeyboard
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    FriendListFragment.OnSomeEventListener, ChatContainerFragmentP.OnChatTabDeleteListener,
    ChatContainerFragment.OnChatTabDeleteListener,DynamicTabsAdapter.Callback {

    private val _logOut = MutableLiveData<Resource<RpSuccess>>()

    private val logOut: LiveData<Resource<RpSuccess>>
        get() = _logOut


    private var toggle: ActionBarDrawerToggle? = null
    private var binding: com.khalidkotlin.online.databinding.ActivityMainBinding? = null
    private var mDb: AppDatabase? = null
    private lateinit var tabs: List<ChatTab>
    private var getChatIdOnButtonClick: GetChatIdOnButtonClick? = null
    private var chatContainerFragmentP: ChatContainerFragmentP? = null
    private var chatContainerFragmentCount = 0
    private var chatContainerPFragmentCount = 0
    private var chatIdList : ArrayList<Int> = ArrayList()

    //New Structure variables

    lateinit var fragmentsAdapter: com.khalidkotlin.online.ui.chat.adapter.FragmentPagerAdapter

    lateinit var dynamicTabAdapter: DynamicTabsAdapter
    lateinit var dynamicTabData: ArrayList<DynamicTabModel>
    var isFirstRettrive: Boolean = true



    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private var token = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initHawk()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        mDb = AppDatabase.getInstance(applicationContext)

        observeLogOut()

        //new Structure init
        fragmentsAdapter = com.khalidkotlin.online.ui.chat.adapter.FragmentPagerAdapter(this,supportFragmentManager)
        binding!!.viewPager.offscreenPageLimit = 10
        binding!!.viewPager.adapter = fragmentsAdapter
//        binding!!.toolBar.tabs.setupWithViewPager(binding!!.viewPager)
        binding!!.toolBar.dynamicTabRecycler.hasFixedSize()
        binding!!.toolBar.dynamicTabRecycler.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        dynamicTabData = ArrayList()


        //Add init Fragments
        val friendListFragment = FriendListFragment()
        fragmentsAdapter.addFragment(friendListFragment,"Home",0)
        dynamicTabData.add(DynamicTabModel("Home",R.drawable.ic_home,true,1000))


        val homeFragment = ChatFragment()
        fragmentsAdapter.addFragment(homeFragment,"Chat Rooms",1)
        dynamicTabData.add(DynamicTabModel("Chat Rooms",R.drawable.ic_chat,false,1000))

        dynamicTabAdapter = DynamicTabsAdapter(dynamicTabData,this,this)

        fragmentsAdapter.notifyDataSetChanged()
        binding!!.toolBar.dynamicTabRecycler.adapter = dynamicTabAdapter

        binding!!.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                hideKeyboard()
                (0 until dynamicTabData.size).forEach {
                    dynamicTabData[it].isSelecteed = false
                }
                dynamicTabData[position].isSelecteed = true

                dynamicTabAdapter.notifyDataSetChanged()
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        retrieveTabs()
        isFirstRettrive = false

//        binding!!.toolBar.tabs.getTabAt(0)?.setIcon(R.drawable.ic_home)



        nav_view.setNavigationItemSelectedListener(this)
        toggle =
            ActionBarDrawerToggle(this, drawer_layout, R.string.open_drawer, R.string.close_drawer)
        drawer_layout.addDrawerListener(toggle!!)
        toggle!!.syncState()

        binding!!.navView.footer_item_1.setOnClickListener {

            logOutUser(token)

        }

        binding!!.toolBar.drawerIcon.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
        }



    }

    private fun logOutUser(token: String) {

        lifecycleScope.launchWhenCreated {
            _logOut.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.logOut(token).let {
                    if (it.isSuccessful) {
                        _logOut.postValue(Resource.success(it.body()))
                    } else
                        _logOut.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _logOut.postValue(Resource.error("No Internet Connection", null))
        }

    }

    private fun observeLogOut() {

        logOut.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    ProgressBar.showProgress(this, "Logout...")
                    Log.e(TAG, "observeLogOut: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeLogOut: ${it.data!!.success}")
                    ProgressBar.hideProgress()
                    //Delete All Credential
                    Hawk.deleteAll()
                    AppExecutors.getInstance().diskIO().execute {
                        mDb!!.chatTabDao().deleteAllTab()
                    }
                    startActivity(Intent(this, SignInActivity::class.java))
                    finish()

                }
                Status.ERROR -> {
                    ProgressBar.hideProgress()
                    Log.e(TAG, "observeLogOut: error.....")
                }
            }
        })
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        Log.e(TAG, "onNavigationItemSelected: this method is called")
        when (item.itemId) {
            R.id.profile -> {
                //supportFragmentManager.beginTransaction().replace(R.id.container, ProfileFragment()).commit()
                val intent = Intent(applicationContext, ProfileActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.accout -> {
                //startActivity(Intent(this, AccountActivity::class.java))
                val intent = Intent(applicationContext, AccountActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.friends -> {
                //startActivity(Intent(this, FriendsActivity::class.java))
                val intent = Intent(applicationContext, FriendsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.explore -> {
                //startActivity(Intent(this, ExploreActivity::class.java))
                val intent = Intent(applicationContext, ExploreActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }

            R.id.feed -> {
                //startActivity(Intent(this, ExploreActivity::class.java))
                val intent = Intent(applicationContext, NewsFeedActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }

            R.id.notification -> {
                //startActivity(Intent(this, NotificationActivity::class.java))
                val intent = Intent(applicationContext, NotificationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.search_people -> {
                //startActivity(Intent(this, SearchPeopleActivity::class.java))
                val intent = Intent(applicationContext, SearchPeopleActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.rewards -> {
                //startActivity(Intent(this, RewardsActivity::class.java))
                val intent = Intent(applicationContext, RewardsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            R.id.trans_coins -> {
                //startActivity(Intent(this, TransferCoinActivity::class.java))
                val intent = Intent(applicationContext, TransferCoinActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }


            R.id.settings -> {
                //startActivity(Intent(this, SettingActivity::class.java))
                val intent = Intent(applicationContext, SettingActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)

            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun someEvent(chatId: Int, name: String, isRoom: Boolean) {
        Constants.hideKeyboard(this)

        val tab = ChatTab(name, false, isRoom, chatId)
        for (i in tabs) {
            if (tab.id == i.id) {
                Log.d(TAG, "someEvent: Already tab is added")
                return
            }

        }

        Log.d(TAG, "someEvent: New Tab")
        AppExecutors.getInstance().diskIO().execute {
            mDb!!.chatTabDao().insertTab(tab)
            retrieveTabs()
        }
    }

    //Here is new method
    fun initInterface(fragmentCommunicator: GetChatIdOnButtonClick) {
        this.getChatIdOnButtonClick = fragmentCommunicator
    }

    private fun retrieveTabs() {

        AppExecutors.getInstance().diskIO().execute {
            tabs = mDb!!.chatTabDao().getAllTab()
            Log.d(TAG, "retrieveTabs: $tabs")


            runOnUiThread {
                for (singleTab in tabs){
                    if (!chatIdList.contains(singleTab.id)){
                        if (singleTab.isRoom) {

                            when(chatContainerFragmentCount){
                                0 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragment(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                1 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentTwo(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                2 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentThree(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                3 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentFour(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                4 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentFive(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                5 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentSix(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                6 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentSeven(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                                7 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentEight(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerFragmentCount++
                                }
                            }
                            fragmentsAdapter.notifyDataSetChanged()
                            chatIdList.add(singleTab.id)
                            dynamicTabData.add(DynamicTabModel(singleTab.title,R.drawable.ic_chat_tab,false,singleTab.id))
                            dynamicTabAdapter.notifyDataSetChanged()
                            if (!isFirstRettrive){

                                binding!!.viewPager.currentItem = fragmentsAdapter.count
                            }

                        } else {
                            Hawk.put(Constants.GROUP_ID, 0)
                            Hawk.put(Constants.RECEIVER_ID, singleTab.id)
                            chatContainerFragmentP = ChatContainerFragmentP(singleTab.id)
                            when(chatContainerPFragmentCount){
                                0 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentP(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                1 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPTwo(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                2 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPThree(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                3 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPFour(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                4 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPFive(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                5 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPSix(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                                6 -> {
                                    fragmentsAdapter.addFragment(ChatContainerFragmentPSeven(singleTab.id),singleTab.title,fragmentsAdapter.returnTabSize())
                                    chatContainerPFragmentCount++
                                }
                            }
                            fragmentsAdapter.notifyDataSetChanged()
                            chatIdList.add(singleTab.id)
                            dynamicTabData.add(DynamicTabModel(singleTab.title,R.drawable.ic_chat_tab,false,singleTab.id))
                            dynamicTabAdapter.notifyDataSetChanged()
                            if (!isFirstRettrive){

                                binding!!.viewPager.currentItem = fragmentsAdapter.count
                            }

                        }
                    }
                }
            }
        }

    }

    private fun checkTheIdIsAdded(id: Int): Boolean {
        (0 until dynamicTabData.size).forEach {
            if (dynamicTabData[it].chatId == id)
                return true
        }
        return false
    }

    override fun chatTabDeleteEvent(chatTab: Int) {
        Log.d(TAG, "chatTabDeleteEvent: Close Chat Tab Clicked")
        AppExecutors.getInstance().diskIO().execute {
            //mDb!!.chatTabDao().deleteTab(Hawk.get(Constants.CHAT_TAB))
            mDb!!.chatTabDao().deleteById(chatTab)

            runOnUiThread {
                for (i in 0 until dynamicTabData.size){
                    if (dynamicTabData[i].chatId == chatTab){
                        dynamicTabData.removeAt(i)
                        fragmentsAdapter.removeFragment(i)


                        dynamicTabAdapter.notifyDataSetChanged()
                        fragmentsAdapter.notifyDataSetChanged()
                        binding!!.viewPager.currentItem = i-1
                    }
                }
            }
            retrieveTabs()
        }

    }

    override fun onTabSelection(position: Int) {
        binding!!.viewPager.currentItem = position
        hideKeyboard()
    }



}