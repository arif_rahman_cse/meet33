package com.khalidkotlin.online.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.card.MaterialCardView
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.home.PostListener
import com.khalidkotlin.online.ui.home.model.RpReactions
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.utils.BottomSheetAdapter
import com.khalidkotlin.online.utils.BottomSheetListView
import com.khalidkotlin.online.utils.Constants
import kotlinx.android.synthetic.main.user_post_item_view.view.*

class UserPostAdapter(
    private val exampleList: List<RpNewsFeed>,
    private val username: String,
    private val context: Context,
    private val postListener: PostListener,
    private val reactionList: ArrayList<RpReactions>
) :
    RecyclerView.Adapter<UserPostAdapter.UserPostViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserPostViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.user_post_item_view,
            parent, false
        )
        return UserPostViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserPostViewHolder, position: Int) {
        val currentItem = exampleList[position]

        holder.userPost.visibility = View.VISIBLE
        //holder.materialCardView.visibility = View.GONE

        holder.userProfileName.text = "${currentItem.author!!.firstName + " " + currentItem.author!!.lastName}"
        holder.userDesignation.text = "${currentItem.author!!.username}"
        holder.userPost.text = currentItem.post!!.trim()
        holder.commentTv.text = "${currentItem.totalComments}"
        holder.shareCount.setText("0")
        holder.loveCount.text = "" + currentItem.totalReactions
        holder.userImage.text = "${currentItem.author.username!![0]}"

        if (username == currentItem.author.username) {
            holder.editImage.visibility = View.VISIBLE
            holder.editImage.setOnClickListener {
                displayBottomSheetCategories(currentItem)
            }
        }else{
            holder.editImage.visibility = View.INVISIBLE
        }

        if (currentItem.attachment != null) {
            holder.materialCardView.visibility = View.VISIBLE
            Glide.with(context).load(Constants.IMAGE_BASE_URL + currentItem.attachment)
                .placeholder(R.drawable.placeholder_img).into(holder.postImage)
        }else{
            holder.materialCardView.visibility = View.GONE
        }

        holder.commentTv.setOnClickListener {
            postListener.onPostClick(currentItem)
        }
        holder.commentImageView.setOnClickListener {
            postListener.onPostClick(currentItem)
        }

        holder.ivLike.setOnClickListener {
            postListener.onReactions(currentItem, 1)
        }
        holder.ivLove.setOnClickListener {
            postListener.onReactions(currentItem, 1)
        }

    }

    override fun getItemCount() = exampleList.size
    inner class UserPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val materialCardView: MaterialCardView = itemView.materialCardView
        val userImage: TextView = itemView.user_img
        val postImage: ImageView = itemView.post_img
        val commentImageView: ImageView = itemView.ivComment
        val ivLike: ImageView = itemView.ivLike
        val ivLove: ImageView = itemView.ivLove
        val editImage: ImageView = itemView.ivEdit
        val userProfileName: TextView = itemView.user_profile_name
        val userDesignation: TextView = itemView.user_designation
        val userPost: TextView = itemView.post_tv
        val commentTv: TextView = itemView.commentCount
        val loveCount: TextView = itemView.loveCount
        val likeCount: TextView = itemView.likeCount
        val shareCount: TextView = itemView.shareCount
    }

    fun displayBottomSheetCategories(rpNewsFeed: RpNewsFeed) {
        val dialog = BottomSheetDialog(context, R.style.BottomSheetDialog)
        dialog.setContentView(R.layout.layout_bottom_sheet)
        val singleList = ArrayList<String>()

        singleList.add("Edit")
        singleList.add("Delete")

        val listView = dialog.findViewById<View>(R.id.listViewBtmSheet) as BottomSheetListView?
        listView!!.adapter = BottomSheetAdapter(context, R.layout.item_bottom_sheet, singleList)
        listView.setOnItemClickListener { adapterView, view, i, l ->
            if (i == 0) {
                postListener.editPost(rpNewsFeed)
            } else {
                postListener.deletePost(rpNewsFeed)
                dialog.dismiss()
            }

            dialog.dismiss()
        }

        dialog.show()
    }

}
