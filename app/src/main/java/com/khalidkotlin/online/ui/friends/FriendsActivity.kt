package com.khalidkotlin.online.ui.friends

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayout
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityFriendsBinding
import com.khalidkotlin.online.ui.friends.friend.PublicFriendListFragment
import com.khalidkotlin.online.ui.friends.friend.RequestFriendListFragment
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendsActivity : AppCompatActivity() {

    private var binding: ActivityFriendsBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_friends)

        initHawk()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        binding!!.userUserFullName.text = rpLogin.firstName + " " + rpLogin.lastName

        val intent = intent
        val key = intent.getStringExtra("NotificationKey")

        if(key.equals("Notification")){
            supportFragmentManager.beginTransaction()
                .replace(R.id.friendContainer, RequestFriendListFragment())
                .commit()
            binding!!.tabLayout.getTabAt(1)!!.select()
        }else{
            supportFragmentManager.beginTransaction()
                .replace(R.id.friendContainer, PublicFriendListFragment())
                .commit()
            binding!!.tabLayout.getTabAt(0)!!.select()
        }

        binding!!.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                var position = tab!!.position

                if (position == 0) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.friendContainer, PublicFriendListFragment())
                        .commit()
                    Log.e(TAG, "onTabSelected: postion :$position")
                } else {
                    Log.e(TAG, "onTabSelected: postion :$position")
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.friendContainer, RequestFriendListFragment())
                        .commit()
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })

        binding!!.ivBackButton.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "FriendsActivity"
    }


}