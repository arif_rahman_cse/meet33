package com.khalidkotlin.online.ui.chat.models

import com.google.gson.annotations.SerializedName

data class RpChatError(

	@field:SerializedName("error")
	val error: String? = null
)
