package com.khalidkotlin.online.ui.profile

import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.ui.profile.models.RpMyPost

interface PostListener {
    fun editPost(rpMyPost: RpMyPost)
    fun deletePost(rpMyPost: RpMyPost)
    fun onPostClick(rpMyPost: RpMyPost)
    fun onReactions(rpMyPost: RpMyPost,id:Int)
    fun onUpdateRecations(rpMyPost: RpMyPost,id:Int)
}