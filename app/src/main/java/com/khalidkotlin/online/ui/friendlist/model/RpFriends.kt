package com.khalidkotlin.online.ui.friendlist.model

import com.google.gson.annotations.SerializedName

data class RpFriends(
	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("profile_picture")
	val profilePicture: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
