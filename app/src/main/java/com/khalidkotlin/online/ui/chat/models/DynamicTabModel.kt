package com.khalidkotlin.online.ui.chat.models

import android.graphics.drawable.Drawable

data class DynamicTabModel(var title: String, var icon: Int, var isSelecteed: Boolean = false,val chatId:Int)
