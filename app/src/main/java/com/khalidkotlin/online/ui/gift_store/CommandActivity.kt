package com.khalidkotlin.online.ui.gift_store

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityCommandBinding
import com.khalidkotlin.online.ui.explore.ExploreActivity
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CommandActivity : AppCompatActivity() {
    private var binding: ActivityCommandBinding? = null

    private val viewModel by viewModels<GiftStoreViewModel>()
    private var token = ""

    private var freeGift = ArrayList<RpGift>()

    private val adapter by lazy {
        CommandAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_command)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_command)
        binding!!.toolbar.tvToolbarHeadline.text = "Command"

        initHawk()
        prepareRecyclerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token ${rpLogin.authToken}"

        viewModel.fetchAllGift(token)
        observeAllGift()


        binding!!.toolbar.ivBackButton.setOnClickListener {
            val intent = Intent(this, ExploreActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }

    private fun observeAllGift() {
        viewModel.gift.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    binding!!.commandLoadPb.visibility = View.VISIBLE
                    Log.e(TAG, "observeAllGift: loading......")
                }
                Status.SUCCESS -> {
                    binding!!.commandLoadPb.visibility = View.GONE
                    Log.e(TAG, "observeAllGift: ${it.data!!.toString()}")
                    freeGift.clear()

                    for (item in it.data) {
                        if (item.isFree) {
                            freeGift.add(item)
                        }
                    }
                    adapter.addItems(freeGift)
                }
                Status.ERROR -> {
                    binding!!.commandLoadPb.visibility = View.GONE
                    Log.e(TAG, "observeAllGift: error")
                }
            }
        })
    }


    private fun initHawk() {
        Hawk.init(this).build()
    }

    private fun prepareRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false)
        binding!!.rvCommand.layoutManager = layoutManager
        binding!!.rvCommand.hasFixedSize()
        binding!!.rvCommand.adapter = adapter
    }

    companion object {
        private const val TAG = "CommandActivity"
    }

}