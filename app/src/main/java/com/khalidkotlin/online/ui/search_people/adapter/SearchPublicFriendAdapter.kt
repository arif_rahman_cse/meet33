package com.khalidkotlin.online.ui.search_people.adapter

import android.content.Context
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewSearchPublicFriendsBinding
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener

class SearchPublicFriendAdapter(private val context: Context, private val callBack: RecyclerViewItemClickListener) :BaseRecyclerViewAdapter<RpSearchUser,ItemViewSearchPublicFriendsBinding>() {
    override fun getLayout()=R.layout.item_view_search_public_friends
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewSearchPublicFriendsBinding>,
        position: Int
    ) {
        holder.binding.userProfileName.text=items[position].firstName+" "+items[position].lastName
        holder.binding.userUserName.text=items[position].username

        Glide.with(context).load(Constants.IMAGE_BASE_URL + items[position].profilePicture)
            .placeholder(R.drawable.developer).into(holder.binding.friendListUserImg)

        holder.binding.addFriendIv.setOnClickListener {
            items[position].userId?.let { it1 ->
                items[position].username?.let { it2 ->
                    callBack.didPressed(
                        it1,
                        position.toString(),
                        it2
                    )
                }
            }
        }
    }
}
