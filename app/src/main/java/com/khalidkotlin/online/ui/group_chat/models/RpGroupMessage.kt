package com.khalidkotlin.online.ui.group_chat.models

import com.google.gson.annotations.SerializedName

data class RpGroupMessage(

	@field:SerializedName("gift")
	val gift: Any? = null,

	@field:SerializedName("is_info_message")
	val isInfoMessage: Boolean? = null,

	@field:SerializedName("is_room_message")
	val isRoomMessage: Boolean? = null,

	@field:SerializedName("attachment")
	val attachment: Any? = null,

	@field:SerializedName("sender")
	val sender: Sender? = null,

	@field:SerializedName("send_date")
	val sendDate: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("is_gift_message")
	val isGiftMessage: Boolean? = null,

	@field:SerializedName("thread")
	val thread: Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("chat_room")
	val chatRoom: Int? = null
)

data class Sender(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
