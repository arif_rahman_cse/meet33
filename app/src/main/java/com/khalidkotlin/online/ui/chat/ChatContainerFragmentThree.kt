package com.khalidkotlin.online.ui.chat

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentChatContainerBinding
import com.khalidkotlin.online.ui.chat.adapter.MessageAdapter
import com.khalidkotlin.online.ui.chat.adapter.TopInfoAdapter
import com.khalidkotlin.online.ui.chat.models.*
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.GetChatIdOnButtonClick
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.main.models.ChatTab
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.single_chat.LiveChatAdapter
import com.khalidkotlin.online.utils.*
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.google.GoogleEmojiProvider
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.*
import okio.ByteString
import java.io.IOException
import java.util.*
import javax.inject.Inject
import kotlin.Boolean
import kotlin.collections.ArrayList


@AndroidEntryPoint
class ChatContainerFragmentThree(val chatId: Int) : Fragment(), GetChatIdOnButtonClick {

    private lateinit var emojiPopup: EmojiPopup

    //new Socket variable
    private lateinit var client: OkHttpClient
    private lateinit var listener: EchoWebSocketListener
    private lateinit var ws: WebSocket

    var chatHistory = ChatHistoryModel.instance
    var roomInfoCallHistory = RoomInforCallIdHolder.instance


    private var binding: FragmentChatContainerBinding? = null

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper


    var onChatTabDeleteListener: ChatContainerFragment.OnChatTabDeleteListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onChatTabDeleteListener = try {
            activity as ChatContainerFragment.OnChatTabDeleteListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement onSomeEventListener")
        }
    }

    private val _leaveRoom = MutableLiveData<Resource<RpSuccess>>()

    val leaveRoom: LiveData<Resource<RpSuccess>>
        get() = _leaveRoom

    private var token = ""


    private val chatAdapter by lazy {
        LiveChatAdapter(requireContext())
    }

    private val _profile = MutableLiveData<Resource<RpProfile>>()
    private val _roomInfo = MutableLiveData<Resource<RpRoomInfo>>()

    val profile: LiveData<Resource<RpProfile>>
        get() = _profile

    val roomInfo: LiveData<Resource<RpRoomInfo>>
        get() = _roomInfo


    private lateinit var messageAdapter: MessageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EmojiManager.install(GoogleEmojiProvider())
        initHawk()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = rpLogin.authToken.toString()
        Log.d(TAG, "onCreate: Room Chat ID: $chatId")

        hideKeyboard()
    }


    private fun addMessagetoHistory(chatId: Int, data: String) {
        if (data.contains("type")) {
            return
        } else {
            val gson = Gson() // Or use new GsonBuilder().create();
            val chatResponse = gson.fromJson(data, ChatResponseModel::class.java)

            if (chatResponse.id != 0) {
                if (chatHistory.size > 0 && isAlreadyAdded(chatId, chatHistory)) {
                    for (i in 0 until chatHistory.size) {
                        val id = chatHistory[i].chatId
                        if (id == chatId) {
                            chatHistory[i].message.add(chatResponse)
                            return
                        }
                    }

                } else {
                    val list: ArrayList<ChatResponseModel> = ArrayList()
                    list.add(chatResponse)
                    chatHistory.add(ChatHistoryModel(chatId, list))
                }
            }
        }


    }

    private fun isAlreadyAdded(chatId: Int, chatHistory: ArrayList<ChatHistoryModel>): Boolean {
        for (i in 0 until chatHistory.size) {
            if (chatHistory[i].chatId == chatId) {
                return true
            } else
                continue
        }
        return false
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chat_container, container, false)
        //emoji start
        emojiPopup =
            EmojiPopup.Builder.fromRootView(binding!!.rootView).build(binding!!.inputMessage)
        emojiPopup.dismiss()

        // Inflate the layout for this fragment
        messageAdapter =
            MessageAdapter(requireContext(), Hawk.get<RpLogin>(Constants.LOGIN_RP).username, object : MessageAdapter.AdapterUIOnClick{
                override fun userNameOnClick(userName: String?) {
                    binding!!.inputMessage.setText("$userName  ")
                }
            })
        (activity as MainActivity?)?.initInterface(object : GetChatIdOnButtonClick {
            override fun getId(id: Int) {

                messageAdapter.deleteAll()

                client.dispatcher.executorService.shutdown()
                val handler = Handler(Looper.getMainLooper())
                handler.postDelayed({ initWs() }, 1000)
            }

        })


        //socket call
        initWs()
        return binding!!.root
    }

    @SuppressLint("CheckResult")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding!!.inputMessage.requestFocus()

        prepareRecylerView()
        observeProfile()
        observeLeaveRoom()

        if (roomInfoCallHistory.isNotEmpty()){
            var isCallRoominfo = false
            for (i in 0 until roomInfoCallHistory.size){
                if (roomInfoCallHistory[i].roomId == chatId){

                }else{
                    isCallRoominfo = true
                }
            }
            if (isCallRoominfo || chatHistory.size<20){
                observeRoomInfo()
                fetchRoomInfo()
                roomInfoCallHistory.add(RoomInforCallIdHolder(chatId))
            }else
                loadHistoryData()
        }else{
            observeRoomInfo()
            fetchRoomInfo()
            roomInfoCallHistory.add(RoomInforCallIdHolder(chatId))
        }

        binding!!.chatMoreOptionsIv.setOnClickListener {
            Log.d(TAG, "onActivityCreated: Chat more options")
            showMoreOptions(binding!!.chatMoreOptionsIv)
        }


        binding!!.checkmark.setOnClickListener {
            if (binding!!.inputMessage.text.isNullOrEmpty()) {
                return@setOnClickListener
            }

            if (binding!!.inputMessage.text.toString().isNotEmpty()) {
//                return@setOnClickListener
                val messageFormat = SendMessageFormatKotlin(binding!!.inputMessage.text.toString())

                val mapper = ObjectMapper()
                var json = ""
                try {
                    json = mapper.writeValueAsString(messageFormat)
                    println("ResultingJSONstring = $json")
                } catch (e: JsonProcessingException) {
                    e.printStackTrace()
                }
                ws.send(json)
                binding!!.inputMessage.setText("")
            }

            Log.e(TAG, "onCreate: sending message")
        }


        binding!!.emojiIv.setOnClickListener {
            if (emojiPopup.isShowing) {
                emojiPopup.dismiss()
                binding!!.emojiIv.setImageResource(R.drawable.ic_emoji)
            } else {
                emojiPopup.toggle()
                binding!!.emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }

    }

    private fun loadHistoryData() {
        //load previouse data
        if (chatHistory.size > 0) {
            for (i in 0 until chatHistory.size) {
                if (chatHistory[i].chatId == chatId) {
                    val specificChat = chatHistory[i]
                    messageAdapter.loadChatHistory(specificChat)
                    break
                }
            }

        }
    }

    private fun fetchRoomInfo() {

        lifecycleScope.launchWhenCreated {
            _roomInfo.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.roomInfo("Token $token", chatId).let {
                    if (it.isSuccessful) {
                        _roomInfo.postValue(Resource.success(it.body()))
                    } else {
                        _roomInfo.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _roomInfo.postValue(Resource.error("No Internet Connection", null))
        }

    }

    private fun observeRoomInfo() {

        roomInfo.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRoomInfo: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRoomInfo: success........")
                    Log.e(TAG, "observeRoomInfo: ${it.data!!}")
                    val data: RpRoomInfo = it.data
                    try {
                        var members = ""
                        for (i in data.members!!.indices) {
                            members = concatenate(members, data.members[i]?.username + ", ")
                        }

                        messageAdapter.addMessageObject(
                            MessageWithTypeModel(
                                data.roomName,
                                "Welcome to ${data.roomName}"
                            )
                        )
                        if (it.data!!.isPrivate) {
                            messageAdapter.addMessageObject(
                                MessageWithTypeModel(
                                    data.roomName,
                                    "This room is managed by meet33"
                                )
                            )
                        } else {
                            messageAdapter.addMessageObject(
                                MessageWithTypeModel(
                                    data.roomName,
                                    "This room is managed by  ${data.createdBy?.username}"
                                )
                            )
                        }

                        messageAdapter.addMessageObject(
                            MessageWithTypeModel(
                                data.roomName,
                                "Currently in this room : $members"
                            )
                        )
                        if (!data.announcement.isNullOrEmpty()) {
                            messageAdapter.addMessageObject(
                                MessageWithTypeModel(
                                    "",
                                    data.announcement, "yellow"
                                )
                            )
                        }

                        loadHistoryData()


                        binding!!.chatMessageRecyclerView.smoothScrollToPosition(messageAdapter.itemCount)
                    } catch (e: Exception) {
                    }

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllGift: error.....")
                    loadHistoryData()
                }
            }
        })
    }

    private fun fetchProfile() {
        lifecycleScope.launchWhenCreated {
            _profile.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.getProfile("Token $token").let {
                    if (it.isSuccessful) {
                        _profile.postValue(Resource.success(it.body()))
                    } else {
                        _profile.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            } else
                _profile.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeProfile() {
        profile.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllGift: loading.......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAllGift: success........")
                    Log.e(TAG, "observeAllGift: ${it.data!!}")
                    ProgressBar.userAttentionPb(
                        requireContext(),
                        "Current Transferable balance is ${it.data.points}"
                    )
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllGift: error.....")
                }
            }
        })

    }

    private fun showMoreOptions(chatMoreOptionsIv: ImageView) {

        val popupMenu = androidx.appcompat.widget.PopupMenu(requireContext(), chatMoreOptionsIv)

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.room_info -> {
                    startActivity(Intent(requireContext(), RoomInfoActivity::class.java))
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.check_balance -> {
                    fetchProfile()
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.participants -> {
                    startActivity(Intent(requireContext(), ParticipantsActivity::class.java))
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.kick_user -> {
                    startActivity(Intent(requireContext(), KickUserActivity::class.java))
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()
                    //changeStatus(tvUsername, item.title)
                    true
                }
                R.id.leave_room -> {
                    leaveRoom(chatId, token)
                    //Toast.makeText(requireContext(), item.title, Toast.LENGTH_LONG).show()

                    //changeStatus(tvUsername, item.title)
                    true
                }
                else -> false
            }
        }

        popupMenu.inflate(R.menu.menu_chat_more_options)

        try {
            val fieldMPopup =
                androidx.appcompat.widget.PopupMenu::class.java.getDeclaredField("mPopup")
            fieldMPopup.isAccessible = true
            val mPopup = fieldMPopup.get(popupMenu)
            mPopup.javaClass
                .getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(mPopup, true)
        } catch (e: Exception) {
            Log.e("Main", "Error showing menu icons.", e)
        } finally {
            popupMenu.show()
        }

    }

    private fun observeLeaveRoom() {

        leaveRoom.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    //ProgressBar.showProgress(requireContext())
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeLeaveRoom: " + it.message)
                    Log.e(TAG, "observeLeaveRoom: success........")
                    Log.e(TAG, "observeLeaveRoom: ${it.data!!}")
                    onChatTabDeleteListener!!.chatTabDeleteEvent(chatId)


                }
                Status.ERROR -> {
                    //ProgressBar.hideProgress()
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun leaveRoom(chatId: Int, token: String) {

        lifecycleScope.launchWhenCreated {
            _leaveRoom.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.leaveRoom("Token $token", chatId).let {
                    if (it.isSuccessful) {
                        _leaveRoom.postValue(Resource.success(it.body()))
                    } else
                        if (it.code() == 403) {
                            val gson = GsonBuilder().create()
                            var errorMsg: RpChatError

                            try {
                                errorMsg = gson.fromJson(
                                    it.errorBody()!!.string(),
                                    RpChatError::class.java)
                                _leaveRoom.postValue(Resource.error(errorMsg.error.toString(), null))

                            } catch (e: IOException) {

                            }

                        }else{
                            _leaveRoom.postValue(Resource.error("Something Went Wrong", null))
                        }

                }
            } else
                _leaveRoom.postValue(Resource.error("No Internet Connection", null))
        }

    }


    private fun prepareRecylerView() {

        val layoutManager = LinearLayoutManager(requireContext())
//        layoutManager.stackFromEnd = true
        //layoutManager.scrollToPositionWithOffset(messageAdapter.itemCount, messageAdapter.itemCount)
        //Set Post RV
        binding!!.chatMessageRecyclerView.layoutManager = layoutManager
//        binding!!.chatMessageRecyclerView.hasFixedSize()
        binding!!.chatMessageRecyclerView.adapter = messageAdapter
        binding!!.chatMessageRecyclerView.itemAnimator = null


    }


    private fun initHawk() {
        Hawk.init(context).build()
    }

    companion object {
        private const val TAG = "ChatContainerFragment"
    }

    override fun getId(id: Int) {
        messageAdapter.deleteAll()
        //load previouse data
        if (chatHistory.size > 0) {
            for (i in 0 until chatHistory.size) {
                if (chatHistory[i].chatId == chatId) {
                    val specificChat = chatHistory[i]
                    messageAdapter.loadChatHistory(specificChat)
                    break
                }
            }

        }
        client.dispatcher.executorService.shutdown()
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({ initWs() }, 1000)
    }

    /*new Socket implememntation*/

    inner class EchoWebSocketListener : WebSocketListener() {
        private val NORMAL_CLOSURE_STATUS = 1000

        override fun onOpen(webSocket: WebSocket, response: Response) {}

        override fun onMessage(webSocket: WebSocket, text: String) {
            Log.d("ws", "Receiving : $text")

            activity!!.runOnUiThread(Runnable {
                addMessagetoHistory(chatId, text)
                messageAdapter.addMessage(text)
                binding!!.chatMessageRecyclerView.smoothScrollToPosition(messageAdapter.itemCount)
            })

        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
            Log.d("ws", "Receiving bytes : " + bytes.hex())
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null)
            Log.d("ws", "Closing : $code / $reason. Reconnecting web socket...")
            client.dispatcher.executorService.shutdown()
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({ initWs() }, 1000)
        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
            webSocket.close(NORMAL_CLOSURE_STATUS, null)
            Log.d("ws", "Error : $t. Reconnecting web socket...")
            client.dispatcher.executorService.shutdown()
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({ initWs() }, 1000)
        }

    }


    private fun initWs() {
        // Start the web socket
        val request: Request =
            Request.Builder().url("ws://meet33api.xyz/ws/chat/room/$chatId/$token/")
                .build()

        client = OkHttpClient()
        listener = EchoWebSocketListener()
        ws = client.newWebSocket(request, listener)
    }


}