package com.khalidkotlin.online.ui.settting.transfer_coin.models

import com.google.gson.annotations.SerializedName

data class RpTransferCoin(
	@field:SerializedName("pin")
	val pin: String? = null,

	@field:SerializedName("receiver_id")
	val receiverId: Int? = null,

	@field:SerializedName("coin")
	val coin: Int? = null
)

