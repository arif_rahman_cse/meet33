package com.khalidkotlin.online.ui.login.models

import com.google.gson.annotations.SerializedName

data class RpLogin(

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("auth_token")
	val authToken: String? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("username")
	val username: String? = null,




)
