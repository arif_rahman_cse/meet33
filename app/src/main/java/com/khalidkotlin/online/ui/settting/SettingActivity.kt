package com.khalidkotlin.online.ui.settting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.settting.change_pin.ChangePinActivity
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.ui.settting.change_password.ChangePasswordActivity
import com.khalidkotlin.online.ui.settting.forget_pin.ForgetPinActivity

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    private var binding: com.khalidkotlin.online.databinding.ActivitySettingBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting)

        binding!!.ivBackButton.setOnClickListener(this)
        binding!!.changePin.setOnClickListener(this)
        binding!!.forgetPin.setOnClickListener(this)
        binding!!.changePassword.setOnClickListener(this)


    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {

            R.id.change_password -> {
                Log.d(TAG, "onClick: Change password")
                val intent = Intent(this, ChangePasswordActivity::class.java)
                startActivity(intent)
            }


            R.id.change_pin -> {
                Log.d(TAG, "onClick: Change Pin")
                val intent = Intent(this, ChangePinActivity::class.java)
                startActivity(intent)
            }

            R.id.forget_pin -> {
                Log.d(TAG, "onClick: Forget Pin")
                val intent = Intent(this, ForgetPinActivity::class.java)
                startActivity(intent)
            }

            R.id.ivBackButton -> {
                Log.d(TAG, "onClick: Back btn")
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }

        }
    }

    companion object {
        private const val TAG = "SettingActivity"
    }
}