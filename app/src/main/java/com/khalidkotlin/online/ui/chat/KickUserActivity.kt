package com.khalidkotlin.online.ui.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityKickUserBinding
import com.khalidkotlin.online.ui.chat.adapter.KickUserAdapter
import com.khalidkotlin.online.ui.group_chat.GroupChatActivity
import com.khalidkotlin.online.ui.group_chat.GroupChatViewModel
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.khalidkotlin.online.utils.ProgressBar
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class KickUserActivity : AppCompatActivity(), KickUserInterface {

    private val viewModel by viewModels<GroupChatViewModel>()

    private var binding: ActivityKickUserBinding? = null
    private var token = ""
    var roomId = 0
    var position = 0

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val _roomInfo = MutableLiveData<Resource<RpRoomInfo>>()

    private val roomInfo: LiveData<Resource<RpRoomInfo>>
        get() = _roomInfo

    private val adapter by lazy {
        KickUserAdapter(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kick_user)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_kick_user)
        binding!!.toolBar.tvToolbarHeadline.text = "Kick User"

        initHawk()
        prepareRecyclerView()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        roomId = Hawk.get(Constants.GROUP_ID)

        observeKick()
        roomInfo(roomId, token)
        observeRoomInfo()


        binding!!.toolBar.ivBackButton.setOnClickListener {
            Log.d(TAG, "onCreate: Back")
            onBackPressed()
        }


    }

    private fun observeKick() {
        viewModel.kickMember.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeKick: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeKick: ${it.data!!.toString()}")
                    //showRoomInfo(it.data)
                    adapter.removeItems(position)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeKick: error")
                    Snackbar.make(
                        binding!!.rootView,
                        "Only admin can kick a member out of a room",
                        Snackbar.LENGTH_SHORT
                    ).show()

                }
            }
        })
    }




    private fun roomInfo(roomId: Int, token: String) {

        lifecycleScope.launchWhenCreated {
            _roomInfo.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.roomInfo(token, roomId).let {
                    if (it.isSuccessful) {
                        _roomInfo.postValue(Resource.success(it.body()))
                    } else
                        _roomInfo.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _roomInfo.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun observeRoomInfo() {
        roomInfo.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeProfile: ${it.data.toString()}")

                    for (item in it.data!!.members!!) {
                        Log.d(TAG, "observeRoomInfo: $item")
                        adapter.addSingleItems(item!!)
                    }


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }


    private fun prepareRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding!!.participantsRv.layoutManager = layoutManager
        binding!!.participantsRv.hasFixedSize()
        binding!!.participantsRv.adapter = adapter
    }


    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "KickUserActivity"
    }


    override fun kickUser(position: Int, userId: Int) {
        Log.d(TAG, "kickUser: Kick User Id: $userId")
        viewModel.kick(roomId, userId, token)
    }

}