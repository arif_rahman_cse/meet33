package com.khalidkotlin.online.ui.login

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.lelin.baseproject.utils.Resource
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpStatus
import com.khalidkotlin.online.utils.NetworkHelper
import kotlinx.coroutines.launch

class LoginViewModel @ViewModelInject constructor(
    private val repository: Repository,
    private val networkHelper: NetworkHelper
) : ViewModel() {
    private val _user = MutableLiveData<Resource<RpLogin>>()

    private val _sendCode = MutableLiveData<Resource<RpStatus>>()
    private val _changePassword = MutableLiveData<Resource<RpStatus>>()


    val user: LiveData<Resource<RpLogin>>
        get() = _user

    val sendCode: LiveData<Resource<RpStatus>>
        get() = _sendCode

    val changePassword: LiveData<Resource<RpStatus>>
        get() = _changePassword


    fun fetchLogin(userName: String, password: String) {
        viewModelScope.launch {
            _user.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.login(userName, password).let {
                    if (it.isSuccessful) {
                        _user.postValue(Resource.success(it.body()))
                    } else
                        _user.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _user.postValue(Resource.error("No Internet Connection", null))
        }
    }


    fun sendResetCode(email: String) {
        viewModelScope.launch {
            _sendCode.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.sendCode(email).let {
                    if (it.isSuccessful) {
                        _sendCode.postValue(Resource.success(it.body()))
                    } else
                        _sendCode.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _sendCode.postValue(Resource.error("No Internet Connection", null))
        }
    }

    fun changePassword(code: String, email: String, password: String, password2: String ) {
        viewModelScope.launch {
            _changePassword.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.confirmCode(code, email, password, password2).let {
                    if (it.isSuccessful) {
                        _changePassword.postValue(Resource.success(it.body()))
                    } else
                        _changePassword.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _changePassword.postValue(Resource.error("No Internet Connection", null))
        }
    }


}