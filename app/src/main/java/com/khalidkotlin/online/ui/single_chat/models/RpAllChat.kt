package com.khalidkotlin.online.ui.single_chat.models

import com.google.gson.annotations.SerializedName

data class RpAllChat(

	@field:SerializedName("date_updated")
	val dateUpdated: String? = null,

	@field:SerializedName("first_participant")
	val firstParticipant: String? = null,

	@field:SerializedName("second_participant")
	val secondParticipant: String? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("second_participant_id")
	val secondParticipantId: String? = null,

	@field:SerializedName("first_participant_id")
	val firstParticipantId: String? = null,

	@field:SerializedName("unseen_message_for_participant_1")
	val unseenMessageForParticipant1: Boolean? = null,

	@field:SerializedName("unseen_message_for_participant_2")
	val unseenMessageForParticipant2: Boolean? = null
)
