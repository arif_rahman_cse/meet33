package com.khalidkotlin.online.ui.chat.adapter

import android.content.Context
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewUserBinding
import com.khalidkotlin.online.databinding.NewChatViewBinding
import com.khalidkotlin.online.ui.chat.models.ChatResponseModel
import com.khalidkotlin.online.ui.group_chat.models.MembersItem
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo

class PersonalChatAdapter(
    private val context: Context,
) : BaseRecyclerViewAdapter<ChatResponseModel, NewChatViewBinding>() {
    override fun getLayout() = R.layout.new_chat_view
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<NewChatViewBinding>,
        position: Int
    ) {

        holder.binding.chatMessageUsername.text = items[position].sender.username
        holder.binding.chatMessageMessage.text = items[position].message

    }
}
