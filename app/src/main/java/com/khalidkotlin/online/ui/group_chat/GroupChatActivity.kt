package com.khalidkotlin.online.ui.group_chat

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.dhaval2404.imagepicker.ImagePicker
import com.khalidkotlin.online.R
import com.khalidkotlin.online.databinding.ActivityGroupChatBinding
import com.khalidkotlin.online.ui.friends.FriendsInterface
import com.khalidkotlin.online.ui.friends.adapter.AllFriendAdapter
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.group_chat.models.MembersItem
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.ProgressBar
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.EmojiPopup
import com.vanniktech.emoji.google.GoogleEmojiProvider
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import kotlin.concurrent.fixedRateTimer


@AndroidEntryPoint
class GroupChatActivity : AppCompatActivity(), KickInterface, FriendsInterface {

    private val viewModel by viewModels<GroupChatViewModel>()
    private var binding: ActivityGroupChatBinding? = null
    private var token = ""
    var groupId = 0
    var position = 0
    var memberPosition = 0

    private val adapter by lazy {
        GroupChatAdapter(this)
    }

    private val roomInfoAdapter by lazy {
        RoomInfoAdapter(this)
    }

    private val allUserAdapter by lazy {
        AllFriendAdapter(this, this)
    }

    private var giftArrayList = ArrayList<RpGift>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EmojiManager.install(GoogleEmojiProvider())
        setContentView(R.layout.activity_group_chat)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_group_chat)

        val actionBar = supportActionBar
        actionBar!!.title =  intent.getStringExtra(Constants.ROOM_NAME)

        val text: Spannable = SpannableString(actionBar.title)
        text.setSpan(
            ForegroundColorSpan(Color.WHITE),
            0,
            text.length,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        actionBar.title = text

        initHawk()
        prepareRecylerView()
        var rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        groupId = Hawk.get(Constants.GROUP_ID)

        viewModel.fetchAllGift(token)
        viewModel.fetchAllGroupMessage(groupId, token)

        observeAllGift()
        observeRoomMessage()
        observeLeaveRoom()
        observeRoomInfo()
        observeKick()
        observeAllUser()
        observeAddMember()
        observeProfile()
        observeRoomGiftMessage()

        fixedRateTimer("timer", false, 0, 5000) {
            this@GroupChatActivity.runOnUiThread {
                viewModel!!.fetchAllGroupMessage(groupId, token)
            }
        }

        binding!!.checkmark.setOnClickListener {

            if (binding!!.inputMessage.text.isNullOrEmpty()) {
                return@setOnClickListener
            }

            for (item in giftArrayList) {
                if (item.giftCommand!! == binding!!.inputMessage.text.toString()) {
                    viewModel.sendRoomGiftMessage(token, groupId, item.id!!)
                    binding!!.inputMessage.setText("")
                    break
                }
            }

            if (binding!!.inputMessage.text.toString().isNotEmpty()) {
                viewModel.sendGroupOnlyMessage(binding!!.inputMessage.text.toString(), groupId, token)
                binding!!.inputMessage.setText("")
            }



        }

        binding!!.attachmentIv.setOnClickListener {
            Log.d(TAG, "onCreate: attachment")

            ImagePicker.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
                .compress(1024)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

        //emoji start

        val emojiPopup = EmojiPopup.Builder.fromRootView(binding!!.rootView).build(binding!!.inputMessage)
        /*emojiPopup.toggle() // Toggles visibility of the Popup.
        emojiPopup.dismiss() // Dismisses the Popup.
        emojiPopup.isShowing*/

        binding!!.emojiIv.setOnClickListener {
            if (emojiPopup.isShowing){
                emojiPopup.dismiss()
                binding!!.emojiIv.setImageResource(R.drawable.ic_emoji)
            }else{
                emojiPopup.toggle()
                binding!!.emojiIv.setImageResource(R.drawable.ic_text_enable_24)
            }
        }


    }

    private fun observeRoomGiftMessage() {
        viewModel.sendRoomGiftMessage.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRoomGiftMessage: loading........")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeSendMessage: success and data is :${it!!.data}")
                    // chatAdapter.addItems(it.data!!)
                    Log.d(TAG, "observeGiftMessage: success!")
                    //viewModel.fetchAllGroupMessage(groupId, token)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeSendMessage: error......")
                }
            }
        })
    }

    private fun observeAllGift() {
        viewModel.gift.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllGift: loading......")
                }
                Status.SUCCESS -> {
                    //giftArrayList.clear()
                    Log.e(TAG, "observeAllGift: ${it.data!!.toString()}")

                    for (item in it.data) {
                        giftArrayList.add(item)
                    }
                    //adapter.addItems(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllGift: error")
                }
            }
        })
    }


    private fun observeProfile() {
        viewModel.profile.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    ProgressBar.userAttentionPb(
                        this,
                        "Current Transferable balance is ${it.data!!.points}")
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }


    private fun observeRoomMessage() {
        viewModel!!.groupMessage.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRoomMessage: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRoomMessage: ${it.data!!.toString()}")
                    adapter.addItems(it.data.reversed())
                    binding!!.chatMessageRecyclerView.smoothScrollToPosition(adapter.itemCount)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRoomMessage: error")
                }
            }
        })
    }


    private fun observeLeaveRoom() {
        viewModel!!.leaveRoom.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeLeaveRoom: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeLeaveRoom: ${it.data!!.toString()}")
                    finish()
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeLeaveRoom: error")
                }
            }
        })
    }


    private fun observeRoomInfo() {
        viewModel!!.roomInfo.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRoomInfo: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRoomInfo: ${it.data!!.toString()}")
                    showRoomInfo(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRoomInfo: error")
                }
            }
        })
    }

    private fun observeKick() {
        viewModel!!.kickMember.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeKick: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeKick: ${it.data!!.toString()}")
                    //showRoomInfo(it.data)
                    roomInfoAdapter.removeItems(position)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeKick: error")
                }
            }
        })
    }


    private fun observeAllUser() {
        viewModel!!.friends.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllUser: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAllUser: ${it.data!!.toString()}")
                    //showRoomInfo(it.data)
                    addMemberDialog(it.data)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllUser: error")
                }
            }
        })
    }


    private fun observeAddMember() {
        viewModel!!.addMembers.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAddMember: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAddMember: ${it.data!!.toString()}")
                    //showRoomInfo(it.data)
                    allUserAdapter.removeItems(memberPosition)
                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAddMember: error")
                }
            }
        })
    }


    private fun prepareRecylerView() {

        val layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        binding!!.chatMessageRecyclerView.layoutManager = layoutManager
        binding!!.chatMessageRecyclerView.adapter = adapter
        binding!!.chatMessageRecyclerView.smoothScrollToPosition(adapter.itemCount)


    }


    private fun showRoomInfo(rpRoomInfo: RpRoomInfo) {
        val dialogBuilder = AlertDialog.Builder(this)

        //val dialog = Dialog(this)
        //dialog.setContentView(R.layout.dialog_room_info)

        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_room_info, null)
        dialogBuilder.setView(dialogView)
        val dialog = dialogBuilder.create()

        val recyclerView = dialogView.findViewById<RecyclerView>(R.id.rvRoomInfo)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.hasFixedSize()
        recyclerView.adapter = roomInfoAdapter

        roomInfoAdapter.addItems(rpRoomInfo.members as List<MembersItem>)

        dialog.show()


    }

    private fun addMemberDialog(list: List<RpAllUser>) {
        //val dialogBuilder = AlertDialog.Builder(this)
        //val dialog = Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
        //dialog.setContentView(R.layout.dialog_room_info)
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_room_info, null)
        dialogBuilder.setView(dialogView)
        val dialog = dialogBuilder.create()

        val recyclerView = dialogView.findViewById<RecyclerView>(R.id.rvRoomInfo)
        recyclerView!!.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.hasFixedSize()
        recyclerView.adapter = allUserAdapter

        allUserAdapter.addItems(list)

        dialog.show()


    }


    private fun initHawk() {
        Hawk.init(this).build()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_group_chat, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.leaveRoom -> {
                Log.e(TAG, "onOptionsItemSelected: leave room")
                viewModel!!.leaveRoom(groupId, token)
            }
            R.id.addmember -> {
                viewModel!!.allFriend(token)
                Log.e(TAG, "onOptionsItemSelected: add member")
            }
            R.id.check_wallet -> {
                Log.e(TAG, "onOptionsItemSelected: Check Balance")
                viewModel.fetchProfile(token)
            }
            else -> {
                Log.e(TAG, "onOptionsItemSelected: room info")
                viewModel!!.roomInfo(groupId, token)
            }
        }
        return true
    }

    companion object {
        private const val TAG = "GroupChatActivity"
    }

    override fun kickMembers(memberId: Int, position: Int) {
        viewModel!!.kick(groupId, memberId, token)
    }

    override fun onAccept(id: Int, position: Int) {
    }

    override fun onReject(id: Int, position: Int) {
    }

    override fun onCancel(id: Int, position: Int) {
    }

    override fun onAdd(id: Int, position: Int) {
        val list = ArrayList<Int>()
        list.add(id)
        viewModel.addMembers(groupId, list, token)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            //binding!!.attachmentShow.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!

            val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
            // MultipartBody.Part is used to send also the actual file name
            val body = MultipartBody.Part.createFormData("attachment", file.name, requestFile)
            if(binding!!.inputMessage.text.toString().isEmpty()){
                //viewModel!!.sendGroupMessage(binding!!.inputMessage.text.toString(), groupId, token, body)
                viewModel!!.sendGroupMessage("", groupId, token, body)
                binding!!.inputMessage.setText("")
            }else{
                viewModel!!.sendGroupMessage(
                    binding!!.inputMessage.text.toString(),
                    groupId,
                    token,
                    body
                )
                binding!!.inputMessage.setText("")
            }

            //You can also get File Path from intent
            //  val filePath:String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


}