package com.khalidkotlin.online.ui.home.model

data class ActiveUser(
    val userImage: Int
)