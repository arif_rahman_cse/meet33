package com.khalidkotlin.online.ui.gift_store.models

import com.google.gson.annotations.SerializedName

data class RpGift(

	@field:SerializedName("gift_icon")
	val giftIcon: String? = null,

	@field:SerializedName("single_send_coin")
	val singleSendCoin: Double? = null,

	@field:SerializedName("is_free")
	val isFree: Boolean,

	@field:SerializedName("all_send_coin")
	val allSendCoin: Double? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("gift_command")
	val giftCommand: String? = null,

	@field:SerializedName("added_on")
	val addedOn: String? = null
)
