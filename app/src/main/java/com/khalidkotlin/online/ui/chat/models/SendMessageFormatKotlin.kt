package com.khalidkotlin.online.ui.chat.models

import kotlinx.serialization.SerialName

data class SendMessageFormatKotlin(@SerialName("message") val message : String)
