package com.khalidkotlin.online.ui.chat.models

import kotlinx.serialization.SerialName

data class ChatErrorResponseModel(@SerialName("error") val error : String)
