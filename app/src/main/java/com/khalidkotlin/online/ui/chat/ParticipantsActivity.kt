package com.khalidkotlin.online.ui.chat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.ActivityParticipantsBinding
import com.khalidkotlin.online.ui.chat.adapter.RoomParticipantsAdapter
import com.khalidkotlin.online.ui.gift_store.CommandActivity
import com.khalidkotlin.online.ui.group_chat.GroupChatViewModel
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ParticipantsActivity : AppCompatActivity(), ViewProfileInterface {

    //private val viewModel by viewModels<GroupChatViewModel>()
    private var binding: ActivityParticipantsBinding? = null
    private var token = ""
    var roomId = 0

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val _roomInfo = MutableLiveData<Resource<RpRoomInfo>>()

    private val roomInfo: LiveData<Resource<RpRoomInfo>>
        get() = _roomInfo


    private val adapter by lazy {
        RoomParticipantsAdapter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_participants)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_participants)
        binding!!.toolBar.tvToolbarHeadline.text = "Participants"

        initHawk()
        prepareRecyclerView()
        val rpLogin = Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token = "Token " + rpLogin.authToken!!
        roomId = Hawk.get(Constants.GROUP_ID)

        roomInfo(roomId, token)
        observeRoomInfo()


        binding!!.toolBar.ivBackButton.setOnClickListener {
            Log.d(TAG, "onCreate: Back")
            onBackPressed()
        }
    }

    private fun roomInfo(roomId: Int, token: String) {

        lifecycleScope.launchWhenCreated {
            _roomInfo.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()) {
                repository.roomInfo(token, roomId).let {
                    if (it.isSuccessful) {
                        _roomInfo.postValue(Resource.success(it.body()))
                    } else
                        _roomInfo.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else
                _roomInfo.postValue(Resource.error("No Internet Connection", null))
        }
    }

    private fun prepareRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding!!.participantsRv.layoutManager = layoutManager
        binding!!.participantsRv.hasFixedSize()
        binding!!.participantsRv.adapter = adapter
    }

    private fun observeRoomInfo() {
        roomInfo.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeProfile: loading.......")
                }
                Status.SUCCESS -> {
                    Log.d(TAG, "observeProfile: ${it.data.toString()}")

                    for (item in it.data!!.members!!) {
                        Log.d(TAG, "observeRoomInfo: $item")
                        adapter.addSingleItems(item!!)
                    }


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeProfile: error.....")
                }
            }
        })
    }

    private fun initHawk() {
        Hawk.init(this).build()
    }

    companion object {
        private const val TAG = "ParticipantsActivity"
    }

    override fun clickProfileId(userId: Int) {
        Log.d(TAG, "clickProfileId: Clicked UserId: $userId")
        val intent = Intent(this, ViewProfileActivity::class.java)
        intent.putExtra(Constants.VIEW_PROFILE, true)
        intent.putExtra(Constants.VIEW_PROFILE_ID, userId)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }


}