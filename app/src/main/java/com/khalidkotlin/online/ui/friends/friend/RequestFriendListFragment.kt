package com.khalidkotlin.online.ui.friends.friend

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.khalidkotlin.online.R
import com.khalidkotlin.online.data.repository.Repository
import com.khalidkotlin.online.databinding.FragmentRequestFriendListBinding
import com.khalidkotlin.online.ui.friends.FriendsInterface
import com.khalidkotlin.online.ui.friends.adapter.AllFriendsRequestAdapter
import com.khalidkotlin.online.ui.friends.models.RpFriendRequest
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.utils.Constants
import com.khalidkotlin.online.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import com.lelin.baseproject.utils.Status
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

@AndroidEntryPoint
class RequestFriendListFragment : Fragment(),FriendsInterface {

    private val _friends = MutableLiveData<Resource<List<RpFriendRequest>>>()
    private val _acceptRequest = MutableLiveData<Resource<RpSuccess>>()
    private val _rejectRequest = MutableLiveData<Resource<RpSuccess>>()
    private var binding:FragmentRequestFriendListBinding?=null
    private var token=""
    private var position=0


    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var networkHelper: NetworkHelper

    private val adapter by lazy {
        AllFriendsRequestAdapter(requireContext(), this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initHawk()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_request_friend_list,
            container,
            false
        )
        return binding!!.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        var rpLogin=Hawk.get<RpLogin>(Constants.LOGIN_RP)
        token="Token "+rpLogin.authToken!!

        prepareRecylerView()
        getAllFriendRequest(token)
        observeAllFriendList()
        observeAcceptFriendList()
        observeRejectFriendList()
    }

    private fun getAllFriendRequest(token: String){

        lifecycleScope.launchWhenCreated {
            _friends.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getFriendRequest(token).let {
                    if (it.isSuccessful){
                        _friends.postValue(Resource.success(it.body()))
                    }
                    else{
                        _friends.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            }
            else
                _friends.postValue(Resource.error("No Internet Connection", null))

        }

    }

    private fun acceptFriendRequest(token: String, id: Int){

        lifecycleScope.launchWhenCreated {
            _acceptRequest.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.acceptFriendRequest(id, token).let {
                    if (it.isSuccessful){
                        _acceptRequest.postValue(Resource.success(it.body()))
                    }
                    else{
                        _acceptRequest.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            }
            else
                _acceptRequest.postValue(Resource.error("No Internet Connection", null))

        }

    }


    private fun rejectFriendRequest(token: String, id: Int){

        lifecycleScope.launchWhenCreated {
            _rejectRequest.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.rejectFriendRequest(id, token).let {
                    if (it.isSuccessful){
                        _rejectRequest.postValue(Resource.success(it.body()))
                    }
                    else{
                        _rejectRequest.postValue(Resource.error(it.errorBody().toString(), null))
                    }
                }
            }
            else
                _rejectRequest.postValue(Resource.error("No Internet Connection", null))

        }

    }


    private fun observeAllFriendList(){
        _friends.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAllFriendList: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAllFriendList: ${it.data!!.toString()}")
                    adapter.addItems(it.data)


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAllFriendList: error")
                }
            }
        })
    }


    private fun observeAcceptFriendList(){
        _acceptRequest.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeAcceptFriendList: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeAcceptFriendList: ${it.data.toString()}")
                    adapter.removeItems(position)
                    //  context!!.toast(it.message!!)

                }
                Status.ERROR -> {
                    Log.e(TAG, "observeAcceptFriendList: error")
                }
            }
        })
    }

    private fun observeRejectFriendList(){
        _rejectRequest.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    Log.e(TAG, "observeRejectFriendList: loading......")
                }
                Status.SUCCESS -> {
                    Log.e(TAG, "observeRejectFriendList: ${it.data.toString()}")
                    adapter.removeItems(position)
                    // context!!.toast(it.message!!)


                }
                Status.ERROR -> {
                    Log.e(TAG, "observeRejectFriendList: error")
                }
            }
        })
    }


    private fun prepareRecylerView() {

        //Set Post RV
        binding!!.rvRequest.layoutManager = LinearLayoutManager(context)
        binding!!.rvRequest.hasFixedSize()
        binding!!.rvRequest.adapter = adapter



    }
    
    
    fun initHawk(){
        Hawk.init(context).build()
    }

    companion object {
        private const val TAG = "RequestFriendListFragme"
    }

    override fun onAccept(id: Int, position: Int) {
        acceptFriendRequest(token, id)
        this.position=position
    }

    override fun onReject(id: Int, position: Int) {
        rejectFriendRequest(token, id)
        this.position=position
    }

    override fun onCancel(id: Int, position: Int) {

    }

    override fun onAdd(id: Int, position: Int) {

    }




}