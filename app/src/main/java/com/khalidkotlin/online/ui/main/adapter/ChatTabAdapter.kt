package com.khalidkotlin.online.ui.main.adapter

import android.content.Context
import android.view.View
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewChatTabBinding
import com.khalidkotlin.online.ui.main.models.ChatTab


class ChatTabAdapter(
    private val context: Context,

) : BaseRecyclerViewAdapter<ChatTab, ItemViewChatTabBinding>() {
    override fun getLayout() = R.layout.item_view_chat_tab

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewChatTabBinding>,
        position: Int
    ) {
        if (!items[position].isOpen) {
            holder.binding.chatTabNameTv.visibility = View.GONE
        } else {
            holder.binding.chatTabNameTv.visibility = View.VISIBLE
            holder.binding.chatTabNameTv.text = items[position].title

            /*val animate = TranslateAnimation(
                0F,
                holder.binding.chatTabNameTv.width.toFloat(),
                0F,
                0F
            )
            animate.duration = 500
            animate.fillAfter = true
            holder.binding.chatTabNameTv.startAnimation(animate)*/

        }

        holder.binding.chatTab.setOnClickListener {
            changeOpenStatus(position)
        }
    }

    private fun changeOpenStatus(position: Int) {
        (0 until items.size).forEach {
            items[it].isOpen = it == position
        }
    }


}
