package com.khalidkotlin.online.ui.home.model

import com.google.gson.annotations.SerializedName

data class RpReactions(

	@field:SerializedName("reaction_name")
	val reactionName: String? = null,

	@field:SerializedName("reaction_description")
	val reactionDescription: Any? = null,

	@field:SerializedName("reaction_command")
	val reactionCommand: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("added_on")
	val addedOn: String? = null,

	@field:SerializedName("reaction_icon")
	val reactionIcon: String? = null
)
