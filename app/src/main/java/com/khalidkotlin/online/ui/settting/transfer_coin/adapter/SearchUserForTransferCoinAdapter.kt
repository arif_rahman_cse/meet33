package com.khalidkotlin.online.ui.settting.transfer_coin.adapter

import android.content.Context
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewUserBinding
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener

class SearchUserForTransferCoinAdapter(
    private val context: Context,
    private val callBack: RecyclerViewItemClickListener
) : BaseRecyclerViewAdapter<RpSearchUser, ItemViewUserBinding>() {
    override fun getLayout() = R.layout.item_view_user
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewUserBinding>,
        position: Int
    ) {
        holder.binding.tvUsername.text = items[position].username

        holder.binding.usernameLo.setOnClickListener {
            items[position].userId?.let { it1 ->
                items[position].username?.let { it2 ->
                    callBack.didPressed(
                        it1,
                        position.toString(),
                        it2
                    )
                }
            }
        }
    }
}
