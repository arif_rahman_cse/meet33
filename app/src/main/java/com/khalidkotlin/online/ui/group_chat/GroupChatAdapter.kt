package com.khalidkotlin.online.ui.group_chat

import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.LayoutChatMessageListItemBinding
import com.khalidkotlin.online.ui.group_chat.models.RpGroupMessage
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk

class GroupChatAdapter(val context: Context) :
    BaseRecyclerViewAdapter<RpGroupMessage, LayoutChatMessageListItemBinding>() {

    var allChat = Hawk.get<RpLogin>(Constants.LOGIN_RP)
    override fun getLayout(): Int {
        hawk()
        return R.layout.layout_chat_message_list_item
    }

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<LayoutChatMessageListItemBinding>,
        position: Int
    ) {

        holder.binding.chatMessageUsername.text = "${items[position].sender!!.username}"

        if (items[position].message == null) {
            holder.binding.chatMessageMessage.visibility = View.GONE
        } else {
            holder.binding.chatMessageMessage.visibility = View.VISIBLE
            holder.binding.chatMessageMessage.text = "${items[position].message}"

            if (items[position].isGiftMessage!!) {
                holder.binding.chatMessageMessage.setTextColor(Color.parseColor("#FF8B00"))
            }else{
                holder.binding.chatMessageMessage.setTextColor(Color.parseColor("#7A8FA6"))
            }
        }


        if (items[position].attachment == null) {
            holder.binding.messageImg.visibility = View.GONE
        } else {
            holder.binding.messageImg.visibility = View.VISIBLE
            Glide.with(context)
                .load(Constants.IMAGE_BASE_URL + items[position].attachment)
                .placeholder(R.drawable.placeholder_img)
                .into(holder.binding.messageImg)
        }


    }


    private fun hawk() {
        Hawk.init(context).build()
    }
}