package com.khalidkotlin.online.ui.gift_store

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemGiftStoreBinding
import com.khalidkotlin.online.ui.gift_store.models.RpGift

class GiftStoreAdapter(val context: Context) :
    BaseRecyclerViewAdapter<RpGift, ItemGiftStoreBinding>() {
    override fun getLayout() = R.layout.item_gift_store

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemGiftStoreBinding>,
        position: Int
    ) {
        Glide.with(context).load(items[position].giftIcon).into(holder.binding.ivGift)
        holder.binding.giftName.text = items[position].giftCommand
        holder.binding.rerCoin.text = items[position].allSendCoin.toString() + " coins"
    }
}