package com.khalidkotlin.online.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.khalidkotlin.online.R
import com.khalidkotlin.online.ui.login.SignInActivity
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.main.MainActivity
import com.khalidkotlin.online.utils.Constants
import com.orhanobut.hawk.Hawk


class SplashScreen : AppCompatActivity() {

    // This is the loading time of the splash screen
    private val SPLASH_TIME_OUT:Long = 2000 // 1 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash_screen)
        initHawk()

        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity

            val rpLogin=Hawk.get<RpLogin>(Constants.LOGIN_RP)

            if (rpLogin!=null){
                startActivity(Intent(this, MainActivity::class.java))

            }
            else{
                startActivity(Intent(this, SignInActivity::class.java))

            }


            // close this activity
            finish()
        }, SPLASH_TIME_OUT)


    }

    private fun initHawk(){
        Hawk.init(this).build()
    }
}