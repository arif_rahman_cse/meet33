package com.khalidkotlin.online.ui.chat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.khalidkotlin.online.R;
import com.khalidkotlin.online.databinding.NewChatViewBinding;
import com.khalidkotlin.online.ui.chat.models.ChatResponseModel;

public class PersonalChatAdapterNew extends RecyclerView.Adapter<PersonalChatAdapterNew.MyViewHolder> {
    private static final String TAG = "PersonalChatAdapterNew";
    private Context context;
    private ChatResponseModel chatResponseModel;

    public PersonalChatAdapterNew(Context context, ChatResponseModel chatResponseModel) {
        this.context = context;
        this.chatResponseModel = chatResponseModel;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.new_chat_view, parent, false);
        NewChatViewBinding itemViewBinding = DataBindingUtil.bind(view);
        assert itemViewBinding != null;
        return new MyViewHolder(itemViewBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if(chatResponseModel != null){
            Log.d(TAG, "onBindViewHolder: Message: "+ chatResponseModel.getMessage());
            holder.binding.chatMessageUsername.setText(chatResponseModel.getSender().getUsername());
            holder.binding.chatMessageMessage.setText(chatResponseModel.getMessage());
        }


    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        NewChatViewBinding binding;

        //Constructor
        MyViewHolder(@NonNull NewChatViewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;

        }

    }

}
