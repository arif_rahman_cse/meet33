package com.khalidkotlin.online.ui.main

interface ChatTabClickInterface {

    fun onTabClick(id: Int, isOpen: Boolean, tabName: String, isRoom: Boolean)
}