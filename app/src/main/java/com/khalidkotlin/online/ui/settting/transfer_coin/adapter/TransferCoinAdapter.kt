package com.khalidkotlin.online.ui.settting.transfer_coin.adapter

import com.khalidkotlin.online.R
import com.khalidkotlin.online.base.BaseRecyclerViewAdapter
import com.khalidkotlin.online.databinding.ItemViewMyFriendsBinding
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.utils.RecyclerViewItemClickListener


class TransferCoinAdapter(private val callBack: RecyclerViewItemClickListener) :
    BaseRecyclerViewAdapter<RpSearchUser, ItemViewMyFriendsBinding>() {
    override fun getLayout() = R.layout.item_view_my_friends
    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<ItemViewMyFriendsBinding>,
        position: Int
    ) {
        holder.binding.friendListUserProfileName.text =
            items[position].firstName + " " + items[position].lastName
        holder.binding.friendListUserName.text = items[position].username
        holder.binding.userItem.setOnClickListener {
            items[position].userId?.let { it1 ->
                items[position].username?.let { it2 ->
                    callBack.didPressed(
                        it1,
                        position.toString(),
                        it2
                    )
                }
            }
        }
    }
}
