package com.khalidkotlin.online.data.roomdb

import androidx.room.*
import com.khalidkotlin.online.ui.main.models.ChatTab

@Dao
interface ChatTabDao {

    @Insert
    fun insertTab(chatTab: ChatTab)

    @Update
    fun updateTab(chatTab: ChatTab)

    @Delete
    fun deleteTab(chatTab: ChatTab)

    @Query("DELETE FROM chat_tab WHERE id = :chatId")
    fun deleteById(chatId: Int)

    @Query("delete from chat_tab")
    fun deleteAllTab()

    @Query("select * from chat_tab")
    fun getAllTab():List<ChatTab>

    /*
    @Query("select * from chat_tab order by priority desc")
    fun getAllNotes(): LiveData<List<ChatTab>>

     */
}