package com.khalidkotlin.online.data.dao

import androidx.room.Dao

@Dao
interface SoftMaxDao {
//    @Query("SELECT * FROM podcast")
//    fun loadPodcasts(): LiveData<List<PodcastEntry?>?>?
//
//    @Query("SELECT * FROM podcast WHERE podcast_id = :podcastId")
//    fun loadPodcastByPodcastId(podcastId: String?): LiveData<PodcastEntry?>?
//
//    @Insert
//    fun insertPodcast(podcastEntry: PodcastEntry?)
//
//    @Delete
//    fun deletePodcast(podcastEntry: PodcastEntry?)
//
//    @Query("SELECT * FROM favorite_episodes")
//    fun loadFavorites(): LiveData<List<FavoriteEntry?>?>?
//
//    @Query("SELECT * FROM favorite_episodes WHERE item_enclosure_url = :url")
//    fun loadFavoriteEpisodeByUrl(url: String?): LiveData<FavoriteEntry?>?
//
//    @Insert
//    fun insertFavoriteEpisode(favoriteEntry: FavoriteEntry?)
//
//    @Delete
//    fun deleteFavoriteEpisode(favoriteEntry: FavoriteEntry?)
//
//    // DownloadEntry
//    @Query("SELECT * FROM downloaded_episodes")
//    fun loadDownloads(): List<DownloadEntry>
//
//    @Query("SELECT * FROM downloaded_episodes WHERE item_enclosure_url = :enclosureUrl")
//    fun loadDownloadedEpisodeByEnclosureUrl(enclosureUrl: String?): LiveData<DownloadEntry?>?
//
//    @Insert
//    fun insertDownloadedEpisode(downloadEntry: DownloadEntry?)
//
//    @Delete
//    fun deleteDownloadedEpisode(downloadEntry: DownloadEntry?)
//
//    @Query("SELECT * FROM downloaded_episodes WHERE item_enclosure_url = :url")
//    fun syncLoadDownload(url: String?): DownloadEntry?
}