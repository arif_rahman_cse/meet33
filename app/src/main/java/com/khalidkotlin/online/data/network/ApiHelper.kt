package com.khalidkotlin.online.data.network

import com.khalidkotlin.online.data.demo.Episode
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.ui.settting.change_pin.RpChangePin
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.friendlist.model.RpFriends
import com.khalidkotlin.online.ui.friends.models.RpAllFriendList
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.ui.friends.models.RpFriendRequest
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.group_chat.models.RpAddMember
import com.khalidkotlin.online.ui.group_chat.models.RpGroupMessage
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.home.model.RpComment
import com.khalidkotlin.online.ui.home.model.RpCreateComment
import com.khalidkotlin.online.ui.home.model.RpReactions
import com.khalidkotlin.online.ui.single_chat.models.RpAllChat
import com.khalidkotlin.online.ui.single_chat.models.RpChat
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpStatus
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.ui.profile.models.RpMyPost
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.ui.send_gift.RpSendGift
import com.khalidkotlin.online.ui.settting.change_password.models.RpChangePassword
import com.khalidkotlin.online.ui.settting.forget_pin.models.RpForgetPin
import com.khalidkotlin.online.ui.settting.transfer_coin.models.RpTransferCoin
import com.khalidkotlin.online.ui.signup.models.RpCountries
import io.reactivex.Flowable
import okhttp3.MultipartBody
import retrofit2.Response

interface ApiHelper {

    suspend fun changePin(
        token: String,
        currentPin: String,
        newPin: String,
        confirmPin: String
    ): Response<RpChangePin>

    suspend fun getMyPost(token: String): Response<List<RpMyPost>>
    suspend fun transferCoin(
        token: String,
        receiverId: Int,
        coin: Int,
        pin: String
    ): Response<RpTransferCoin>

    suspend fun getPopular(): Response<List<Popular>>
    suspend fun getEpisode(): Response<List<Episode>>
    suspend fun login(userName: String, password: String): Response<RpLogin>
    suspend fun getAllCountries(): Response<List<RpCountries>>
    suspend fun sendVerificationCode(email: String): Response<RpLogin>
    suspend fun register(
        userName: String,
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        vCode: String,
        country: String,
        gender: String
    ): Response<RpLogin>

    suspend fun logOut(token: String): Response<RpSuccess>
    suspend fun sendCode(email: String): Response<RpStatus>
    suspend fun confirmCode(
        token: String,
        email: String,
        password: String,
        password2: String
    ): Response<RpStatus>

    suspend fun getProfile(token: String): Response<RpProfile>
    suspend fun getSingleProfile(token: String, id: Int): Response<RpProfile>
    suspend fun getAllChat(token: String): Response<List<RpAllChat>>
    suspend fun getFeed(token: String): Response<List<RpNewsFeed>>
    suspend fun updatePost(token: String, id: Int, post: String): Response<RpNewsFeed>
    suspend fun deletePost(token: String, id: Int): Response<RpSuccess>
    suspend fun getFriends(token: String): Response<List<RpFriends>>
    suspend fun getAllFriend(token: String): Response<List<RpAllFriendList>>
    suspend fun createPostWithImage(
        token: String,
        post: String,
        file: MultipartBody.Part
    ): Response<RpCreatePost>

    suspend fun createPost(token: String, post: String): Response<RpCreatePost>

    //suspend fun createPost(token:String, post:String, privacy: String):Response<RpCreatePost>
    //suspend fun createPostWithImage(post:String,privacy:String,file: MultipartBody.Part,token:String):Response<RpCreatePostWithImage>
    suspend fun getSingleChat(token: String, id: Int): Response<List<RpChat>>
    suspend fun sendMessage(token: String, receiver: Int, message: String): Response<List<RpChat>>
    suspend fun sendMessageWithImage(
        token: String,
        receiver: Int,
        message: String,
        file: MultipartBody.Part
    ): Response<List<RpChat>>

    suspend fun getComment(id: Int, token: String): Response<RpComment>
    suspend fun acceptFriendRequest(id: Int, token: String): Response<RpSuccess>
    suspend fun cancelFriendRequest(id: Int, token: String): Response<RpSuccess>
    suspend fun rejectFriendRequest(id: Int, token: String): Response<RpSuccess>
    suspend fun sendFriendRequest(id: Int, token: String): Response<RpSuccess>
    suspend fun unfriend(id: Int, token: String): Response<RpSuccess>
    suspend fun createComment(token: String, id: Int, comment: String): Response<RpCreateComment>
    suspend fun getFriendRequest(token: String): Response<List<RpFriendRequest>>
    suspend fun getAllUser(token: String): Response<List<RpAllUser>>
    suspend fun createRoom(
        token: String,
        roomName: String,
        isPrivate: Boolean
    ): Response<RpCreateRoom>

    suspend fun getAllRoom(token: String): Response<List<RpCreateRoom>>
    suspend fun getHotRoom(token: String): Response<List<RpCreateRoom>>
    suspend fun getMyRoom(token: String): Response<List<RpCreateRoom>>
    suspend fun getFavouriteRoom(token: String): Response<List<RpCreateRoom>>
    suspend fun getRecentRoom(token: String): Response<List<RpCreateRoom>>
    suspend fun getAllReactions(token: String): Response<List<RpReactions>>
    suspend fun getAllRoomMessage(token: String, id: Int): Response<List<RpGroupMessage>>
    suspend fun sendGroupMessage(
        token: String,
        roomId: Int,
        message: String,
        file: MultipartBody.Part
    ): Response<List<RpGroupMessage>>

    suspend fun sendGroupOnlyMessage(
        token: String,
        roomId: Int,
        message: String
    ): Response<List<RpGroupMessage>>

    suspend fun addReactions(token: String, postId: Int, reactionId: Int): Response<RpSuccess>
    suspend fun searchRoom(token: String, roomName: String): Response<List<RpCreateRoom>>
    suspend fun getAllGift(token: String): Response<List<RpGift>>

    suspend fun sendGift(token: String, receiverId: Int, giftId: Int): Response<List<RpSendGift>>
    suspend fun sendRoomGift(token: String, roomId: Int, giftId: Int): Response<List<RpSendGift>>

    suspend fun joinRoom(id: Int, token: String): Response<RpSuccess>
    suspend fun makeFavoriteRoom(id: Int, token: String): Response<RpSuccess>
    suspend fun removeFromFavoriteRoom(id: Int, token: String): Response<RpSuccess>
    suspend fun getSearchUser(token: String, keyWord: String): Response<List<RpSearchUser>>
    suspend fun updateProfilePic(token: String, file: MultipartBody.Part): Response<RpProfile>
    suspend fun updateCoverPic(token: String, file: MultipartBody.Part): Response<RpProfile>
    suspend fun updateBio(token: String, bio: String): Response<RpProfile>
    suspend fun updateRoom(
        token: String,
        id: Int,
        roomName: String,
        isPrivate: Boolean
    ): Response<RpCreateRoom>

    suspend fun deleteRoom(id: Int, token: String): Response<RpSuccess>
    suspend fun leaveRoom(id: Int, token: String): Response<RpSuccess>
    suspend fun roomInfo(id: Int, token: String): Response<RpRoomInfo>
    suspend fun kickMember(token: String, roomId: Int, memberId: Int): Response<RpSuccess>
    suspend fun addMember(token: String, roomId: Int, members: List<Int>): Response<RpAddMember>

    fun getList(): Flowable<List<Popular>>
    suspend fun forgetPin(
        token: String,
        currentPassword: String,
        newPin: String,
        confirmPin: String
    ): Response<RpForgetPin>

    suspend fun changePassword(
        token: String,
        currentPassword: String,
        newPass: String,
        confirmNewPass: String
    ): Response<RpChangePassword>
}