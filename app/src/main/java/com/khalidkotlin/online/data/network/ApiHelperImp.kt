package com.khalidkotlin.online.data.network

import okhttp3.MultipartBody
import javax.inject.Inject

class ApiHelperImp @Inject constructor(private val apiService: ApiService) : ApiHelper {
    override suspend fun getPopular() = apiService.getPopular()
    override suspend fun getEpisode() = apiService.getEpisode()
    override suspend fun login(username: String, password: String) =
        apiService.login(username, password)

    override suspend fun getAllCountries() = apiService.getAllCountries()
    override suspend fun sendVerificationCode(email: String) =
        apiService.sendVerificationCode(email)

    override suspend fun register(
        userName: String,
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        vCode: String,
        country: String,
        gender: String
    ) = apiService.register(userName, email, password, firstName, lastName, vCode, country, gender)

    override suspend fun logOut(token: String) = apiService.logOut(token)

    override suspend fun sendCode(email: String) = apiService.sendCode(email)
    override suspend fun confirmCode(
        token: String,
        email: String,
        password: String,
        password2: String
    ) = apiService.confirmCode(token, email, password, password2)

    override suspend fun getProfile(token: String) = apiService.getProfile(token)
    override suspend fun getSingleProfile(token: String, id: Int) =
        apiService.getSingleProfile(token, id)

    override suspend fun getFeed(token: String) = apiService.getNewsfeed(token)
    override suspend fun getAllChat(token: String) = apiService.getAllChat(token)
    override suspend fun updatePost(token: String, id: Int, post: String) =
        apiService.updatePost(token, id, post)

    override suspend fun deletePost(token: String, id: Int) = apiService.deletePost(token, id)
    override suspend fun getFriends(token: String) = apiService.getFriendList(token)
    override suspend fun getAllFriend(token: String) = apiService.getAllFriend(token)

    override suspend fun createPostWithImage(
        token: String,
        post: String,
        file: MultipartBody.Part
    ) = apiService.createPostWithImage(token, post, file)

    override suspend fun createPost(token: String, post: String) =
        apiService.createPost(token, post)

    //override suspend fun createPost(token: String, post: String, privacy: String) = apiService.createPost(token, post, privacy)

    // override suspend fun createPostWithImage(post: String, privacy: String,file: MultipartBody.Part,token:String) = apiService.createPostWithImage(post,privacy,file, token)


    override suspend fun getSingleChat(token: String, id: Int) = apiService.getSingleChat(token, id)
    override suspend fun sendMessage(token: String, receiver: Int, message: String) =
        apiService.sendMessage(token, receiver, message)

    override suspend fun sendMessageWithImage(
        token: String,
        receiver: Int,
        message: String,
        file: MultipartBody.Part
    ) = apiService.sendMessageWithImage(token, receiver, message, file)

    override suspend fun getComment(id: Int, token: String) = apiService.getComment(id, token)
    override suspend fun acceptFriendRequest(id: Int, token: String) =
        apiService.AcceptFriendRequest(token, id)

    override suspend fun cancelFriendRequest(id: Int, token: String) =
        apiService.cancelFriendRequest(token, id)

    override suspend fun joinRoom(id: Int, token: String) = apiService.joinRoom(token, id)
    override suspend fun makeFavoriteRoom(id: Int, token: String) =
        apiService.makeFavoriteRoom(token, id)

    override suspend fun removeFromFavoriteRoom(id: Int, token: String) =
        apiService.removeFromFavoriteRoom(token, id)

    override suspend fun rejectFriendRequest(id: Int, token: String) =
        apiService.rejectFriendRequest(token, id)

    override suspend fun sendFriendRequest(id: Int, token: String) =
        apiService.sendFriendRequest(token, id)

    override suspend fun unfriend(id: Int, token: String) = apiService.unfriend(token, id)
    override suspend fun createComment(token: String, id: Int, comment: String) =
        apiService.createComment(token, id, comment)

    override suspend fun getFriendRequest(token: String) = apiService.getFriendRequest(token)
    override suspend fun getAllUser(token: String) = apiService.getAllUser(token)
    override suspend fun getAllRoom(token: String) = apiService.getAllRoom(token)
    override suspend fun getHotRoom(token: String) = apiService.getHotRoom(token)
    override suspend fun getMyRoom(token: String) = apiService.getMyRoom(token)
    override suspend fun getFavouriteRoom(token: String) = apiService.getFavouriteRoom(token)
    override suspend fun getRecentRoom(token: String) = apiService.getRecentRoom(token)
    override suspend fun getAllReactions(token: String) = apiService.getAllReactions(token)
    override suspend fun getAllRoomMessage(token: String, id: Int) =
        apiService.getAllRoomMessage(token, id)

    override suspend fun createRoom(token: String, roomName: String, isPrivate: Boolean) =
        apiService.createRoom(token, roomName, isPrivate)

    override suspend fun sendGroupMessage(
        token: String,
        roomId: Int,
        message: String,
        file: MultipartBody.Part
    ) = apiService.sendGroupMessage(token, roomId, message, file)

    override suspend fun sendGroupOnlyMessage(token: String, roomId: Int, message: String) =
        apiService.sendGroupOnlyMessage(token, roomId, message)

    override suspend fun addReactions(token: String, postId: Int, reactionId: Int) =
        apiService.addReaction(token, postId, reactionId)

    override suspend fun searchRoom(token: String, roomName: String) =
        apiService.searchRoom(token, roomName)

    override suspend fun getAllGift(token: String) = apiService.getAllGift(token)

    override suspend fun sendGift(token: String, receiverId: Int, giftId: Int) =
        apiService.sendGift(token, receiverId, giftId)

    override suspend fun sendRoomGift(token: String, roomId: Int, giftId: Int) =
        apiService.sendRoomGift(token, roomId, giftId)

    override suspend fun updateProfilePic(token: String, file: MultipartBody.Part) =
        apiService.updateProfilePic(token, file)

    override suspend fun updateCoverPic(token: String, file: MultipartBody.Part) =
        apiService.updateCoverPic(token, file)

    override suspend fun updateBio(token: String, bio: String) = apiService.updateBio(token, bio)
    override suspend fun updateRoom(token: String, id: Int, roomName: String, isPrivate: Boolean) =
        apiService.updateRoom(token, id, roomName, isPrivate)

    override suspend fun deleteRoom(id: Int, token: String) = apiService.deleteRoom(token, id)
    override suspend fun leaveRoom(id: Int, token: String) = apiService.leaveRoom(token, id)
    override suspend fun roomInfo(id: Int, token: String) = apiService.roomInfo(token, id)
    override suspend fun getSearchUser(token: String, keyWord: String) =
        apiService.getSearchUser(token, keyWord)

    override suspend fun kickMember(token: String, roomId: Int, memberId: Int) =
        apiService.kickMember(token, roomId, memberId)

    override suspend fun addMember(token: String, roomId: Int, members: List<Int>) =
        apiService.addMember(token, roomId, members)

    override suspend fun changePin(
        token: String,
        currentPin: String,
        newPin: String,
        confirmPin: String
    ) = apiService.changePin(token, currentPin, newPin, confirmPin)

    override suspend fun transferCoin(token: String, receiverId: Int, coin: Int, pin: String) =
        apiService.transferCoin(token, receiverId, coin, pin)

    override suspend fun getMyPost(token: String) = apiService.getMyPost(token)
    override fun getList() = apiService.getList()
    override suspend fun forgetPin(
        token: String,
        currentPassword: String,
        newPin: String,
        confirmPin: String
    ) = apiService.forgetPin(token, currentPassword, newPin, confirmPin)

    override suspend fun changePassword(
        token: String,
        currentPassword: String,
        newPass: String,
        confirmNewPass: String
    ) = apiService.changePassword(token, currentPassword, newPass, confirmNewPass)

}