package com.khalidkotlin.online.data.network

import com.khalidkotlin.online.data.demo.Episode
import com.khalidkotlin.online.data.model.Popular
import com.khalidkotlin.online.ui.settting.change_pin.RpChangePin
import com.khalidkotlin.online.ui.chat.models.RpCreateRoom
import com.khalidkotlin.online.ui.friendlist.model.RpFriends
import com.khalidkotlin.online.ui.friends.models.RpAllFriendList
import com.khalidkotlin.online.ui.friends.models.RpAllUser
import com.khalidkotlin.online.ui.friends.models.RpFriendRequest
import com.khalidkotlin.online.ui.gift_store.models.RpGift
import com.khalidkotlin.online.ui.group_chat.models.RpAddMember
import com.khalidkotlin.online.ui.group_chat.models.RpGroupMessage
import com.khalidkotlin.online.ui.group_chat.models.RpRoomInfo
import com.khalidkotlin.online.ui.home.model.RpComment
import com.khalidkotlin.online.ui.home.model.RpCreateComment
import com.khalidkotlin.online.ui.home.model.RpReactions
import com.khalidkotlin.online.ui.single_chat.models.RpAllChat
import com.khalidkotlin.online.ui.single_chat.models.RpChat
import com.khalidkotlin.online.ui.login.models.RpLogin
import com.khalidkotlin.online.ui.login.models.RpStatus
import com.khalidkotlin.online.ui.login.models.RpSuccess
import com.khalidkotlin.online.ui.main.models.RpCreatePost
import com.khalidkotlin.online.ui.main.models.RpNewsFeed
import com.khalidkotlin.online.ui.profile.models.RpMyPost
import com.khalidkotlin.online.ui.profile.models.RpProfile
import com.khalidkotlin.online.ui.search_people.models.RpSearchUser
import com.khalidkotlin.online.ui.send_gift.RpSendGift
import com.khalidkotlin.online.ui.settting.change_password.models.RpChangePassword
import com.khalidkotlin.online.ui.settting.forget_pin.models.RpForgetPin
import com.khalidkotlin.online.ui.settting.transfer_coin.models.RpTransferCoin
import com.khalidkotlin.online.ui.signup.models.RpCountries
import io.reactivex.Flowable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("episodes/popular/0/10")
    suspend fun getPopular(): Response<List<Popular>>

    @GET("episodes/popular/0/10")
    fun getList(): Flowable<List<Popular>>

    @GET("channels/5f14537df962815f6369107b/episodes")
    fun getEpisode(): Response<List<Episode>>

    @FormUrlEncoded
    @POST("account/login/")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Response<RpLogin>

    @GET("common/all-countries/")
    suspend fun getAllCountries(): Response<List<RpCountries>>


    @FormUrlEncoded
    @POST("account/verify-email/")
    suspend fun sendVerificationCode(
        @Field("email") email: String
    ): Response<RpLogin>

    @FormUrlEncoded
    @POST("account/register/")
    suspend fun register(
        @Field("username") username: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("first_name") firstName: String,
        @Field("last_name") lastName: String,
        @Field("verification_code") vCode: String,
        @Field("country") country: String,
        @Field("gender") gender: String,
    ): Response<RpLogin>


    @POST("account/logout/")
    suspend fun logOut(
        @Header("Authorization") token: String
    ): Response<RpSuccess>


    @FormUrlEncoded
    @POST("account/reset-password/")
    suspend fun sendCode(
        @Field("email") email: String
    ): Response<RpStatus>

    @FormUrlEncoded
    @POST("account/confirm-password-reset/")
    suspend fun confirmCode(
        @Field("token") token: String,
        @Field("email") email: String,
        @Field("new_password") password: String,
        @Field("new_password_2") password2: String,
    ): Response<RpStatus>


    @GET("newsfeed/my-feed/")
    suspend fun getNewsfeed(
        @Header("Authorization") token: String
    ): Response<List<RpNewsFeed>>

    @FormUrlEncoded
    @POST("newsfeed/my-post/update-post/{id}/")
    suspend fun updatePost(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Field("post") post: String
    ): Response<RpNewsFeed>

    @GET("account/my-profile/")
    suspend fun getProfile(
        @Header("Authorization") token: String
    ): Response<RpProfile>

    @GET("account/users/get-profile/{id}/")
    suspend fun getSingleProfile(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpProfile>

    @FormUrlEncoded
    @POST("newsfeed/my-post/create-post/")
    suspend fun createPost(
        @Header("Authorization") token: String,
        @Field("post") post: String,
        //@Field("post_privacy") policy: String
    ): Response<RpCreatePost>

    @Multipart
    @POST("newsfeed/my-post/create-post/")
    suspend fun createPostWithImage(
        @Header("Authorization") token: String,
        @Part("post") post: String,
        //@Part("post_privacy") policy: String,
        @Part file: MultipartBody.Part
    ): Response<RpCreatePost>


    @GET("friends/all-friends/")
    suspend fun getFriendList(
        @Header("Authorization") token: String
    ): Response<List<RpFriends>>


    @POST("newsfeed/my-post/delete-post/{id}/")
    suspend fun deletePost(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>

    @GET("chat/one-to-one/all-threads/")
    suspend fun getAllChat(
        @Header("Authorization") token: String,
    ): Response<List<RpAllChat>>

    @GET("chat/one-to-one/thread/{id}")
    suspend fun getSingleChat(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<List<RpChat>>

    @FormUrlEncoded
    @POST("chat/one-to-one/send-message/")
    suspend fun sendMessage(
        @Header("Authorization") token: String,
        @Field("receiver_id") id: Int,
        @Field("message") message: String
    ): Response<List<RpChat>>

    @Multipart
    @POST("chat/one-to-one/send-message/")
    suspend fun sendMessageWithImage(
        @Header("Authorization") token: String,
        @Part("receiver_id") id: Int,
        @Part("message") message: String,
        @Part file: MultipartBody.Part
    ): Response<List<RpChat>>

    @GET("newsfeed/my-post/details/{id}/")
    suspend fun getComment(
        @Path("id") id: Int,
        @Header("Authorization") token: String
    ): Response<RpComment>

    @FormUrlEncoded
    @POST("newsfeed/comment/create-comment/")
    suspend fun createComment(
        @Header("Authorization") token: String,
        @Field("post_id") id: Int,
        @Field("comment") message: String
    ): Response<RpCreateComment>


    @GET("friends/all-friend-requests/")
    suspend fun getFriendRequest(
        @Header("Authorization") token: String
    ): Response<List<RpFriendRequest>>

    @GET("friends/all-friends/")
    suspend fun getAllFriend(
        @Header("Authorization") token: String
    ): Response<List<RpAllFriendList>>

    @GET("account/users/all/")
    suspend fun getAllUser(
        @Header("Authorization") token: String
    ): Response<List<RpAllUser>>


    @POST("friends/accept-friend-request/{id}/")
    suspend fun AcceptFriendRequest(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
    ): Response<RpSuccess>

    @POST("friends/cancel-friend-request/{id}/")
    suspend fun cancelFriendRequest(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
    ): Response<RpSuccess>

    @POST("friends/reject-friend-request/{id}/")
    suspend fun rejectFriendRequest(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
    ): Response<RpSuccess>

    @POST("friends/un-friend/{id}/")
    suspend fun unfriend(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
    ): Response<RpSuccess>

    @FormUrlEncoded
    @POST("friends/send-friend-request/")
    suspend fun sendFriendRequest(
        @Header("Authorization") token: String,
        @Field("receiver_id") id: Int,
    ): Response<RpSuccess>


    @FormUrlEncoded
    @POST("chat/room/create-room/")
    suspend fun createRoom(
        @Header("Authorization") token: String,
        @Field("room_name") roomName: String,
        @Field("is_private") isPrivate: Boolean
    ): Response<RpCreateRoom>

    @GET("chat/room/all-rooms/")
    suspend fun getAllRoom(
        @Header("Authorization") token: String
    ): Response<List<RpCreateRoom>>

    @GET("chat/room/hot-rooms/")
    suspend fun getHotRoom(
        @Header("Authorization") token: String
    ): Response<List<RpCreateRoom>>

    @POST("chat/room/my-favourite-rooms/")
    suspend fun getFavouriteRoom(
        @Header("Authorization") token: String
    ): Response<List<RpCreateRoom>>

    @GET("chat/room/my-rooms/")
    suspend fun getMyRoom(
        @Header("Authorization") token: String
    ): Response<List<RpCreateRoom>>

    @GET("chat/room/recent-rooms/")
    suspend fun getRecentRoom(
        @Header("Authorization") token: String
    ): Response<List<RpCreateRoom>>


    @GET("chat/room/all-messages/{id}/")
    suspend fun getAllRoomMessage(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<List<RpGroupMessage>>


    @Multipart
    @POST("chat/room/send-message/")
    suspend fun sendGroupMessage(
        @Header("Authorization") token: String,
        @Part("room_id") roomId: Int,
        @Part("message") message: String,
        @Part file: MultipartBody.Part
    ): Response<List<RpGroupMessage>>


    @FormUrlEncoded
    @POST("chat/room/send-message/")
    suspend fun sendGroupOnlyMessage(
        @Header("Authorization") token: String,
        @Field("room_id") roomId: Int,
        @Field("message") message: String,
    ): Response<List<RpGroupMessage>>

    @GET("common/all-reactions/")
    suspend fun getAllReactions(
        @Header("Authorization") token: String
    ): Response<List<RpReactions>>


    @FormUrlEncoded
    @POST("newsfeed/reaction/add-reaction/")
    suspend fun addReaction(
        @Header("Authorization") token: String,
        @Field("post_id") id: Int,
        @Field("reaction_id") reaction_id: Int
    ): Response<RpSuccess>

    @FormUrlEncoded
    @POST("chat/room/search-room/")
    suspend fun searchRoom(
        @Header("Authorization") token: String,
        @Field("room_name") roomName: String,
    ): Response<List<RpCreateRoom>>


    @POST("chat/room/join-room/{id}/")
    suspend fun joinRoom(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>

    //api/chat/room/make-favourite//
    @POST("chat/room/make-favourite/{id}/")
    suspend fun makeFavoriteRoom(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>

    @POST("chat/room/remove-favourite/{id}/")
    suspend fun removeFromFavoriteRoom(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>

    @GET("common/all-gifts/")
    suspend fun getAllGift(
        @Header("Authorization") token: String,
    ): Response<List<RpGift>>

    @FormUrlEncoded
    @POST("chat/one-to-one/send-gift/")
    suspend fun sendGift(
        @Header("Authorization") token: String,
        @Field("receiver_id") receiverID: Int,
        @Field("gift_id") giftId: Int,
    ): Response<List<RpSendGift>>

    @FormUrlEncoded
    @POST("chat/room/send-gift/")
    suspend fun sendRoomGift(
        @Header("Authorization") token: String,
        @Field("room_id") roomId: Int,
        @Field("gift_id") giftId: Int,
    ): Response<List<RpSendGift>>

    @FormUrlEncoded
    @POST("account/users/search-user/")
    suspend fun getSearchUser(
        @Header("Authorization") token: String,
        @Field("search_keyword") keyWord: String
    ): Response<List<RpSearchUser>>

    @GET("newsfeed/my-post/all-posts/")
    suspend fun getMyPost(
        @Header("Authorization") token: String
    ): Response<List<RpMyPost>>

    @FormUrlEncoded
    @POST("account/change-pin/")
    suspend fun changePin(
        @Header("Authorization") token: String,
        @Field("current_pin") currentPin: String,
        @Field("new_pin") newPin: String,
        @Field("new_pin_2") confirmPin: String
    ): Response<RpChangePin>

    @Multipart
    @POST("account/my-profile/")
    suspend fun updateProfilePic(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part
    ): Response<RpProfile>

    @FormUrlEncoded
    @POST("account/my-profile/")
    suspend fun updateBio(
        @Header("Authorization") token: String,
        @Field("bio") bio: String,
    ): Response<RpProfile>

    @Multipart
    @POST("account/my-profile/")
    suspend fun updateCoverPic(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part
    ): Response<RpProfile>


    @FormUrlEncoded
    @POST("account/transfer-coin/")
    suspend fun transferCoin(
        @Header("Authorization") token: String,
        @Field("receiver_id") receiverID: Int,
        @Field("coin") coin: Int,
        @Field("pin") pin: String,
    ): Response<RpTransferCoin>


    @FormUrlEncoded
    @POST("chat/room/update-room/")
    suspend fun updateRoom(
        @Header("Authorization") token: String,
        @Field("room_id") id: Int,
        @Field("room_name") roomName: String,
        @Field("is_private") isPrivate: Boolean
    ): Response<RpCreateRoom>


    @POST("chat/room/delete-room/{id}/")
    suspend fun deleteRoom(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>


    @POST("chat/room/leave-room/{id}/")
    suspend fun leaveRoom(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpSuccess>

    @GET("chat/room/room-info/{id}/")
    suspend fun roomInfo(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Response<RpRoomInfo>

    @FormUrlEncoded
    @POST("chat/room/kick-out/")
    suspend fun kickMember(
        @Header("Authorization") token: String,
        @Field("room_id") id: Int,
        @Field("member_id") memberId: Int,
    ): Response<RpSuccess>

    @FormUrlEncoded
    @POST("chat/room/add-members/")
    suspend fun addMember(
        @Header("Authorization") token: String,
        @Field("room_id") id: Int,
        @Field("members") member: List<Int>,
    ): Response<RpAddMember>

    @FormUrlEncoded
    @POST("account/forget-pin/")
    suspend fun forgetPin(
        @Header("Authorization") token: String,
        @Field("current_password") currentPin: String,
        @Field("new_pin") newPin: String,
        @Field("new_pin_2") confirmPin: String
    ): Response<RpForgetPin>

    @FormUrlEncoded
    @POST("account/change-password/")
    suspend fun changePassword(
        @Header("Authorization") token: String,
        @Field("current_password") currentPass: String,
        @Field("new_password") newPass: String,
        @Field("new_password_2") confirmNewPass: String
    ): Response<RpChangePassword>


}