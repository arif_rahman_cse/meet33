package com.khalidkotlin.online.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Defines the schema of a table in room for a single downloaded episode.
 */
@Entity(tableName = "downloaded_episodes")
public class DownloadEntry implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "podcast_id")
    private String podcastId;

    /** The podcast title */
    @ColumnInfo(name = "podcast_title")
    private String title;

    @ColumnInfo(name = "artwork_image_url")
    private String artworkImageUrl;

    @ColumnInfo(name = "channel_id")
    private String channel_id;

    @ColumnInfo(name = "channel_name")
    private String channel_name;

    @ColumnInfo(name = "channel_cat")
    private String channel_cat;

    @ColumnInfo(name = "status")
    private Boolean status;

    @ColumnInfo(name = "totalplays")
    private int totalplays;

    @ColumnInfo(name = "totalDownloads")
    private int totalDownloads;


    @ColumnInfo(name = "episod_number")
    private int episod_number;

    @ColumnInfo(name = "host")
    private String host;

    @ColumnInfo(name = "updated_at")
    private String updated_at;

    @ColumnInfo(name = "created_at")
    private String created_at;

    @ColumnInfo(name = "_v")
    private int _v;

    @ColumnInfo(name = "isDeleted")
    private boolean isDeleted;


    @ColumnInfo(name = "item_title")
    private String itemTitle;

    @ColumnInfo(name = "item_description")
    private String itemDescription;

    @ColumnInfo(name = "item_pub_date")
    private String itemPubDate;

    @ColumnInfo(name = "item_duration")
    private int itemDuration;

    @ColumnInfo(name = "item_enclosure_url")
    private String itemEnclosureUrl;






    public String getChannel_id() {
        return channel_id;
    }

    public String getChannel_cat() {
        return channel_cat;
    }

    public void setChannel_cat(String channel_cat) {
        this.channel_cat = channel_cat;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }


    public DownloadEntry() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPodcastId(String podcastId) {
        this.podcastId = podcastId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtworkImageUrl(String artworkImageUrl) {
        this.artworkImageUrl = artworkImageUrl;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public void setItemPubDate(String itemPubDate) {
        this.itemPubDate = itemPubDate;
    }

    public void setItemEnclosureUrl(String itemEnclosureUrl) {
        this.itemEnclosureUrl = itemEnclosureUrl;
    }

    public DownloadEntry(int id, String podcastId,
                         String artworkImageUrl,
                         String itemTitle
                        , int itemDuration,
                         String itemEnclosureUrl
                        , String channel_id,
                         String channel_name,
                         String channel_cat,
                         int totalDownloads,
                         int totalplays,
                         int episodNumber
                         , String host,
                         String updated_at,
                         String created_at,
                         boolean isDeleted,
                         boolean status,
                         int _v) {
        this.podcastId = podcastId;
        this.artworkImageUrl = artworkImageUrl;
        this.itemTitle = itemTitle;
        this.channel_id =channel_id;
         this.channel_name =channel_name;
        this.itemDuration = itemDuration;
        this.itemEnclosureUrl = itemEnclosureUrl;
        this._v = _v;
        this.isDeleted = isDeleted;
        this.created_at = created_at;
        this.updated_at =updated_at;
        this.totalDownloads =totalDownloads;
        this.totalplays = totalplays;
        this.episod_number = episodNumber;
        this.host =host;
        this.channel_cat = channel_cat;
        this.id = id;
        this.status =status;

    }

    /**
     * Constructor used by Room to create DownloadEntries.
     */

//    public DownloadEntry(int id, String podcastId, String title,
//                         String artworkImageUrl, String itemTitle, String itemDescription,
//                         String itemPubDate, int itemDuration, String itemEnclosureUrl,int totalDownloads,int totalplays,int episodNumber
//            ,String host,String updated_at,String created_at,boolean isDeleted,int _v,boolean status
//                         ) {
//        this.id = id;
//        this.podcastId = podcastId;
//        this.title = title;
//        this.artworkImageUrl = artworkImageUrl;
//        this.itemTitle = itemTitle;
//        this.itemDescription = itemDescription;
//        this.itemPubDate = itemPubDate;
//        this.itemDuration = itemDuration;
//        this.itemEnclosureUrl = itemEnclosureUrl;
//        this._v = _v;
//        this.isDeleted = isDeleted;
//        this.created_at = created_at;
//        this.updated_at =updated_at;
//        this.totalDownloads =totalDownloads;
//        this.totalplays = totalplays;
//        this.episod_number = episodNumber;
//        this.host =host;
//        this.status = status;
//
//
//
//    }

    protected DownloadEntry(Parcel in) {
        podcastId = in.readString();
        title = in.readString();
        artworkImageUrl = in.readString();
        itemTitle = in.readString();
        itemDescription = in.readString();
        itemPubDate = in.readString();
        itemDuration = in.readInt();
        itemEnclosureUrl = in.readString();
        _v = in.readInt();
        isDeleted =  in.readByte()!=0;
        updated_at = in.readString();
        created_at =  in.readString();
        host =in.readString();
        channel_id = in.readString();
        channel_name = in.readString();
        channel_cat = in.readString();
        totalDownloads = in.readInt();
        totalplays = in.readInt();
        episod_number = in.readInt();
        status = in.readByte()!=0;
    }

    public static final Creator<DownloadEntry> CREATOR = new Creator<DownloadEntry>() {
        @Override
        public DownloadEntry createFromParcel(Parcel in) {
            return new DownloadEntry(in);
        }

        @Override
        public DownloadEntry[] newArray(int size) {
            return new DownloadEntry[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getPodcastId() {
        return podcastId;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getTotalplays() {
        return totalplays;
    }

    public void setTotalplays(int totalplays) {
        this.totalplays = totalplays;
    }

    public int getTotalDownloads() {
        return totalDownloads;
    }

    public void setTotalDownloads(int totalDownloads) {
        this.totalDownloads = totalDownloads;
    }

    public int getEpisod_number() {
        return episod_number;
    }

    public void setEpisod_number(int episod_number) {
        this.episod_number = episod_number;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getItemDuration() {
        return itemDuration;
    }

    public void setItemDuration(int itemDuration) {
        this.itemDuration = itemDuration;
    }

    public int get_v() {
        return _v;
    }

    public void set_v(int _v) {
        this._v = _v;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getArtworkImageUrl() {
        return artworkImageUrl;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public String getItemPubDate() {
        return itemPubDate;
    }


    public String getItemEnclosureUrl() {
        return itemEnclosureUrl;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(podcastId);
        dest.writeString(title);
        dest.writeString(artworkImageUrl);
        dest.writeString(itemTitle);
        dest.writeString(itemDescription);
        dest.writeString(itemPubDate);
        dest.writeInt(itemDuration);
        dest.writeString(itemEnclosureUrl);
        dest.writeInt(_v);
        dest.writeByte((byte) (status ? 1 : 0));
        dest.writeString(updated_at);
        dest.writeString(created_at);
        dest.writeString(host);
        dest.writeString(channel_id);
        dest.writeString(channel_name);
        dest.writeString(channel_cat);
        dest.writeInt(totalDownloads);
        dest.writeInt(totalplays);
        dest.writeInt(episod_number);
        dest.writeByte((byte) (status ? 1 : 0));



    }


}
