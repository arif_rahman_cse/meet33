package com.khalidkotlin.online.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.khalidkotlin.online.data.dao.SoftMaxDao
import com.khalidkotlin.online.data.converter.DateConverter
import com.khalidkotlin.online.data.entity.DownloadEntry
import com.khalidkotlin.online.data.entity.FavoriteEntry
import com.khalidkotlin.online.utils.Constants

@Database(entities = [FavoriteEntry::class, DownloadEntry::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun softMaxDao(): SoftMaxDao?
    companion object {

        private val LOCK = Any()
        private var sInstance: AppDatabase? = null
        @JvmStatic
        fun getInstance(context: Context): AppDatabase? {
            //Timber.d("Getting the database")
            if (sInstance == null) {
                synchronized(LOCK) {
                    sInstance = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java, Constants.DATABASE_NAME).build()
                    //Timber.d("Made new database")
                }
            }
            return sInstance
        }
    }
}