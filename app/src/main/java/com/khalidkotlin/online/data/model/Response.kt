package com.khalidkotlin.online.data.model

import com.google.gson.annotations.SerializedName

data class Response(

	@field:SerializedName("author_name")
	val authorName: String? = null,

	@field:SerializedName("total_comments")
	val totalComments: Int? = null,

	@field:SerializedName("post")
	val post: String? = null,

	@field:SerializedName("date_edited")
	val dateEdited: String? = null,

	@field:SerializedName("attachment")
	val attachment: Any? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("is_edited")
	val isEdited: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("post_privacy")
	val postPrivacy: String? = null,

	@field:SerializedName("author_id")
	val authorId: String? = null,

	@field:SerializedName("total_reactions")
	val totalReactions: Int? = null
)
