package com.khalidkotlin.online.data.repository

import com.khalidkotlin.online.data.network.ApiHelper
import okhttp3.MultipartBody
import javax.inject.Inject

class Repository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun login(userName: String, password: String) = apiHelper.login(userName, password)
    suspend fun getAllCountries() = apiHelper.getAllCountries()
    suspend fun sendVerificationCode(email: String) = apiHelper.sendVerificationCode(email)
    suspend fun register(
        userName: String, email: String, password: String, firstName: String, lastName: String,
        vCode: String, country: String, gender: String
    ) = apiHelper.register(userName, email, password, firstName, lastName, vCode, country, gender)

    suspend fun logOut(token: String) = apiHelper.logOut(token)
    suspend fun sendCode(email: String) = apiHelper.sendCode(email)
    suspend fun confirmCode(
        token: String,
        email: String,
        password: String,
        password2: String
    ) = apiHelper.confirmCode(token, email, password, password2)

    suspend fun getProfile(token: String) = apiHelper.getProfile(token)
    suspend fun getSingleProfile(token: String, id: Int) = apiHelper.getSingleProfile(token, id)
    suspend fun getNewsFeed(token: String) = apiHelper.getFeed(token)
    suspend fun getFriends(token: String) = apiHelper.getFriends(token)
    suspend fun getAllChat(token: String) = apiHelper.getAllChat(token)
    suspend fun getAllUser(token: String) = apiHelper.getAllUser(token)
    suspend fun updatePost(token: String, id: Int, post: String) =
        apiHelper.updatePost(token, id, post)

    suspend fun deletePost(token: String, id: Int) = apiHelper.deletePost(token, id)
    suspend fun createPostWithImage(token: String, post: String, file: MultipartBody.Part) =
        apiHelper.createPostWithImage(token, post, file)

    suspend fun createPost(token: String, post: String) = apiHelper.createPost(token, post)

    //suspend fun createPost(token: String, post: String, privacy: String) = apiHelper.createPost(token, post, privacy)
    //suspend fun createPostWithImage(post: String, privacy: String,file: MultipartBody.Part,token:String) = apiHelper.createPostWithImage(post,privacy, file,token)
    suspend fun getSingleChat(token: String, id: Int) = apiHelper.getSingleChat(token, id)
    suspend fun sendMessage(token: String, receiver: Int, message: String) =
        apiHelper.sendMessage(token, receiver, message)

    suspend fun sendMessageWithImage(
        token: String,
        receiver: Int,
        message: String,
        file: MultipartBody.Part
    ) = apiHelper.sendMessageWithImage(token, receiver, message, file)

    suspend fun createComment(token: String, id: Int, comment: String) =
        apiHelper.createComment(token, id, comment)

    suspend fun getComment(id: Int, token: String) = apiHelper.getComment(id, token)
    suspend fun acceptFriendRequest(id: Int, token: String) =
        apiHelper.acceptFriendRequest(id, token)

    suspend fun sendFriendRequest(id: Int, token: String) = apiHelper.sendFriendRequest(id, token)
    suspend fun unfriend(id: Int, token: String) = apiHelper.unfriend(id, token)
    suspend fun cancelFriendRequest(id: Int, token: String) =
        apiHelper.cancelFriendRequest(id, token)

    suspend fun rejectFriendRequest(id: Int, token: String) =
        apiHelper.rejectFriendRequest(id, token)

    suspend fun getFriendRequest(token: String) = apiHelper.getFriendRequest(token)
    suspend fun getAllFriend(token: String) = apiHelper.getAllFriend(token)
    suspend fun getAllRoom(token: String) = apiHelper.getAllRoom(token)
    suspend fun getHotRoom(token: String) = apiHelper.getHotRoom(token)
    suspend fun getMyRoom(token: String) = apiHelper.getMyRoom(token)
    suspend fun getFavouriteRoom(token: String) = apiHelper.getFavouriteRoom(token)
    suspend fun getRecentRoom(token: String) = apiHelper.getRecentRoom(token)
    suspend fun getAllRoomMessage(token: String, id: Int) = apiHelper.getAllRoomMessage(token, id)
    suspend fun createRoom(token: String, roomName: String, isPrivate: Boolean) =
        apiHelper.createRoom(token, roomName, isPrivate)

    suspend fun updateRoom(token: String, id: Int, roomName: String, isPrivate: Boolean) =
        apiHelper.updateRoom(token, id, roomName, isPrivate)

    suspend fun sendGroupMessage(
        token: String,
        roomId: Int,
        message: String,
        file: MultipartBody.Part
    ) = apiHelper.sendGroupMessage(token, roomId, message, file)

    suspend fun sendGroupOnlyMessage(token: String, roomId: Int, message: String) =
        apiHelper.sendGroupOnlyMessage(token, roomId, message)

    suspend fun getSearchUser(token: String, keyWord: String) =
        apiHelper.getSearchUser(token, keyWord)

    suspend fun getAllReactions(token: String) = apiHelper.getAllReactions(token)
    suspend fun addReactions(token: String, postId: Int, reactionId: Int) =
        apiHelper.addReactions(token, postId, reactionId)

    suspend fun searchRoom(token: String, roomName: String) = apiHelper.searchRoom(token, roomName)
    suspend fun joinRoom(id: Int, token: String) = apiHelper.joinRoom(id, token)
    suspend fun makeFavoriteRoom(id: Int, token: String) = apiHelper.makeFavoriteRoom(id, token)
    suspend fun removeFromFavoriteRoom(id: Int, token: String) =
        apiHelper.removeFromFavoriteRoom(id, token)

    suspend fun getAllGift(token: String) = apiHelper.getAllGift(token)

    suspend fun sendGift(token: String, receiverId: Int, giftId: Int) =
        apiHelper.sendGift(token, receiverId, giftId)

    suspend fun sendRoomGift(token: String, roomId: Int, giftId: Int) =
        apiHelper.sendRoomGift(token, roomId, giftId)

    suspend fun deleteRoom(token: String, id: Int) = apiHelper.deleteRoom(id, token)
    suspend fun leaveRoom(token: String, id: Int) = apiHelper.leaveRoom(id, token)
    suspend fun roomInfo(token: String, id: Int) = apiHelper.roomInfo(id, token)
    suspend fun kickMember(token: String, roomId: Int, memberId: Int) =
        apiHelper.kickMember(token, roomId, memberId)

    suspend fun addMember(token: String, roomId: Int, members: List<Int>) =
        apiHelper.addMember(token, roomId, members)

    suspend fun updateProfilePic(token: String, file: MultipartBody.Part) =
        apiHelper.updateProfilePic(token, file)

    suspend fun updateCoverPic(token: String, file: MultipartBody.Part) =
        apiHelper.updateCoverPic(token, file)

    suspend fun updateBio(token: String, bio: String) = apiHelper.updateBio(token, bio)
    suspend fun changePin(token: String, currentPin: String, newPin: String, confirmPin: String) =
        apiHelper.changePin(token, currentPin, newPin, confirmPin)

    suspend fun transferCoin(token: String, receiverId: Int, coin: Int, pin: String) =
        apiHelper.transferCoin(token, receiverId, coin, pin)

    suspend fun getMyPost(token: String) = apiHelper.getMyPost(token)
    fun getList() = apiHelper.getList()
    suspend fun forgetPin(
        token: String,
        currentPassword: String,
        newPin: String,
        confirmPin: String
    ) = apiHelper.forgetPin(token, currentPassword, newPin, confirmPin)

    suspend fun changePassword(
        token: String,
        currentPassword: String,
        newPass: String,
        confirmNewPass: String
    ) = apiHelper.changePassword(token, currentPassword, newPass, confirmNewPass)

}